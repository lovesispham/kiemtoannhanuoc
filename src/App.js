import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import routes from '../src/routes/routes';

import 'bootstrap/dist/css/bootstrap.min.css';
import './shards-dashboard/styles/shards-dashboards.1.1.0.min.css';
import { TicketProvider } from './components/notification/TicketContent';

const App = props => {

  return (
    <TicketProvider>
    <React.Suspense fallback={<div>....Loading</div>}>
      {/* {checkRenderSidebar(location.pathname) && <Sidebar pathRoute={''} />} */}
      <BrowserRouter>
        <>
          {/* <DefaultLayout /> */}
          <Switch>
            {Object.keys(routes).map((key, index) => {
              const route = routes[key];
              return (
                <Route
                  key={index}
                  path={route.path}
                  exact={route.exact}
                  component={() => {
                    return (
                      <route.layout {...props}>
                        {/* <route.component {...props} /> */}
                        <route.route key={route.path} {...route} />
                      </route.layout>
                    );
                  }}></Route>
              );
            })}
            {/* <Route path="*" component={NotFound} /> */}
          </Switch>
        </>
      </BrowserRouter>
    </React.Suspense>
    </TicketProvider>
 );
};

export default App;
