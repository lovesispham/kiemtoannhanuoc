/* eslint-disable max-len */
import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import authReducer from '../erp/redux/auth';
import accountReducer from '../erp/redux/account';
import orgReducer from '../erp/redux/org';
import contactReducer from '../erp/redux/contact';
import infoReducer from '../erp/redux/info';
import ticketReducer from '../erp/redux/ticket';
import positionReducer from '../erp/redux/position';
import locationReducer from '../erp/redux/location';
import userReducer from '../erp/redux/user';
import sidebarReducer from '../erp/redux/sidebar';
import reportReducer from '../erp/redux/report';
import departmentReducer from '../erp/redux/department';
const store = configureStore({
  reducer: {
    auth: authReducer,
    accounts: accountReducer,
    sidebar: sidebarReducer,
    org:orgReducer,
    contact:contactReducer,
    info: infoReducer,
    ticket:ticketReducer,
    position:positionReducer,
    location:locationReducer,
    user:userReducer,
    report:reportReducer,
    department:departmentReducer
  },
  middleware: getDefaultMiddleware({
    serializableCheck: false
  }),
  devTools: true
});
export default store;
