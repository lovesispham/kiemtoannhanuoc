export default function () {
  return [
    {
      title: 'Thống kê',
      htmlBefore: '<i class="fa fa-home" aria-hidden="true"></i>',
      to: '/thong-ke'
    },
    {
      title: 'Xử lý thông tin cuộc gọi',
      htmlBefore: '<i class="fa fa-ticket-alt" ></i>',
      to: '/quan-ly-ticket'
    },
    {
      title: 'Quản lý liên hệ',
      htmlBefore: '<i class="fa fa-address-book"></i>',
      to: '/quan-ly-lien-he'
    },
    {
      title: 'Quản lý tài liệu hỗ trợ',
      htmlBefore: '<i class="fa fa-book"></i>',
      to: '/quan-ly-tai-lieu'
    },
    {
      title: 'Quản lý đơn vị',
      htmlBefore: '<i class="fa fa-university" aria-hidden="true"></i>',
      to: '/quan-ly-don-vi'
    },
    {
      title: 'Cấu hình',
      htmlBefore: '<i class="fa fa-cog" ></i>',
      to: '',
      submenu:[

        {
          title: 'Phòng ban',
          htmlBefore: '<i class="fa fa-building"></i>',
          to: '/quan-ly-phong-ban'
        },

        {
        title: 'Nhân viên',
        htmlBefore: '<i class="fa fa-users"></i>',
        to: '/quan-ly-nhan-vien'
        },
        {
          title: 'Chức vụ',
          htmlBefore: '<i class="material-icons">person</i>',
          to: '/quan-ly-chuc-vu'
        },
      ]
    },





  ];
}
