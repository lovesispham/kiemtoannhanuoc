import React from 'react';
import { Nav } from 'shards-react';
import { useContext } from 'react';
import { TicketContext } from '../../../components/notification/TicketContent';
import SidebarNavItem from './SidebarNavItem';
import getSidebarNavItems from '../../../data/sidebar-nav-items';



const SidebarNavItems = () => {
  const dataContext = useContext(TicketContext);

  const profile = dataContext.profile;
  const navItems = getSidebarNavItems();
  const navItemsUser = navItems.filter(item => item.title !== 'Quản lý nhân viên')


  return (
    <div className="nav-wrapper">
    <Nav className="nav--no-borders flex-column">
    {
      profile && profile.is_superuser ? (
        navItems.map((item, idx) => (
        <SidebarNavItem key={idx} item={item} />

      ))
      ) : (
        navItemsUser.map((item, idx) => (
        <SidebarNavItem key={idx} item={item} />

      ))
      )
    }

    </Nav>
  </div>
  );
};

export default SidebarNavItems;
