import React from "react";
import PropTypes from "prop-types";
import { Container } from "shards-react";

const MainFooter = ({ contained, copyright }) => (
  <footer className="d-flex p-2 px-3 bg-footer border-top">
    <Container fluid={contained}>
      <div className="text-center">
      <span className="copyright">{copyright}</span>

      </div>
    </Container>
  </footer>
);

MainFooter.propTypes = {
  /**
   * Whether the content is contained, or not.
   */
  contained: PropTypes.bool,
  /**
   * The menu items array.
   */
  menuItems: PropTypes.array,
  /**
   * The copyright info.
   */
  copyright: PropTypes.string
};

MainFooter.defaultProps = {
  contained: false,
  copyright: "Copyright © 2022 Kiểm toán nhà nước Việt Nam",

};

export default MainFooter;
