import axios from 'axios';
import { TIMEOUT } from '../../constants';
import { routeConstants } from '../../constants';
import { getCookieStorage, removeAllCookieStorage, setAllCookieStorage } from '../helps/storage';
import jwt_decode from 'jwt-decode';

const axiosInstance = axios.create({
  baseURL: `${process.env.REACT_APP_BASE_API}`,
  timeout: 10000,
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json',
  },
});

let isAlreadyFetchingAccessToken = false;
let failedQueue = [];

async function getAccessToken(refreshToken) {
  const access_token = await axios
    .post(`${process.env.REACT_APP_BASE_API}/api/token/refresh/`, {
      refresh: refreshToken,
    })
    .then((res) => res.data.access)
    .catch((er) => {
      removeAllCookieStorage(['access_token', 'refresh_token', 'expire_token', 'expire_refresh_token']);
      // window.location.replace(routeConstants.SIGN_IN);
    });

  // Set cookie
  if (access_token) {
    try {
      const tokenDecoded = jwt_decode(access_token);
      const expToken = tokenDecoded.exp ? parseFloat(tokenDecoded.exp) * 1000 : 0;

      setAllCookieStorage([
        { key: 'access_token', value: access_token },
        { key: 'expire_token', value: expToken },
      ]);
      failedQueue.forEach((cb) => cb(access_token));
    } catch (error) {
    } finally {
      failedQueue = [];
      isAlreadyFetchingAccessToken = false;
    }
  }
}

axiosInstance.interceptors.request.use(
  async (config) => {
    const accessToken = getCookieStorage('access_token');
    const expireToken = parseFloat(getCookieStorage('expire_token') || '0');
    const expireRefreshToken = parseFloat(getCookieStorage('expire_refresh_token') || '0');

    if (!!accessToken && expireToken && expireRefreshToken && new Date().getTime() > expireRefreshToken) {
      removeAllCookieStorage(['access_token', 'refresh_token', 'expire_token', 'expire_refresh_token']);
      window.location.replace(routeConstants.SIGN_IN);
    }

    // if (new Date().getTime() > expireToken && accessToken && expireToken) {
    //   // await getAccessToken(refreshToken);
    //   accessToken = getCookieStorage('access_token');
    // }

    if (accessToken) {
      config.headers['Authorization'] = `JWT ${accessToken}`;
    }

    return config;
  },
  (error) => Promise.reject(error)
);

axiosInstance.interceptors.response.use(
  async (response) => {
    if (response && response.data) {
      return response.data;
    }

    return response;
  },
  (error) => {
    // if (!error.response) {
    //   store.dispatch(
    //     openSnackbar({
    //       message: 'No Internet connection',
    //       variant: SnackbarVariant.ERROR,
    //     }),
    //   );
    //   return Promise.reject(error);
    // }
    switch (error.response.status) {
      case 401:
        if (!isAlreadyFetchingAccessToken) {
          getAccessToken(getCookieStorage('refresh_token')).then();
          isAlreadyFetchingAccessToken = true;
        }
        return new Promise(function(resolve, reject) {
          failedQueue.push((newAccessToken) => {
            error.config.headers['Authorization'] = `JWT ${newAccessToken}`;
            try {
              resolve(axiosInstance(error.config)); // retry
            } catch (err) {
              reject(err);
            }
          });
        });

      case 403:
        // if (error.response.data?.code === httpExceptionSubCode.FORBIDDEN.USER_DEACTIVE) {
        //   removeAllCookieStorage(['access_token', 'refresh_token', 'expire_token', 'expire_refresh_token']);
        //   store.dispatch(accountDisabled());
        // }
        break;

      case 404:
        // window.location.href = PATH.PAGE_404;
        break;

      case 500:
        break;

      default:
        break;
    }

    return Promise.reject(error);
  }
);

export default axiosInstance;
