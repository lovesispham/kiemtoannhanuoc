import React, { useEffect, useState, useRef } from 'react';
import { useContext } from 'react';
import { TicketContext } from '../../components/notification/TicketContent';
import { Row, Col, Container, FormInput } from 'shards-react';
import { useDispatch, useSelector } from 'react-redux';
import DialogContact from '../../components/contact/DialogContact';
import Spinner from '../../components/common/Spinner';
import { getContact } from '../redux/contact';
import moment from 'moment';
import { usePaginationState } from '../../hooks/use-pagination';
import { Pagination } from '../../components/common/Pagination';
import { toast_type } from '../../constants';
import ToastMsg from '../../components/common/Toast';
import Modal, { ModalContent } from '../../components/common/Modal';
import { getSyncData } from '../redux/ticket';
const Contact = () => {
  const dispatch = useDispatch();
  const dataContext = useContext(TicketContext);

  const profile = dataContext.profile;

  const contactList = useSelector((state) => state.contact.contactList);

  const loadingState = useSelector((state) => state.contact.loading);
  const { successCreate } = useSelector((state) => state.contact);
  const { successEdit } = useSelector((state) => state.contact);

  const [openModal, setOpenModal] = useState(false);
  const [data, setData] = useState('');
  const [sortConfig, setSortConfig] = useState({});
  const [listConfig, setListConfig] = useState([]);

  const pagination = usePaginationState();

  const wrapperRef = useRef(null);

  const closeModal = (e) => {
    if (e.target === wrapperRef.current) setOpenModal(false);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
    setData('');
  };

  useEffect(() => {
    if (successCreate === true) {
      setTimeout(() => {
        dispatch(getContact({}));
      }, 1000);
    }
  }, [successCreate]);

  useEffect(() => {
    if (successEdit === true) {
      setTimeout(() => {
        dispatch(getContact({}));
      }, 1000);
    }
  }, [successEdit]);

  const [search, setSearch] = useState('');
  const initialValue = {
    name: search,
  };
  const handleChangeSearch = (e) => {
    setSearch(e.target.value);
  };

  useEffect(() => {
    dispatch(
      getContact({
        ...initialValue,
      })
    );
  }, [search]);

  useEffect(() => {
    if (!contactList.contacts) {
      return;
    }
    if (!contactList.contacts.contacts) {
      return;
    }
    setListConfig(contactList.contacts.contacts);
  }, [contactList.contacts]);

  const requestSort = (key) => {
    setSortConfig((s) => {
      const currentDirection = s[key];
      return { ...s, [key]: currentDirection === 'ascending' ? 'descending' : 'ascending' };
    });
    setListConfig((l) => {
      let sorted = [...l];
      sorted = sorted.sort((a, b) => {
        const direction = sortConfig[key];
        if (a[key] && b[key]) {
          let fa = a[key].toLowerCase();
          let fb = b[key].toLowerCase();
          if (fa < fb) {
            return direction === 'ascending' ? -1 : 1;
          }
          if (fa > fb) {
            return direction === 'ascending' ? 1 : -1;
          }
          return 0;
        }
      });
      return sorted;
    });
  };
  const getClassNamesFor = (key) => {
    if (!sortConfig) {
      return;
    }
    return sortConfig[key] === 'ascending' ? 'fa fa-sort-down align-top' : 'fa fa-sort-up align-bottom';
  };
  useEffect(() => {
    dispatch(
      getContact({
        limit: pagination.perPage,
        offset: pagination.perPage * pagination.page - pagination.perPage,
        ...initialValue,
      })
    );
  }, [pagination, search]);
  const SyncData = () => {
    dispatch(getSyncData({}))
      .unwrap()
      .then((res) => console.log('res', res));
  };

  return (
    <>
      <Container fluid className='main-content-container px-4'>
        {successCreate && <ToastMsg type={toast_type.success} />}
        {successCreate === false && <ToastMsg type={toast_type.error} msg='Thất Bại ' />}
        {successEdit && <ToastMsg type={toast_type.success} />}
        {successEdit === false && <ToastMsg type={toast_type.error} msg='Thất Bại ' />}

        <Modal active={openModal} id={`${!data ? '' : `modal_${data.id}`}`}>
          <ModalContent heading={!data ? 'Tạo' : 'Sửa'} onClose={handleCloseModal}>
            <DialogContact setUDO={setOpenModal} setData={setData} dataEdit={data} />
          </ModalContent>
        </Modal>

        {openModal && (
          <>
            <div className='modal-overlay'></div>
            <div ref={wrapperRef} onClick={closeModal} className='modal-popup_style2 open'>
              <div className='popup-container pu-general'>
                <h3 className='pu-heading'>{!data ? 'Tạo' : 'Sửa'}</h3>
                <div className='popup-close' onClick={() => handleCloseModal()}>
                  <span className='fa fa-times'></span>
                </div>
                <DialogContact setUDO={setOpenModal} setData={setData} dataEdit={data} />
              </div>
            </div>
          </>
        )}
        <div className='d-flex box-top'>
          <h1 className='heading'>
            Danh sách liên hệ
            {/* {profile && profile.is_superuser && (
              <span
                className="btn-create"
                onClick={() => {
                  setOpenModal(true);
                  setData('');
                }}>
                <i className="fa fa-plus"></i>
              </span>
            )} */}
          </h1>
          <div className='text-right ml-auto'>
            <div className='form-search'>
              <FormInput
                type='search'
                name='name'
                placeholder='Tìm kiếm'
                value={search}
                onChange={handleChangeSearch}
              />
            </div>
          </div>
        </div>

        <div
          style={{
            marginBottom: '8px',
            height: '30px',
            backgroundColor: '#074d89',
            width: '80px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: '5px',
            color: 'white',
            fontWeight: '600',
            cursor: 'pointer',
          }}
          onClick={SyncData}
        >
          {' '}
          Đồng Bộ
        </div>
        <Row>
          <Col>
            {loadingState ? (
              <Spinner loading={loadingState} />
            ) : (
              <div className='table-wrapper'>
                <table className='table mb-0'>
                  <thead className='bg-light'>
                    <tr>
                      <th scope='col' className='border-0' style={{ width: 40 }}>
                        STT
                      </th>
                      <th scope='col' className='border-0'>
                        <button className='font-semibold' onClick={() => requestSort('fullname')}>
                          Tên
                          <span style={{ marginLeft: '5px' }} className={getClassNamesFor('fullname')}>
                            {' '}
                          </span>
                        </button>
                      </th>
                      <th scope='col' className='border-0'>
                        <button className='font-semibold' onClick={() => requestSort('phone')}>
                          SĐT
                          <span style={{ marginLeft: '5px' }} className={getClassNamesFor('phone')}>
                            {' '}
                          </span>
                        </button>
                      </th>
                      <th scope='col' className='border-0'>
                        Email
                      </th>
                      <th scope='col' className='border-0'>
                        Phòng Ban
                      </th>
                      <th scope='col' className='border-0'>
                        Tên đơn vị
                      </th>
                      <th scope='col' className='border-0'>
                        Chức vụ
                      </th>

                      <th scope='col' className='border-0'>
                        Ngày tạo
                      </th>
                      {profile && profile.is_superuser && (
                        <th scope='col' className='border-0'>
                          Hành động
                        </th>
                      )}
                    </tr>
                  </thead>
                  <tbody>
                    {listConfig.length > 0 &&
                      listConfig.map((item, idx) => {
                        return (
                          <tr key={idx}>
                            <td className='text-center'>{idx + 1}</td>
                            <td>{item.fullname}</td>
                            <td>{item.mobile}</td>
                            <td>{item.email}</td>
                            <td>{item.department}</td>
                            <td>{item.organization}</td>
                            <td>{item.position}</td>

                            <td>{moment(item.created_on).format('DD-MM-YYYY')}</td>
                            {profile && profile.is_superuser && (
                              <td>
                                <span
                                  className='btn btn-edit'
                                  onClick={() => {
                                    setOpenModal(true);
                                    setData(item);
                                  }}
                                >
                                  <i className='fa fa-edit'></i>
                                </span>
                              </td>
                            )}
                          </tr>
                        );
                      })}
                  </tbody>
                </table>
                <Pagination
                  currentPage={pagination.page}
                  pageSize={pagination.perPage}
                  lastPage={Math.min(
                    Math.ceil(
                      (contactList ? contactList.contacts && contactList.contacts.count : 0) / pagination.perPage
                    ),
                    Math.ceil(
                      (contactList ? contactList.contacts && contactList.contacts.count : 0) / pagination.perPage
                    )
                  )}
                  onChangePage={pagination.setPage}
                  onChangePageSize={pagination.setPerPage}
                  onGoToLast={() =>
                    pagination.setPage(Math.ceil((contactList ? contactList.total_page : 0) / pagination.perPage))
                  }
                />
              </div>
            )}
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Contact;
