import React, { useEffect, useState, useRef, useMemo } from 'react';
import { useContext } from 'react';
import { TicketContext } from '../../components/notification/TicketContent';
import { Row, Col, Container, FormInput } from 'shards-react';
import { useDispatch, useSelector } from 'react-redux';
import Spinner from '../../components/common/Spinner';
import { getPosition } from '../redux/position';
import moment from 'moment';
import DiaglogPosition from '../../components/position/DialogPosition';
import { usePaginationState } from '../../hooks/use-pagination';
import { Pagination } from '../../components/common/Pagination';
import { toast_type } from '../../constants';
import ToastMsg from '../../components/common/Toast';
import Modal, { ModalContent } from '../../components/common/Modal';
const Position = () => {
  const dispatch = useDispatch();
  const dataContext = useContext(TicketContext);

  const profile = dataContext.profile;
  const positionList = useSelector(state => state.position.positionList);
  const loadingState = useSelector(state => state.position.loading);
  const { successCreate } = useSelector(state => state.position);
  const { successEdit } = useSelector(state => state.position);

  const [openModal, setOpenModal] = useState(false);
  const [data, setData] = useState('');
  const [sortConfig, setSortConfig] = useState({});
  const [listConfig, setListConfig] = useState(positionList.result);

  const pagination = usePaginationState();

  const wrapperRef = useRef(null)

  const closeModal = (e) => {
    if (e.target === wrapperRef.current) setOpenModal(false);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
    setData('');
  };
  useEffect(() => {
    setListConfig(positionList.result)
  },[positionList])
console.log('listConfig',listConfig);
  ////////////////hiển thị Phòng ban
  useEffect(() => {
    if (successEdit === true) {
      setTimeout(() => {
        dispatch(getPosition());
      }, 1000);
    }
  }, [successEdit]);

  useEffect(() => {
    if (successCreate === true) {
      setTimeout(() => {
        dispatch(getPosition());
      }, 1000);
    }
  }, [successCreate]);

  const [search, setSearch] = useState('');
  const initialValue = {
    name: search
  };
  const handleChangeSearch = e => {
    setSearch(e.target.value);
  };

  useEffect(() => {
    dispatch(
      getPosition({
        ...initialValue,
        limit: pagination.perPage,
        offset: pagination.perPage * pagination.page - pagination.perPage
      })
    );
  }, [pagination, search]);

  ////////////soft
  useMemo(() => {
    if (positionList.results === undefined) {
      return []
    }
    let sorted;
    let sortableItems = [...positionList.results];
    if (sortConfig !== null) {
      let key = sortConfig.key
      if (key === undefined) {
        key = 'name'
      }
      sorted = sortableItems.sort((a, b) => {
        if (a[key] !== undefined && b[key] !== undefined) {
          let fa = a[key].toLowerCase()
          let fb = b[key].toLowerCase()
          if (fa < fb) {
            return sortConfig.direction === 'ascending' ? -1 : 1;
          }
          if (fa > fb) {
            return sortConfig.direction === 'descending' ? 1 : -1;
          }
          return 0;
        }
      });

    }
    setListConfig(sorted);
    return sorted
  }, [positionList.results, sortConfig]);

  const requestSort = (key) => {
    let direction = 'ascending';
    if (
      sortConfig &&
      sortConfig.key === key &&
      sortConfig.direction === 'ascending'
    ) {
      direction = 'descending';
    }

    setSortConfig({ key, direction });
  };

  const getClassNamesFor = (name) => {
    if (!sortConfig) {
      return;
    }
    return name ='name' ||  sortConfig.key === name ? `${sortConfig.direction === 'ascending' ? 'fa fa-sort-down align-top' : 'fa fa-sort-up align-bottom'}` : undefined;
  };

  return (
    <>
      <Container fluid className="main-content-container px-4">
        {successCreate && <ToastMsg type={toast_type.success} />}
        {successCreate === false && <ToastMsg type={toast_type.error} msg='Thất Bại ' />}
        {successEdit && <ToastMsg type={toast_type.success} />}
        {successEdit === false && <ToastMsg type={toast_type.error} msg='Thất Bại ' />}
        {openModal && (
          <>
            <div className="modal-overlay"></div>
            <div ref={wrapperRef} onClick={closeModal} className="modal-popup_style2 open">
              <div className="popup-container pu-general">
                <h3 className="pu-heading">{!data ? 'Tạo' : 'Sửa'}</h3>
                <div className="popup-close" onClick={() => handleCloseModal()}>
                  <span className="fa fa-times"></span>
                </div>
                <DiaglogPosition
                  setUDO={setOpenModal}
                  setData={setData}
                  dataEdit={data}
                />
              </div>
            </div>
          </>
        )}
        <div className="d-flex box-top">
          <h1 className="heading">
            Danh Sách Chức vụ
            {profile && profile.is_superuser && (
              <span
                className="btn-create"
                onClick={() => {
                  setOpenModal(true);
                  setData('');
                }}>
                <i className="fa fa-plus"></i>
              </span>
            )}
          </h1>
          <div className="text-right ml-auto">
            <div className="form-search">
              <FormInput
                type="search"
                name="question"
                placeholder="Tìm kiếm"
                value={search}
                onChange={handleChangeSearch}
              />
            </div>
          </div>
        </div>

        <Row>
          <Col>
            {
              loadingState ? <Spinner loading={loadingState} /> :
                <div className="table-wrapper">
                  <table className="table mb-0">
                    <thead className="bg-light">
                      <tr>
                        <th scope="col" className="border-0" style={{ width: 40 }}>
                          STT
                        </th>
                        <th scope="col" className="border-0">
                          <button className='font-semibold' onClick={() => requestSort('name')} >Tên Chức vụ
                          <span style={{marginLeft:'5px'}} className={getClassNamesFor('name') } > </span>
                          </button>
                        </th>

                        <th scope="col" className="border-0">
                          Ngày tạo
                        </th>
                        {profile && profile.is_superuser && (
                        <th scope="col" className="border-0">
                          Hành động
                        </th>
                      )}
                      </tr>
                    </thead>
                    <tbody>
                      {listConfig && listConfig.length > 0 &&
                        listConfig.map((item, idx) => {
                          return (
                            <tr key={idx}>
                              <td className='text-center'>{idx + 1}</td>
                              <td>{item.name}</td>
                              <td>{moment(item.created_on).format('DD-MM-YYYY')}</td>
                              {profile && profile.is_superuser && (
                              <td>
                                <span
                                  className="btn btn-edit"
                                  onClick={() => {
                                    setOpenModal(true);
                                    setData(item);
                                  }}>
                                  <i className="fa fa-edit"></i>
                                </span>
                              </td>
                            )}


                          </tr>
                        );
                      })}
                  </tbody>
                </table>
                <Pagination
                  currentPage={pagination.page}
                  pageSize={pagination.perPage}
                  lastPage={Math.min(
                    Math.ceil((positionList ? positionList.count : 0) / pagination.perPage),
                    Math.ceil((positionList ? positionList.count : 0) / pagination.perPage)
                  )}
                  onChangePage={pagination.setPage}
                  onChangePageSize={pagination.setPerPage}
                  onGoToLast={() =>
                    pagination.setPage(
                      Math.ceil((positionList ? positionList.count : 0) / pagination.perPage)
                    )
                  }
                />
              </div>
            }
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Position;
