import React, { useEffect, useState, useRef, useMemo } from 'react';
import { Row, Col, Container, FormInput, Button } from 'shards-react';
import { useDispatch, useSelector } from 'react-redux';
import DialogUser from '../../components/user/DialogUser';
import Spinner from '../../components/common/Spinner';
import moment from 'moment';
import { usePaginationState } from '../../hooks/use-pagination';
import { Pagination } from '../../components/common/Pagination';
import { getUser } from '../redux/user';
import { toast_type } from '../../constants';
import ToastMsg from '../../components/common/Toast';
import Modal, { ModalContent } from '../../components/common/Modal';
const User = () => {
  const dispatch = useDispatch();
  const userList = useSelector(state => state.user.userList);
  const loadingState = useSelector(state => state.user.loading);
  const { successCreate } = useSelector(state => state.user);
  const { successEdit } = useSelector(state => state.user);

  const [openModal, setOpenModal] = useState(false);
  const [data, setData] = useState('');
  const [sortConfig, setSortConfig] = useState({});
  const [listConfig, setListConfig] = useState(userList.result);
  const [disable, setDisable] = useState()
  const pagination = usePaginationState();

  const wrapperRef = useRef(null)

  const closeModal = (e) => {
    if (e.target === wrapperRef.current) setOpenModal(false);
  };



  const handleCloseModal = () => {
    setOpenModal(false);
    setData('');
  };

  /////////////hiển thị thông tin dang nhap
  useEffect(() => {
    if (successEdit === true) {
      setTimeout(() => {
        dispatch(getUser());
      }, 1000);
    }
  }, [successEdit]);

  useEffect(() => {
    if (successCreate === true) {
      setTimeout(() => {
        dispatch(getUser());
      }, 1000);
    }
  }, [successCreate]);

  const [search, setSearch] = useState('');
  const initialValue = {
    name: search
  };
  const handleChangeSearch = e => {
    setSearch(e.target.value);
  };

  useEffect(() => {
    dispatch(getUser({
      ...initialValue,
      limit: pagination.perPage,
      offset: (pagination.perPage * pagination.page) - pagination.perPage,
    }))
  }, [pagination, search])

  useEffect(()  => {
    if (!userList.result) {
      return;
    }
    setListConfig(userList.result)
  }, [userList.result])

  const requestSort = (key) => {
    setSortConfig(s => {
      const currentDirection = s[key];
      return {...s , [key]: currentDirection === 'ascending' ? "descending" : "ascending"}
    });
    setListConfig(l => {
      let sorted = [...l]
      sorted = sorted.sort((a, b) => {
        const direction = sortConfig[key]
        if(a[key] && b[key]){
          let fa = a[key].toLowerCase();
          let fb = b[key].toLowerCase();
          if (fa < fb) {
           return  direction === 'ascending' ? -1 : 1;
          }
          if (fa > fb) {
           return  direction === 'ascending' ? 1 : -1;
          }
          return  0;
        }
      });
      return sorted;
    })
  };
  const getClassNamesFor = (key) => {
    if (!sortConfig) {
      return;
    }
    return sortConfig[key] === 'ascending' ? 'fa fa-sort-down align-top' : 'fa fa-sort-up align-bottom';
  };


  return (
    <>
      <Container fluid className="main-content-container px-4">
        {successCreate && <ToastMsg type={toast_type.success} />}
        {successCreate === false && <ToastMsg type={toast_type.error} msg='Thất Bại ' />}
        {successEdit && <ToastMsg type={toast_type.success} />}
        {successEdit === false && <ToastMsg type={toast_type.error} msg='Thất Bại ' />}
        {openModal && (
          <>
            <div className="modal-overlay"></div>
            <div ref={wrapperRef} onClick={closeModal} className="modal-popup_style2 open">
              <div className="popup-container pu-general pu-user">
                <h3 className="pu-heading">{!data ? 'Tạo' : 'Chỉnh sửa nhân viên '}</h3>
                <div className="popup-close" onClick={() => handleCloseModal()}>
                  <span className="fa fa-times"></span>
                </div>

                <DialogUser
                  setUDO={setOpenModal}
                  setData={setData}
                  dataEdit={data}
                />

              </div>
            </div>
          </>
        )}

        <Modal active={openModal} id={`${!data ? '' : `modal_${data.id}`}`}>
          <ModalContent heading={!data ? 'Tạo' : 'Sửa'} onClose={handleCloseModal}>
            <DialogUser setUDO={setOpenModal} setData={setData} dataEdit={data} />{' '}
          </ModalContent>
        </Modal>
        <div className="d-flex box-top">
          <h1 className="heading">
            Danh sách nhân viên
            <span
              className="btn-create"
              onClick={() => {
                setOpenModal(true);
                setData('');
              }}>
              <i className="fa fa-plus"></i>
            </span>
          </h1>
          <div className="text-right ml-auto">
            <div className="form-search">
              <FormInput type='search'
                name="question"
                placeholder="Tìm kiếm"
                value={search}
                onChange={handleChangeSearch}
              />
            </div>
          </div>
        </div>

        <Row>
          <Col>
            {
              loadingState ? <Spinner loading={loadingState} /> :
                <div className="table-wrapper">
                  <table className="table mb-0">
                    <thead className="bg-light">
                      <tr>
                        <th scope="col" className="border-0" style={{ width: 40 }}>
                          STT
                        </th>
                        <th scope="col" className="border-0">
                          Họ
                        </th>
                        <th scope="col" className="border-0">
                          <button className='font-semibold' onClick={() => requestSort('last_name')}>Tên
                          <span style={{marginLeft:'5px'}} className={getClassNamesFor('last_name') } > </span>
                          </button>
                        </th>
                        <th scope="col" className="border-0" style={{ width: 155 }}>
                          <button className='font-semibold' onClick={() => requestSort('username')} >Tên đăng nhập
                           <span style={{marginLeft:'5px'}} className={getClassNamesFor('username') } > </span>
                          </button>

                        </th>
                        <th scope="col" className="border-0">
                          <button className='font-semibold'  onClick={() => requestSort('email')}> Email
                           <span style={{marginLeft:'5px'}} className={getClassNamesFor('email') } > </span>
                          </button>

                        </th>
                        <th scope="col" className="border-0">
                          SĐT
                        </th>

                        <th scope="col" className="border-0">
                          Extention
                        </th>

                        <th scope="col" className="border-0" style={{ width: 120 }}>
                          Ngày tạo
                        </th>
                        <th scope="col" className="border-0">
                          Hành động
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {listConfig && listConfig.length > 0 &&
                        listConfig.map((item, idx) => {
                          return (
                            <tr key={idx}>
                              <td className='text-center'>{idx + 1}</td>
                              <td>{item.first_name}</td>
                              <td>{item.last_name}</td>
                              <td>{item.username}</td>
                              <td>{item.email}</td>
                              <td>
                                {item.phone && item.phone.phone}
                              </td>
                              <td>
                                {item.ext && item.ext.exentions}
                              </td>

                              <td>{moment(item.created_on).format('DD-MM-YYYY')}</td>
                              <td>
                                <span
                                  className="btn btn-edit"
                                  onClick={() => {
                                    setOpenModal(true);
                                    setData(item);
                                  }}>
                                  <i className="fa fa-edit"></i>
                                </span>
                              </td>
                            </tr>
                          )
                        })}

                    </tbody>
                  </table>
                  <Pagination
                    currentPage={pagination.page}
                    pageSize={pagination.perPage}
                    lastPage={Math.min(
                      Math.ceil((userList ? userList.total_page : 0) / pagination.perPage),
                      Math.ceil((userList ? userList.total_page : 0) / pagination.perPage)
                    )
                  }
                />
              </div>
            }
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default User;
