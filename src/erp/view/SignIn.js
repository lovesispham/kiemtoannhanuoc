import React from 'react';
import { ListGroup, ListGroupItem, Row, Col, FormInput, Card } from 'shards-react';
import CustomButton from '../../components/common/CustomButton';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { HelpText } from '../../components/components-overview/Help-text';
import { useDispatch, useSelector } from 'react-redux';
import { signIn } from '../redux/auth';
import { routeConstants } from '../../constants';

const SignIn = () => {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.auth.loading)
  const validationSchema = yup.object({
    email: yup
      .string()
      .trim()
      //   .email('Email không hợp lệ')
      .required('Nhập tài khoản'),
    password: yup.string().required('Bạn chưa nhập mật khẩu')
  });
  const { values, errors, handleChange, handleBlur, handleSubmit, touched } = useFormik({
    validationSchema: validationSchema,
    initialValues: {
      email: '',
      password: ''
    },
    onSubmit: async values => {
      const res = await dispatch(
        signIn({
          username: values.email,
          password: values.password
        })
      );

      if (res.payload) {
        window.location.replace(routeConstants.DASHBOARD);
        return;
      }
    }
  });

  return (
    <div className="loginform center-block w-2/6" style={{ top: '40%' }}>
      <div className="text-center">
        <img src={require('../../images/logo.png')} alt="" />
      </div>
      <h1 className="text-center heading">Đăng nhập </h1>
      <Card small>
        <ListGroup flush>
          <ListGroupItem className="p-3">
            <Row>
              <Col>
                <form onSubmit={handleSubmit} autoComplete="off">
                  <Row form>
                    <Col className="form-group">
                      <label htmlFor="feEmailAddress">Tên đăng nhập</label>
                      <FormInput
                        id="feEmailAddress"
                        name="email"
                        placeholder="Tên đăng nhập"
                        values={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        status={touched.email && errors.email ? 'error' : undefined}
                      />
                      <HelpText status="error">
                        {touched.email && errors.email ? errors.email : ''}
                      </HelpText>
                    </Col>
                  </Row>
                  <Row>
                  <Col className="form-group">
                      <label htmlFor="fePassword">Mật khẩu</label>
                      <FormInput
                        id="fePassword"
                        name="password"
                        type="password"
                        placeholder="Mật khẩu"
                        values={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        status={touched.password && errors.password ? 'error' : undefined}
                        helpText={touched.password && errors.password ? errors.password : ''}
                      />
                      <HelpText status="error">
                        {touched.password && errors.password ? errors.password : ''}
                      </HelpText>
                    </Col>
                  </Row>
                  <div className="form-input-box text-right">
                    <CustomButton
                        type="submit"
                        loading={loading}
                        icon={true}
                        >
                        Đăng nhập
                    </CustomButton>
                  </div>
                </form>
              </Col>
            </Row>
          </ListGroupItem>
        </ListGroup>
      </Card>
    </div>
  );
};

export default SignIn;
