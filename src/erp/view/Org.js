import React, { useEffect, useState } from 'react';
import { useContext } from 'react';
import { TicketContext } from '../../components/notification/TicketContent';
import { Row, Col, Container, FormInput, FormSelect } from 'shards-react';
import { useDispatch, useSelector } from 'react-redux';
import DialogUCOrg from '../../components/org/DialogUCOrg';
import DialogOrgCreate from '../../components/org/DialogOrgCreate';
import Spinner from '../../components/common/Spinner';
import { getOrg } from '../redux/org';
import { usePaginationState } from '../../hooks/use-pagination';
import { Pagination } from '../../components/common/Pagination'
import { toast_type } from '../../constants';
import ToastMsg from '../../components/common/Toast';
import Modal,{ModalContent} from '../../components/common/Modal';
const Org = () => {
  const dispatch = useDispatch();
  const dataContext = useContext(TicketContext);

  const profile = dataContext.profile;
  const orgList = useSelector(state => state.org.orgList);
  const loadingState = useSelector(state => state.org.loading);
  const { successCreate } = useSelector(state => state.org);
  const { successEdit } = useSelector(state => state.org);

  const [openModal, setOpenModal] = useState(false);

  const [data, setData] = useState('');
  const [sortConfig, setSortConfig] = useState({});
  const [listConfig, setListConfig] = useState(orgList.result);


  const pagination = usePaginationState();


  const handleCloseModal = () => {
    setOpenModal(false);
    setData('');
  };


  useEffect(() => {
    if (successEdit === true) {
      setTimeout(() => {
        dispatch(getOrg({}))
      }, 1000);
    }
  }, [successEdit]);

  useEffect(() => {
    if (successCreate === true) {
      setTimeout(() => {
        dispatch(getOrg({}))
      }, 1000);
    }
  }, [successCreate]);


  const isparentOpt = [
    {value:'', name:'Tất cả'},
    {value:false, name:'Không có đơn vị'},
    {value:true, name:'Có đơn vị'},
  ]
  const [search, setSearch] = useState('');
  const [active, setActive] = useState({is_parent:null})

  const initialValue = {
    search: search,
  };
  const handleChangeSearch = e => {
    setSearch(e.target.value);
  };


  useEffect(() => {
    dispatch(getOrg({
      ...initialValue,
    }))
  }, [search,active])

  ////////////soft
  useEffect(()  => {
    if (!orgList.result) {
      return;
    }
    setListConfig(orgList.result)
  }, [orgList.result])

  const requestSort = (key) => {
    setSortConfig(s => {
      const currentDirection = s[key];
      return {...s , [key]: currentDirection === 'ascending' ? "descending" : "ascending"}
    });
    setListConfig(l => {
      let sorted = [...l]
      sorted = sorted.sort((a, b) => {
        const direction = sortConfig[key]
        if(a[key] && b[key]){
          let fa = a[key].toLowerCase();
          let fb = b[key].toLowerCase();
          if (fa < fb) {
           return  direction === 'ascending' ? -1 : 1;
          }
          if (fa > fb) {
           return  direction === 'ascending' ? 1 : -1;
          }
          return  0;
        }
      });
      return sorted;
    })
  };
  const getClassNamesFor = (key) => {
    if (!sortConfig) {
      return;
    }
    return sortConfig[key] === 'ascending' ? 'fa fa-sort-down align-top' : 'fa fa-sort-up align-bottom';
  };
  return (
    <>
      <Container fluid className="main-content-container px-4">
        {successCreate && <ToastMsg type={toast_type.success} />}
        {successCreate === false && <ToastMsg type={toast_type.error} msg='Thất Bại ' />}
        {successEdit && <ToastMsg type={toast_type.success} />}
        {successEdit === false && <ToastMsg type={toast_type.error} msg='Thất Bại ' />}

        <Modal active={openModal} id={`${!data ? '' : `modal_${data.id}`}`}>
          <ModalContent heading={!data ? 'Tạo' : 'Sửa'} onClose={handleCloseModal}>
            {
              !data ? (
                <DialogOrgCreate
                  setUDO={setOpenModal}
                  setData={setData}
                />
              ) : (
                <DialogUCOrg
                  setUDO={setOpenModal}
                  setData={setData}
                  dataEdit={data}
                />
              )
            }
          </ModalContent>
        </Modal>

        <div className="d-flex box-top">
          <h1 className="heading">
            Danh sách đơn vị
            {profile && profile.is_superuser && (
              <span
                className="btn-create"
                onClick={() => {
                  setOpenModal(true);
                  setData('');
                }}>
                <i className="fa fa-plus"></i>
              </span>
            )}
          </h1>
          <div className="text-right d-flex ml-auto ">
            <div className="form-search">
              <FormInput type='search'
                name="name"
                placeholder="Tìm kiếm"
                value={search}
                onChange={handleChangeSearch}
              />
            </div>

          </div>
        </div>

        <Row>
          <Col>
            {
              loadingState ? <Spinner loading={loadingState} /> :
                <div className="table-wrapper">
                  <table className="table mb-0">
                    <thead className="bg-light">
                      <tr>
                        <th scope="col" className="border-0" style={{ width: 40 }}>
                          STT
                        </th>
                        <th scope="col" className="border-0" >
                          <button className='font-semibold' onClick={() => requestSort('name')}> Tên đơn vị
                            <span style={{ marginLeft: '5px' }} className={getClassNamesFor('name')} > </span></button>

                        </th>

                        {profile && profile.is_superuser && (
                        <th scope="col" className="border-0">
                          Hành động
                        </th>
                      )}
                      </tr>
                    </thead>
                    <tbody>
                      {listConfig && listConfig.length > 0 &&
                        listConfig.map((item, idx) => {
                          return (

                            <tr key={idx}>
                              <td className='text-center'>{idx + 1}</td>
                              <td>{item.name}</td>

                              {profile && profile.is_superuser && (
                              <td>
                                <span
                                  className="btn btn-edit"
                                  onClick={() => {
                                    setOpenModal(true);
                                    setData(item);
                                  }}>
                                  <i className="fa fa-edit"></i>
                                </span>
                              </td>
                            )}


                            </tr>
                          )
                        })}
                    </tbody>
                  </table>
                  <Pagination
                    currentPage={pagination.page}
                    pageSize={pagination.perPage}
                    lastPage={Math.min(
                      Math.ceil((orgList ? orgList.total_page : 0) / pagination.perPage),
                      Math.ceil((orgList ? orgList.total_page : 0) / pagination.perPage)
                    )}
                    onChangePage={pagination.setPage}
                    onChangePageSize={pagination.setPerPage}
                    onGoToLast={() => pagination.setPage(Math.ceil((orgList ? orgList.reg_count : 0) / pagination.perPage))}
                  />

                </div>
            }


          </Col>
        </Row>
      </Container>
    </>
  )
}

export default Org
