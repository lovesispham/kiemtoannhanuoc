import React, { useEffect, useState, useRef } from 'react';
import { useContext } from 'react';
import { TicketContext } from '../../components/notification/TicketContent';
import { FormSelect, FormInput, Row, Col, Container } from 'shards-react';
import { useDispatch, useSelector } from 'react-redux';
import DialogTicket from '../../components/ticket/DialogTicket';
import Spinner from '../../components/common/Spinner';
import { getSyncData, getTicket, getTicketStatus, getUUIDTicket } from '../redux/ticket';
import moment from 'moment';
import { usePaginationState } from '../../hooks/use-pagination';
import { Pagination } from '../../components/common/Pagination';
import TicketDetail from '../../components/ticket/TicketDetail';
import TicketQuest from '../../components/ticket/TicketQuest';

import Modal, { ModalContent } from '../../components/common/Modal';
import DialogHistory from '../../components/ticket/DialogHistory';
import { toast_type } from '../../constants';
import ToastMsg from '../../components/common/Toast';
import { trangthaiList, priorityList } from '../../constants';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import Report from '../../components/ticket/Report';

const Ticket = () => {
  const dataContext = useContext(TicketContext);

  const count = dataContext.count;
  const uuidNotify = dataContext.uuid;

  const [uuid, setUUID] = useState('');
  const ticketList = useSelector((state) => state.ticket.ticketList);

  const [listTicket, setListTicket] = useState({});
  const [listTicket123, setListTicket123] = useState({});

  const dispatch = useDispatch();

  const infoList = useSelector((state) => state.info.infoList);
  const loadingState = useSelector((state) => state.ticket.loading);
  const { successCreate, successEditUUID, successEdit, successRequest, loadingHistory } = useSelector(
    (state) => state.ticket
  );
  const pagination = usePaginationState();

  const [activeClear, setActiveClear] = useState(false);
  const [search, setSearch] = useState('');
  const [number, setNumber] = useState(1);
  var today = new Date();

  const [filter, setFilter] = useState({
    from: new Date(today.getFullYear(), today.getMonth(), 1),
    to: new Date(today.getFullYear(), today.getMonth() + 1, 0),
    status: '',
  });

  const initialValue = {
    search: search,
  };

  const [openModal, setOpenModal] = useState(false);
  const [pulse, setPulse] = useState(false);

  const [data, setData] = useState('');
  const listStatus = useSelector((state) => state.ticket.statusList);
  const [ListStatusFilter, setListStatusFilter] = useState([]);
  const wrapperRef = useRef(null);
  const handleCloseModal = () => {
    setOpenModal(false);
    setData('');
  };

  const closeModal = (e) => {
    if (e.target === wrapperRef.current) setOpenModal(false);
    setOpenModalInfos(false);
  };

  ///////////khoirw taoj (create)
  useEffect(() => {
    if (successCreate === true) {
      setTimeout(() => {
        dispatch(getTicket());
      }, 1000);
    }
  }, [successCreate]);

  //////////////thay doi (edit)
  useEffect(() => {
    if (successEdit === true) {
      setTimeout(() => {
        dispatch(getTicket());
      }, 1000);
    }
  }, [successEdit]);

  useEffect(() => {
    setListTicket(
      ticketList &&
        ticketList.data &&
        ticketList.data.map((v) => {
          return {
            name: v.name,
            ctFullName: v.contact && v.contact.fullname,
            ctPhone: v.contact && v.contact.mobile,
            ctOrgName: v.contact && v.contact.organization && v.contact.organization,
            ctPriority: v.priority,
            unique_id: v.unique_id,
            request_id: v.request_id,
            phone: v.phone,
            content: v.content,
            created_date: v.created_date,
            trangthai: v.trangthai && v.trangthai.name,
            priority: v.priority && v.priority.name,
          };
        })
    );
  }, [ticketList]);

  useEffect(() => {
    setListTicket123(listTicket);
  }, [listTicket]);
  useEffect(() => {
    dispatch(
      getTicket({
        limit: pagination.perPage,
        offset: pagination.perPage * pagination.page - pagination.perPage,
        ...initialValue,
      })
    );
  }, [pagination, search]);

  //   ticket detail

  useEffect(() => {
    if (successEditUUID === true) {
      setUUID('');

      dispatch(getUUIDTicket(uuid));
      dispatch(
        getTicket({
          limit: pagination.perPage,
          offset: pagination.perPage * pagination.page - pagination.perPage,
          ...initialValue,
        })
      );
    }
  }, [successEditUUID]);

  useEffect(() => {
    if (successRequest === true && uuid) {
      dispatch(getUUIDTicket(uuid));
    }
  }, [successRequest]);

  useEffect(() => {
    console.log('huu thin', uuid);
    if (uuid !== '' && uuid !== undefined && uuid !== null) {
      setTimeout(() => {
        setOpenModalTicket(true);
        setPulse(false);
      }, 3000);
      //   setData(filterDatabyuuid);
    }
  }, [uuid]);

  useEffect(() => {
    if (uuid !== '' && uuid !== undefined && uuid !== null) {
      setTimeout(() => {
        dispatch(getUUIDTicket(uuid));
      }, 1000);
      //   setData(filterDatabyuuid);
    }
  }, [uuid]);
  const [openModalTicket, setOpenModalTicket] = useState(false);

  const handleCloseModalTicket = () => {
    setOpenModalInfos(false);
    setOpenModalTicket(false);
    setUUID('');
  };

  const [openModalInfos, setOpenModalInfos] = useState(false);

  const handleOpenModalInfos = () => {
    setOpenModalInfos(!openModalInfos);
  };

  const handleChangeSearch = (e) => {
    setSearch(e.target.value);
  };

  const onFocus = (e) => {
    setActiveClear(true);
  };

  const filterTrangthai = (trangthai) => {
    const found = trangthaiList.filter((item) => item.id === parseInt(trangthai));
    return found.length && found[0].name;

    //
  };

  const filterPriority = (priority) => {
    const found = priorityList.filter((item) => item.id === parseInt(priority));
    return found.length && found[0].name;
  };

  // change Status color

  const getBackgroundColor2 = (priority2) => {
    let intStatus = parseInt(priority2);

    if (intStatus === 4) {
      return 'bg-failed';
    } else {
      return 'bg-rs';
    }
  };

  const [activeEditOrg, setEditOrg] = useState(false);
  const [activeEditContact, setEditContact] = useState(false);
  const [activeEditAssign, setEditAssign] = useState(false);

  const [openChart, setOpenChart] = useState(false);

  const [openModalTicketAsk, setOpenModalTicketAsk] = useState(false);

  // search

  const [openSearch, setOpenSearch] = useState(false);

  const handleChange = (date, name) => {
    setFilter({
      ...filter,
      [name]: date,
    });
  };

  const handleSearch = () => {
    const newValue = {
      ...filter,
      from: moment(filter.from).format('YYYY-MM-DD'),
      to: moment(filter.to).format('YYYY-MM-DD'),
    };
    dispatch(
      getTicket({
        limit: pagination.perPage,
        offset: pagination.perPage * pagination.page - pagination.perPage,

        ...newValue,
      })
    );
    setOpenSearch(!openSearch);
  };

  const handleClear = () => {
    dispatch(getTicket({}));
    setOpenSearch(!openSearch);
  };

  // modal history

  const [openModalHistory, setOpenModalHistory] = useState(false);
  const [phone, setPhone] = useState([]);

  const handleCloseModalHistory = () => {
    setOpenModalHistory(false);
  };
  const handleOpenModalHistory = (phone) => {
    setPhone(phone);
    setOpenModalHistory(!openModalHistory);
  };

  const sortTicketTime = (value) => {
    setListTicket(
      listTicket &&
        listTicket.length > 0 &&
        listTicket.sort((a, b) => {
          let x = 0;

          const name1 = new Date(a[value]);
          const name2 = new Date(b[value]);

          if (name1 > name2) {
            x = -1 * number;
          }
          if (name1 < name2) {
            x = 1 * number;
          }

          return x;
        })
    );
  };

  const sortTicket = (value) => {
    setListTicket(
      listTicket &&
        listTicket.length > 0 &&
        listTicket.sort((a, b) => {
          let x = 0;
          if (a[value]) {
            const name1 = a[value].toUpperCase();
            const name2 = b[value].toUpperCase();

            if (name1 > name2) {
              x = -1 * number;
            }
            if (name1 < name2) {
              x = 1 * number;
            }
          }

          return x;
        })
    );
  };

  useEffect(() => {
    dispatch(getTicketStatus({}));
  }, []);
  useEffect(() => {
    setListStatusFilter(
      listStatus &&
        listStatus.data &&
        listStatus.data.map((v) => ({
          name: v.name,
          id: v.id,
        }))
    );
  }, [listStatus]);

  return (
    <>
      <Container fluid className='main-content-container px-4'>
        {(successCreate || successEditUUID || successEdit || successRequest) && <ToastMsg type={toast_type.success} />}
        {(successCreate === false ||
          successEditUUID === false ||
          successEdit === false ||
          successRequest === false) && <ToastMsg type={toast_type.error} msg='Thất bại' />}
        {pulse && (
          <>
            <div className='modal-popup_style2 open'>
              <div className='modal-overlay'></div>

              <div className='popup-container'>
                <div className='dot-pulse'></div>
              </div>
            </div>
          </>
        )}
        {openModal && (
          <>
            <div className='modal-overlay'></div>
            <div ref={wrapperRef} onClick={closeModal} className='modal-popup_style2 open'>
              <div
                className={`popup-container pu-general pu-ticket ${
                  activeEditOrg || activeEditContact || activeEditAssign ? 'expand' : ''
                }`}
              >
                <h3 className='pu-heading'>Tạo Ticket</h3>
                <div className='popup-close' onClick={() => handleCloseModal()}>
                  <span className='fa fa-times'></span>
                </div>
                <DialogTicket
                  setUDO={setOpenModal}
                  openModal={openModal}
                  activeEditOrg={activeEditOrg}
                  activeEditContact={activeEditContact}
                  activeEditAssign={activeEditAssign}
                  setEditOrg={setEditOrg}
                  setEditContact={setEditContact}
                  setEditAssign={setEditAssign}
                />
              </div>
            </div>
          </>
        )}

        {openModalTicket && (
          <>
            <div className='modal-overlay'></div>

            <div className={`pu-notify-ticket ${openModalTicketAsk && count > 0 ? 'active' : ''}`}>
              <h3 className='title'>Bạn có muốn mở Ticket mới</h3>
              <div className='d-flex'>
                <span
                  className='btn'
                  onClick={() => {
                    setPulse(true);
                    setOpenModalTicket(false);
                    setUUID(uuidNotify);
                  }}
                >
                  Đồng ý
                </span>

                <span className='btn' onClick={() => setOpenModalTicketAsk(false)}>
                  Huỷ bỏ
                </span>
              </div>
            </div>

            <div className='notify-ticket' onClick={() => setOpenModalTicketAsk(!openModalTicketAsk)}>
              <div className='icon'>
                <div className='text'>
                  <span className='fa fa-bell'></span>
                </div>
                <div className='count'>{count}</div>
              </div>
            </div>
            <div className='pu-quest' onClick={() => handleOpenModalInfos()}>
              <div className='text'>
                Tài liệu
                <span className='icon'>
                  <i className='fa fa-question'></i>
                </span>
              </div>
            </div>
            <div className={`modal-popup_style2 open ${openModalInfos ? 'screen-dt' : ''}`}>
              <div className='popup-container pu-general pu-ticket style2'>
                <h3 className='pu-heading'>Chi tiết Ticket</h3>
                <div className='popup-close' onClick={() => handleCloseModalTicket()}>
                  <span className='fa fa-times'></span>
                </div>

                <TicketDetail uuid={uuid} setUDO={setOpenModalTicket} />

                {openModalInfos && (
                  <TicketQuest setUDO={setOpenModalInfos} openModalInfos={openModalInfos} infoList={infoList} />
                )}
              </div>
            </div>
          </>
        )}
        {openModalHistory && (
          <Modal active={openModalHistory}>
            <ModalContent
              className={`${!loadingHistory ? 'pu-ticket style2' : ''}`}
              heading={'Lịch sử cuộc gọi'}
              onClose={handleCloseModalHistory}
            >
              <DialogHistory setUDO={setOpenModalHistory} phone={phone} loading={loadingHistory} />
            </ModalContent>
          </Modal>
        )}
        <div className='d-flex box-top'>
          <h1 className='heading'>
            Danh sách Ticket
            <span
              className='btn-create'
              onClick={() => {
                setOpenModal(true);
                setData('');
              }}
            >
              <i className='fa fa-plus'></i>
            </span>
          </h1>
          <div className='text-right ml-auto' style={{ position: 'relative' }}>
            <div className='form-search'>
              <FormInput
                type='text'
                name='search'
                placeholder='Tìm kiếm'
                value={search}
                onChange={handleChangeSearch}
                onFocus={onFocus}
              />
              {activeClear && (
                <span onClick={() => setSearch('')} className='clear-search'>
                  <i className='fa fa-times'></i>
                </span>
              )}
            </div>
            <span
              className='btn'
              style={{ minWidth: 'inherit', marginLeft: 10, fontSize: 10, width: 30 }}
              onClick={() => setOpenSearch(!openSearch)}
            >
              <i className={`fa ${!openSearch ? 'fa-filter' : 'fa-times'}`}></i>
            </span>

            <div className={`box-search ${openSearch ? 'active' : ''}`}>
              <div className='form-input-box'>
                <span className='label-name'>Ngày bắt đầu - Ngày kết thúc</span>
                <div className='box-date'>
                  <DatePicker
                    autoComplete='off'
                    id='from'
                    name='from'
                    selected={filter.from}
                    dateFormat='dd/MM/yyyy'
                    onChange={(date) => handleChange(date, 'from')}
                  />
                  <DatePicker
                    autoComplete='off'
                    id='to'
                    name='to'
                    selected={filter.to}
                    dateFormat='dd/MM/yyyy'
                    onChange={(date) => handleChange(date, 'to')}
                  />
                </div>
              </div>
              <div className='form-input-box'>
                <div className='label-name'>Trạng Thái</div>
                <FormSelect
                  id='trangthai'
                  name='trangthai'
                  onChange={(e) => {
                    filter['status'] = e.target.value;
                  }}
                >
                  <option>Chọn</option>
                  {ListStatusFilter &&
                    ListStatusFilter.map((item, index) => {
                      return (
                        <option key={index} value={item.id}>
                          {item.name}
                        </option>
                      );
                    })}
                </FormSelect>
              </div>
              <div className='form-input-box text-right'>
                <div className='btn' onClick={() => handleClear()} style={{ marginRight: 10 }}>
                  Xoá bộ lọc
                </div>
                <div className='btn' onClick={() => handleSearch()}>
                  Tìm kiếm
                </div>
              </div>
            </div>
          </div>
        </div>
        {false ? (
          <>
            <Report />
          </>
        ) : (
          <>
            <Row>
              <Col>
                {loadingState ? (
                  <Spinner loading={loadingState} />
                ) : (
                  <div className='table-wrapper'>
                    <table className='table mb-0'>
                      <thead className='bg-light'>
                        <tr>
                          <th scope='col' className='border-0' style={{ width: 50, cursor: 'pointer' }}>
                            STT
                          </th>
                          <th
                            scope='col'
                            className='border-0'
                            style={{ width: 220, cursor: 'pointer' }}
                            onClick={() => {
                              sortTicket('name');
                              setNumber(-number);
                            }}
                          >
                            Tên ticket
                            <i class='fa fa-sort' aria-hidden='true' style={{ marginLeft: '10px' }}></i>
                          </th>
                          <th
                            scope='col'
                            className='border-0'
                            style={{ width: 220, cursor: 'pointer' }}
                            onClick={() => {
                              sortTicket('ctFullName');
                              setNumber(-number);
                            }}
                          >
                            Người liên hệ
                            <i class='fa fa-sort' aria-hidden='true' style={{ marginLeft: '10px' }}></i>
                          </th>
                          <th
                            scope='col'
                            className='border-0'
                            style={{ width: 190, cursor: 'pointer' }}
                            onClick={() => {
                              sortTicket('ctPhone');
                              setNumber(-number);
                            }}
                          >
                            Số điện thoại
                            <i class='fa fa-sort' aria-hidden='true' style={{ marginLeft: '10px' }}></i>
                          </th>
                          <th
                            scope='col'
                            className='border-0'
                            style={{ width: 190, cursor: 'pointer' }}
                            onClick={() => {
                              sortTicket('ctOrgName');
                              setNumber(-number);
                            }}
                          >
                            Đơn vị
                            <i class='fa fa-sort' aria-hidden='true' style={{ marginLeft: '10px' }}></i>
                          </th>
                          <th
                            scope='col'
                            className='border-0'
                            style={{ width: 250, cursor: 'pointer' }}
                            onClick={() => {
                              sortTicket('content');
                              setNumber(-number);
                            }}
                          >
                            Nội dung
                            <i class='fa fa-sort' aria-hidden='true' style={{ marginLeft: '10px' }}></i>
                          </th>

                          <th
                            scope='col'
                            className='border-0'
                            style={{ width: 130, cursor: 'pointer' }}
                            onClick={() => {
                              sortTicketTime('priority');
                              setNumber(-number);
                            }}
                          >
                            Ngày tạo
                            <i class='fa fa-sort' aria-hidden='true' style={{ marginLeft: '10px' }}></i>
                          </th>
                          <th
                            scope='col'
                            className='border-0'
                            style={{ width: 130, cursor: 'pointer' }}
                            onClick={() => {
                              sortTicket('priority');
                              setNumber(-number);
                            }}
                          >
                            Trạng thái
                            <i class='fa fa-sort' aria-hidden='true' style={{ marginLeft: '10px' }}></i>
                          </th>
                          <th
                            scope='col'
                            className='border-0'
                            style={{ width: 130, cursor: 'pointer' }}
                            onClick={() => {
                              sortTicket('trangthai');
                              setNumber(-number);
                            }}
                          >
                            Quyền ưu tiên
                            <i class='fa fa-sort' aria-hidden='true' style={{ marginLeft: '10px' }}></i>
                          </th>
                          <th scope='col' className='border-0' style={{ width: 90, cursor: 'pointer' }}>
                            HelpDesk
                            <i class='fa fa-sort' aria-hidden='true' style={{ marginLeft: '10px' }}></i>
                          </th>

                          <th scope='col' className='border-0' style={{ width: 130 }}>
                            Hành động
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {listTicket123 &&
                          listTicket123.length > 0 &&
                          listTicket123.map((item, idx) => {
                            return (
                              <tr key={idx}>
                                <td className='text-center'>{idx + 1}</td>
                                <td>{item.name}</td>
                                <td>{item.ctFullName}</td>
                                <td>{item.ctPhone}</td>
                                <td>{item.ctOrgName}</td>
                                <td>
                                  <div className='ticket-truncate'>{item.content}</div>
                                </td>

                                <td>{moment(item.created_date).format('DD-MM-YYYY')}</td>
                                <td>{item.trangthai}</td>
                                <td
                                  style={{
                                    color:
                                      item.priority === 'Medium'
                                        ? '#0099FF'
                                        : item.priority === 'High'
                                        ? 'red'
                                        : '#FF9900',
                                  }}
                                >
                                  {item.priority}
                                </td>
                                <td>
                                  <i
                                    class='fas fa-check'
                                    style={{ color: item.request_id === '0' || item.request_id === null ? 'red' : '' }}
                                  ></i>
                                </td>
                                <td>
                                  <div className='d-flex'>
                                    <span
                                      className='btn btn-edit'
                                      onClick={() => {
                                        setPulse(true);
                                        setUUID(item.unique_id);
                                      }}
                                    >
                                      <i className='fa fa-edit'></i>
                                    </span>

                                    <span
                                      className='btn'
                                      style={{ minWidth: 'inherit', width: 30, marginLeft: 5 }}
                                      onClick={() => {
                                        console.log('item.phone', item.ctPhone);
                                        handleOpenModalHistory(item.ctPhone);
                                      }}
                                    >
                                      <i className='fa fa-history'></i>
                                    </span>
                                  </div>
                                </td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                    <Pagination
                      currentPage={pagination.page}
                      pageSize={pagination.perPage}
                      lastPage={Math.min(
                        Math.ceil((ticketList ? ticketList.data_count : 0) / pagination.perPage),
                        Math.ceil((ticketList ? ticketList.data_count : 0) / pagination.perPage)
                      )}
                      onChangePage={pagination.setPage}
                      onChangePageSize={pagination.setPerPage}
                      onGoToLast={() =>
                        pagination.setPage(Math.ceil((ticketList ? ticketList.data_count : 0) / pagination.perPage))
                      }
                    />
                  </div>
                )}
              </Col>
            </Row>
          </>
        )}
      </Container>
    </>
  );
};

export default Ticket;
