import React, { useEffect, useState } from 'react';
import { useContext } from 'react';
import { TicketContext } from '../../components/notification/TicketContent';
import { Row, Col, Container, FormInput } from 'shards-react';
import { useDispatch, useSelector } from 'react-redux';
import DialogInfo from '../../components/info/DialogInfo';

import { getInfo } from '../redux/info';
import moment from 'moment';
import { usePaginationState } from '../../hooks/use-pagination';
import { Pagination } from '../../components/common/Pagination';
import { toast_type } from '../../constants';
import ToastMsg from '../../components/common/Toast';
import Modal,{ModalContent} from '../../components/common/Modal';
import Spinner from '../../components/common/Spinner';
const Info = () => {  const dispatch = useDispatch();
  const dataContext = useContext(TicketContext);

  const profile = dataContext.profile;
  const infoList = useSelector(state => state.info.infoList);
  const loadingState = useSelector(state => state.info.loading);
  const { successEdit } = useSelector(state => state.info);
  const { successCreate } = useSelector(state => state.info);

  const [openModal, setOpenModal] = useState(false);
  const [data, setData] = useState('');

  const pagination = usePaginationState();
  const handleCloseModal = () => {
    setOpenModal(false);
    setData('');
  };


  useEffect(() => {
    if (successEdit === true) {
      setTimeout(() => {
        dispatch(getInfo({}));
      }, 1000);
    }
  }, [successEdit]);

  useEffect(() => {
    if (successCreate === true) {
      setTimeout(() => {
        dispatch(getInfo({}));
      }, 1000);
    }
  }, [successCreate]);

  const [search, setSearch] = useState('');
  const initialValue = {
    question: search
  };
  const handleChangeSearch = e => {
    setSearch(e.target.value);
  };

  useEffect(() => {
    dispatch(
      getInfo({
        ...initialValue,
        limit: pagination.perPage,
        offset: pagination.perPage * pagination.page - pagination.perPage
      })
    );
  }, [pagination, search]);
  return (
    <>
      <Container fluid className="main-content-container px-4">
        {successCreate && <ToastMsg type={toast_type.success} />}
        {successCreate === false && <ToastMsg type={toast_type.error} msg="Thất Bại " />}
        {successEdit && <ToastMsg type={toast_type.success} />}
        {successEdit === false && <ToastMsg type={toast_type.error} msg="Thất Bại " />}

        <Modal active={openModal} id={`${!data ? '' : `modal_${data.id}`}`}>
          <ModalContent heading={!data ? 'Tạo' : 'Sửa'} onClose={handleCloseModal}>
            <DialogInfo setUDO={setOpenModal} setData={setData} dataEdit={data} />
          </ModalContent>
        </Modal>
        <div className="d-flex box-top">
          <h1 className="heading">
            Danh sách tài liệu
            {
              profile && profile.is_superuser && (
                 <span
              className="btn-create"
              onClick={() => {
                setOpenModal(true);
                setData('');
              }}>
              <i className="fa fa-plus"></i>
            </span>
              )
            }

          </h1>
          <div className="text-right ml-auto">
            <div className="form-search">
              <FormInput
                type="search"
                name="question"
                placeholder="Tìm kiếm"
                value={search}
                onChange={handleChangeSearch}
              />
            </div>
          </div>
        </div>

        <Row>
          <Col>
            {loadingState ? (
              <Spinner loading={loadingState} />
            ) : (
              <div className="table-wrapper">
                <table className="table mb-0">
                  <thead className="bg-light">
                    <tr>
                      <th scope="col" className="border-0" style={{ width: 40 }}>
                        STT
                      </th>
                      <th scope="col" className="border-0" style={{ width: 300 }}>
                        Câu hỏi
                      </th>
                      <th scope="col" className="border-0" style={{ width: 500 }}>
                        Câu trả lời
                      </th>

                      <th scope="col" className="border-0" style={{ width: 120 }}>
                        Ngày tạo
                      </th>
                      {
                        profile && profile.is_superuser && (
                                                <th scope="col" className="border-0">
                        Hành động
                      </th>
                        )
                      }

                    </tr>
                  </thead>
                  <tbody>
                    {infoList && infoList.data &&
                      infoList.data.length > 0 &&
                      infoList.data.map((item, i) => {
                        return (
                          <tr key={i}>
                            <td className="text-center">{i + 1}</td>
                            <td>{item.question}</td>
                            <td>
                              <div className="ticket-truncate">{item.answer}</div>
                            </td>
                            <td>{moment(item.created_on).format('DD-MM-YYYY')}</td>
                            {
                              profile && profile.is_superuser && (
                                <td>
                              <span
                                className="btn btn-edit"
                                onClick={() => {
                                  setOpenModal(true);
                                  setData(item);
                                }}>
                                <i className="fa fa-edit"></i>
                              </span>
                            </td>
                              )
                            }

                          </tr>
                        );
                      })}
                  </tbody>
                </table>
                <Pagination
                  currentPage={pagination.page}
                  pageSize={pagination.perPage}
                  lastPage={Math.min(
                    Math.ceil((infoList ? infoList.data_count : 0) / pagination.perPage),
                    Math.ceil((infoList ? infoList.data_count : 0) / pagination.perPage)
                  )}
                  onChangePage={pagination.setPage}
                  onChangePageSize={pagination.setPerPage}
                  onGoToLast={() =>
                    pagination.setPage(
                      Math.ceil((infoList ? infoList.data_count : 0) / pagination.perPage)
                    )
                  }
                />
              </div>
            )}
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Info;
