import React from 'react'
import { Pagination } from '../../components/common/Pagination';
import { useDispatch,useSelector } from 'react-redux';
import { useEffect } from 'react';
import { getdepartment, selectListDepartment } from '../redux/department';
import { useState } from 'react';
import { Col, Container, FormInput, Row } from 'shards-react';
import DialogUCOrgDQ from '../../components/department/DialogUCOrgDQ';
import Modal,{ModalContent} from '../../components/common/Modal';
import Toast from '../../components/common/Toast';

const Department = () => {
  const dispatch = useDispatch()
  const Department = useSelector(selectListDepartment)
  const [listDepartment,setDepartment] = useState([])
  const [name,setName] = useState()
  const requestSort = (key) => {}
  const getClassNamesFor = (key) => {}
  const [openModal,setOpenModal] = useState((false))
  const [data,setData] = useState()
  const [success,setSuccsee] = useState()
  const [action,setAction] = useState();
useEffect(()=> {
  setDepartment(Department.result)
},[Department])

  useEffect(() => {
dispatch(getdepartment({}))
  },[])

const seachChange = (e) => {
  setName(e.target.value)
}
const handleCloseModal = () => {
  setOpenModal(false)
}
useEffect(()=> {
  if(!openModal) {
    setSuccsee('success')

    setTimeout(()=> {
      dispatch(getdepartment({}))
    setData({})

    },1000)
  }

},[openModal])
  return (
    <div>
       {success && data ==='1'&& <Toast msg={'thanh công'} type={success} />}
      <Modal active={openModal} >
          <ModalContent heading={!data ? 'Tạo' : 'Sửa'} onClose={handleCloseModal}>
            {
              action ? (
                <DialogUCOrgDQ
                  setUDO={setOpenModal}
                  setData={setData}
                  action={action}
                />
              ) : (
                <DialogUCOrgDQ
                  setUDO={setOpenModal}
                  setData={setData}
                  dataEdit={data}
                />
              )
            }
          </ModalContent>
        </Modal>
          <Container fluid className="main-content-container px-4">
          <div className="d-flex box-top">
          <h1 className="heading">
            Danh Sách Phòng Ban
            {true && (
              <span
                className="btn-create"
                onClick={() => {
                  setOpenModal(true);
                  // setData('');
                  setAction(true)
                }}>
                <i className="fa fa-plus"></i>
              </span>
            )}
          </h1>
          <div className="text-right ml-auto">
            <div className="form-search">
              <FormInput
                type="search"
                name="question"
                placeholder="Tìm kiếm"
                value={name}
                onChange={seachChange}
              />
            </div>
          </div>
        </div>
          <Row>
           <Col>
                 <div className="table-wrapper">
                  <table className="table mb-0">
                    <thead className="bg-light">
                      <tr>
                        <th scope="col" className="border-0" style={{ width: 40 }}>
                          STT
                        </th>


                        <th scope="col" className="border-0">
                          Tên Phòng Ban
                        </th>
                        {true && (
                        <th scope="col" className="border-0">
                          Hành động
                        </th>
                      )}
                      </tr>
                    </thead>
                    <tbody>
                      {listDepartment && listDepartment.length > 0 &&
                        listDepartment.map((item, idx) => {
                          return (
                            <tr key={idx}>
                              <td className='text-center'>{idx + 1}</td>
                              <td>{item.name}</td>

                              {true && (
                              <td>
                                <span
                                  className="btn btn-edit"
                                  onClick={() => {
                                    setOpenModal(true);
                                    setData({name : item.name,id:item.id});
                                    setAction(false)
                                  }}>
                                  <i className="fa fa-edit"></i>
                                </span>
                              </td>
                            )}


                          </tr>
                        );
                      })}
                  </tbody>
                </table>
                {/* <Pagination
                  currentPage={pagination.page}
                  pageSize={pagination.perPage}
                  lastPage={Math.min(
                    Math.ceil((positionList ? positionList.count : 0) / pagination.perPage),
                    Math.ceil((positionList ? positionList.count : 0) / pagination.perPage)
                  )}
                  onChangePage={pagination.setPage}
                  onChangePageSize={pagination.setPerPage}
                  onGoToLast={() =>
                    pagination.setPage(
                      Math.ceil((positionList ? positionList.count : 0) / pagination.perPage)
                    )
                  }
                /> */}
              </div>
              </Col>
              </Row>
              </Container>
    </div>
  )
}

export default Department
