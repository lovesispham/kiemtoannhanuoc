import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axiosInstance from '../service/config';

const initialState = {
  infoList: [],

  successEdit:null,
  successCreate:null,
};

const ACTION = {
  GET_INFO: 'info/getInfo',
  CREATE_INFO:'info/createInfo',
  EDIT_INFO:'info/editInfo',
};

export const getInfo = createAsyncThunk(ACTION.GET_INFO, async body => {
  let params = "";
  const { question, limit,offset} = body;
  params += limit !== "" && limit !== undefined ? `limit=${limit}&`: ""
  params += offset !== "" && offset !== undefined ? `offset=${offset}&`: ""
  params += question !== "" && question !== undefined ? `question=${question}&`: ""
  return axiosInstance.get('/api/infos/?' + params);
});
export const createInfo = createAsyncThunk(ACTION.CREATE_INFO, async body => {
  let params = "";
  const { question, answer} = body;
  params += question !== "" && question !== undefined ? `question=${question}&`: ""
  params += answer !== "" && answer !== undefined ? `answer=${answer}&`: ""

  return axiosInstance.post('/api/infos/?' + params);
});

export const editInfo = createAsyncThunk(ACTION.EDIT_INFO, async body => {
  let params = "";
  const { question, answer} = body;
  params += question !== "" && question !== undefined ? `question=${question}&`: ""
  params += answer !== "" && answer !== undefined ? `answer=${answer}&`: ""


  return axiosInstance.put(`/api/infos/${body.id}/?` + params);
});

const infoSlice = createSlice({
  name: 'info',
  initialState: initialState,
  extraReducers: {
    [getInfo.pending.toString()]: state => {
      state.loading = true;
    },
    [getInfo.rejected.toString()]: state => {
      state.loading = false;
    },
    [getInfo.fulfilled.toString()]: (state, action) => {
      state.loading = false;
      state.successEdit = null;
      state.successCreate = null;
      state.infoList = action.payload;
    },
    [createInfo.pending.toString()]: state => {
    },
    [createInfo.rejected.toString()]: (state,action) => {
      state.loading = false;
      state.successCreate = false
    },
    [createInfo.fulfilled.toString()]: (state, action) => {
      state.loading = false;
      state.successCreate = true
    },
    [editInfo.pending.toString()]: state => {
    },
    [editInfo.rejected.toString()]: (state) => {
      state.successEdit = false;
    },
    [editInfo.fulfilled.toString()]: state => {
      state.successEdit = true;
    },
  }
});

const { reducer: infoReducer } = infoSlice;
export default infoReducer;
