import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';


import axiosInstance from '../service/config';

const initialState = {
loading:false,
listDepartment:''
};

export const getdepartment= createAsyncThunk('department', async body => {
  let params="";

  return axiosInstance.get('/api/department/'+ params, {
      params: body
  })
})

export const putDepartmetn = createAsyncThunk('put/department' ,async ({id,body},{ rejectWithValue }) => {
  return axiosInstance.put(`/api/department/${id}/?` , body);
})
export const postDepartmetn = createAsyncThunk('post',async (body) => {
  return axiosInstance.post('/api/department/',body)
})

const  departmentSlice = createSlice({
  name: 'department',
  initialState: initialState,
  extraReducers: {
      [getdepartment.pending.toString()]: state => {
          state.loading = true;
      },
      [getdepartment.rejected.toString()]: state => {
          state.loading = false;
      },
      [getdepartment.fulfilled.toString()]: (state, action) => {
          state.loading = false;
          state.listDepartment = action.payload

      },

  }
});

const { reducer: departmentReducer } = departmentSlice;
 export const selectListDepartment = (state) => state.department.listDepartment
export default departmentReducer;
