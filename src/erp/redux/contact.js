import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axiosInstance from '../service/config';

const initialState = {
  contactList: [],
  contactId:[],
  successEdit:null,
  successCreate:null,
};

const ACTION = {
  GET_CONTACT: 'contact/getContact',
  GET_CONTACT_ID:'org/getContactId',
  CREATE_CONTACT:'contact/createContact',
  EDIT_CONTACT:'contact/editContact',
};


export const getContact = createAsyncThunk(ACTION.GET_CONTACT, async body => {
  let params = "";
  const { name,limit,offset} = body;
  params += name !== "" && name !== undefined ? `name=${name}&`: ""
  params += offset !== "" && offset !== undefined ? `offset=${offset}&`: ""
  params += limit !== "" && limit !== undefined ? `limit=${limit}&`: ""
  return axiosInstance.get(`/api/contacts/?${params}`)
});

export const getContactId = createAsyncThunk(ACTION.GET_CONTACT_ID, async body => {
  return axiosInstance.get(`/api/contacts/${body}/`);
});


export const createContact = createAsyncThunk(ACTION.CREATE_CONTACT, async body => {
  let params = "";
  const { name, fullname, phone, email, position, org} = body;
  params += name !== "" && name !== undefined ? ` fullname=${name}&`: ""
  params += fullname !== "" && fullname !== undefined ? `fullname=${fullname}&`: ""
  params += phone !== "" && phone !== undefined ? `phone=${phone}&`: ""
  params += email !== "" && email !== undefined ? `email=${email}&`: ""
  params += org !== "" && org !== undefined ? `org=${org}&`: ""
  params += position !== "" && position !== undefined ? `position=${position}&`: ""

  return axiosInstance.post('/api/contacts/?' + params, body);

});

export const editContact = createAsyncThunk(ACTION.EDIT_CONTACT, async body => {
  let params = "";
  const { name, fullname, phone, email, position, org} = body;
  params += name !== "" && name !== undefined ? ` fullname=${name}&`: ""
  params += fullname !== "" && fullname !== undefined ? `fullname=${fullname}&`: ""
  params += phone !== "" && phone !== undefined ? `phone=${phone}&`: ""
  params += email !== "" && email !== undefined ? `email=${email}&`: ""
  params += position !== "" && position !== undefined ? `position=${position}&`: ""
  params += org !== "" && org !== undefined ? `org=${org}&`: ""

  return axiosInstance.put(`/api/contacts/${body.id}/?` + params);
});

const contactSlice = createSlice({
  name: 'contact',
  initialState: initialState,
  extraReducers: {
    [getContact.pending.toString()]: state => {
      state.loading = true;
    },
    [getContact.rejected.toString()]: state => {
      state.loading = false;
    },
    [getContact.fulfilled.toString()]: (state, action) => {
      state.loading = false;
      state.successCreate=null;
      state.successEdit=null;
      state.contactList = action.payload;
    },
    [getContactId.pending.toString()]: state => {
      state.loading = true;
    },
    [getContactId.rejected.toString()]: state => {
      state.loading = false;
    },
    [getContactId.fulfilled.toString()]: (state, action) => {
      state.loading = false;
      state.contactId = action.payload.data;
    },
    [createContact.pending.toString()]: state => {
    },
    [createContact.rejected.toString()]: (state,action) => {
      state.loading = false;
      state.successCreate = false
    },
    [createContact.fulfilled.toString()]: (state, action) => {
      state.loading = false;
      state.successCreate = true
    },
    [editContact.pending.toString()]: state => {
    },
    [editContact.rejected.toString()]: (state) => {
      state.successEdit = false;
    },
    [editContact.fulfilled.toString()]: state => {
      state.successEdit = true;
    },
  }
});

const { reducer: contactReducer } = contactSlice;
export default contactReducer;
