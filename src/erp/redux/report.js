import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import axiosInstance from '../service/config';

const initialState = {
  report: [],
};

const ACTION = {
  GET_REPORT: 'report/getReport',
};

export const getReport = createAsyncThunk(ACTION.GET_REQUEST, async (params) => {
  return axiosInstance.get('/api/report/', { params });
});
export const exportReport = createAsyncThunk('export', async (data) => {
  return axiosInstance.post('/api/report/', data, {
    responseType: 'blob',
  });
});

const reportSlice = createSlice({
  name: 'Report',
  initialState: initialState,
  extraReducers: {
    [getReport.pending.toString()]: (state) => {
      state.loading = true;
    },
    [getReport.rejected.toString()]: (state) => {
      state.loading = false;
    },
    [getReport.fulfilled.toString()]: (state, action) => {
      state.loading = false;
      state.report = action.payload;
    },
  },
});

const { reducer: reportReducer } = reportSlice;
export default reportReducer;
