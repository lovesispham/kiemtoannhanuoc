import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axiosInstance from '../service/config';

const initialState = {
  userList: [],

  successEdit: null,
  successCreate: null,
};

const ACTION = {
  GET_USER: 'user/getUser',
  CREATE_USER: 'user/createUser',
  EDIT_USER: 'user/editUser',
};

export const getUser = createAsyncThunk(ACTION.GET_USER, async (body) => {
  return axiosInstance.get('/api/users/', {
    params: body,
  });
});

export const createUser = createAsyncThunk(ACTION.CREATE_USER, async (body) => {
  let params = '';
  const { username, password, email, first_name, last_name, position, extention, phone, technician } = body;
  params += username !== '' && username !== undefined ? `username=${username}&` : '';
  params += password !== '' && password !== undefined ? `password=${password}&` : '';
  params += email !== '' && email !== undefined ? `email=${email}&` : '';
  params += first_name !== '' && first_name !== undefined ? `first_name=${first_name}&` : '';
  params += last_name !== '' && last_name !== undefined ? `last_name=${last_name}&` : '';
  params += position !== '' && position !== undefined ? `position=${position}&` : '';
  params += extention !== '' && extention !== undefined ? `extention=${extention}&` : '';
  params += phone !== '' && phone !== undefined ? `phone=${phone}&` : '';
  params += technician !== '' && technician !== undefined ? `technician=${technician}&` : '';

  return axiosInstance.post('/api/users/?' + params);
});

export const editUser = createAsyncThunk(ACTION.EDIT_USER, async (body) => {
  let params = '';
  const { username, password, email, first_name, last_name, position, extention, phone, technician } = body;
  params += username !== '' && username !== undefined ? `username=${username}&` : '';
  params += password !== '' && password !== undefined ? `password=${password}&` : '';
  params += email !== '' && email !== undefined ? `email=${email}&` : '';
  params += first_name !== '' && first_name !== undefined ? `first_name=${first_name}&` : '';
  params += last_name !== '' && last_name !== undefined ? `last_name=${last_name}&` : '';
  params += position !== '' && position !== undefined ? `position=${position}&` : '';
  params += extention !== '' && extention !== undefined ? `extention=${extention}&` : '';
  params += phone !== '' && phone !== undefined ? `phone=${phone}&` : '';
  params += technician !== '' && technician !== undefined ? `technician=${technician}&` : '';
  return axiosInstance.put(`/api/users/${body.id}/?` + params);
});

const userSlice = createSlice({
  name: 'user',
  initialState: initialState,
  extraReducers: {
    [getUser.pending.toString()]: (state) => {
      state.loading = true;
    },
    [getUser.rejected.toString()]: (state) => {
      state.loading = false;
    },
    [getUser.fulfilled.toString()]: (state, action) => {
      state.loading = false;
      state.successCreate = null;
      state.successEdit = null;
      state.userList = action.payload;
    },
    [createUser.pending.toString()]: (state) => {},
    [createUser.rejected.toString()]: (state) => {
      state.successCreate = false;
    },
    [createUser.fulfilled.toString()]: (state, action) => {
      state.successCreate = true;
    },
    [createUser.pending.toString()]: (state) => {},
    [createUser.rejected.toString()]: (state, action) => {
      state.loading = false;
      state.successCreate = false;
    },
    [createUser.fulfilled.toString()]: (state, action) => {
      state.loading = false;
      state.successCreate = true;
    },
    [editUser.pending.toString()]: (state) => {},
    [editUser.rejected.toString()]: (state) => {
      state.successEdit = false;
    },
    [editUser.fulfilled.toString()]: (state) => {
      state.successEdit = true;
    },
  },
});

const { reducer: userReducer } = userSlice;
export default userReducer;
