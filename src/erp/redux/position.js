import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axiosInstance from '../service/config';

const initialState = {
    positionList: [],

    successEdit: null,
    successCreate: null,
};

const ACTION = {
    GET_POSITION: 'position/getPosition',
    CREATE_POSITION: 'position/createPosition',
    EDIT_POSITION: 'position/editPosition',
};


export const getPosition = createAsyncThunk(ACTION.GET_POSITION, async body => {
    let params="";

    return axiosInstance.get('/api/positions/'+ params, {
        params: body
    })
})
export const createPosition = createAsyncThunk(ACTION.CREATE_POSITION, async body => {
    let params = "";
    const { name } = body;
    params += name !== '' && name !== undefined ? `name=${name}` : ''

    return axiosInstance.post('/api/positions/?', body)
})

export const editPosition = createAsyncThunk(ACTION.EDIT_POSITION, async body => {
    let params = "";
    const { position } = body
    params += position !== '' && position !== undefined ? `position=${position}` : ''

    return axiosInstance.put(`/api/positions/${body.id}/?` ,body)
})

const positionSlice = createSlice({
    name: 'position',
    initialState: initialState,
    extraReducers: {
        [getPosition.pending.toString()]: state => {
            state.loading = true;
        },
        [getPosition.rejected.toString()]: state => {
            state.loading = false;
        },
        [getPosition.fulfilled.toString()]: (state, action) => {
            state.loading = false;
            state.successCreate = null;
            state.successEdit = null;
            state.positionList = action.payload;
        },
        [createPosition.pending.toString()]: state => {
        },
        [createPosition.rejected.toString()]: (state, action) => {
            state.loading = false;
            state.successCreate = false
        },
        [createPosition.fulfilled.toString()]: (state, action) => {
            state.loading = false;
            state.successCreate = true
        },
        [editPosition.pending.toString()]: state => {
        },
        [editPosition.rejected.toString()]: (state) => {
            state.successEdit = false;
        },
        [editPosition.fulfilled.toString()]: state => {
            state.successEdit = true;
        },
    }
});

const { reducer: positionReducer } = positionSlice;
export default positionReducer;
