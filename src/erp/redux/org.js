import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axiosInstance from '../service/config';

const initialState = {
  orgList: [],
  orgListFull: [],
  orgId:[],
  successEdit:null,
  successCreate:null,
};

const ACTION = {
  GET_ORG: 'org/getOrg',
  GET_ORGFULL: 'org/getOrgFull',
  GET_ORG_ID:'org/getOrgId',

  CREATE_ORG:'org/createOrg',
  EDIT_ORG:'org/editOrg',
};


export const getOrg = createAsyncThunk(ACTION.GET_ORG, async body => {
  // let params = "";
  // const { search} = body;
  // params += search !== "" && search !== undefined ? `search=${search}&`: ""
  return axiosInstance.get(`/api/org/?`,{ params: body });
});

export const getOrgFull = createAsyncThunk(ACTION.GET_ORGFULL, async body => {
  // let params = "";
  // const { search} = body;
  // params += search !== "" && search !== undefined ? `search=${search}&`: ""
  return axiosInstance.get(`/api/organizations/?`,body);
});

export const getOrgId = createAsyncThunk(ACTION.GET_ORG_ID, async body => {
  return axiosInstance.get(`/api/organizations/${body}/`);
});



export const createOrg = createAsyncThunk(ACTION.CREATE_ORG, async body => {


  return axiosInstance.post('/api/org/?' , body);
});

export const editOrg = createAsyncThunk(ACTION.EDIT_ORG, async ( {id,body}) => {


  return axiosInstance.put(`/api/org/${id}/?` , body);
});

const orgSlice = createSlice({
  name: 'org',
  initialState: initialState,
  extraReducers: {
    [getOrgFull.pending.toString()]: state => {
    },
    [getOrgFull.rejected.toString()]: state => {
    },
    [getOrgFull.fulfilled.toString()]: (state, action) => {

      state.orgListFull = action.payload;
    },
    [getOrg.pending.toString()]: state => {
      state.loading = true;
    },
    [getOrg.rejected.toString()]: state => {
      state.loading = false;
    },
    [getOrg.fulfilled.toString()]: (state, action) => {
      state.loading = false;
      state.successCreate=null;
      state.successEdit=null;
      state.orgList = action.payload;
    },

    [getOrgId.pending.toString()]: state => {
      state.loading = true;
    },
    [getOrgId.rejected.toString()]: state => {
      state.loading = false;
    },
    [getOrgId.fulfilled.toString()]: (state, action) => {
      state.loading = false;
      state.orgId = action.payload.data;
    },
    [createOrg.pending.toString()]: state => {
    },
    [createOrg.rejected.toString()]: (state,action) => {
      // state.loading = false;
      state.successCreate = false
    },
    [createOrg.fulfilled.toString()]: (state, action) => {
      // state.loading = false;
      state.successCreate = true
    },
    [editOrg.pending.toString()]: state => {
    },
    [editOrg.rejected.toString()]: (state) => {
      state.successEdit = false;
    },
    [editOrg.fulfilled.toString()]: state => {
      state.successEdit = true;
    },
  }
});

const { reducer: orgReducer } = orgSlice;
export default orgReducer;
