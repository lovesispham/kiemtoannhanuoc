import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axiosInstance from '../service/config';

const initialState = {
    cityList: [],
    districtList: [],
    wardList: [],
    detailWard:[], 

    successEdit: null,
    successCreate: null,
};

const ACTION = {
    GET_CITY: 'city/getCity',
    GET_DISTRICT: 'district/getDistrict',
    GET_WARD: 'ward/getWard',
    GET_DETAIL_WARD: 'ward/getDetailWard',
};

export const getCity = createAsyncThunk(ACTION.GET_CITY, async body => {

    return axiosInstance.get('/api/cities/', {
        params: body
      });

});

export const getDistrict = createAsyncThunk(ACTION.GET_DISTRICT, async body => {
   

    return axiosInstance.get(`/api/cities/${body}` )
});
   
export const getWard = createAsyncThunk(ACTION.GET_WARD, async body => {

    return axiosInstance.get(`/api/districts/${body}/`);
    
});

export const getDetailWard = createAsyncThunk(ACTION.GET_DETAIL_WARD, async body => {
    return axiosInstance.get(`/api/wards/${body}/`);
    
});



const locationSlice = createSlice({
    name: 'city',
    initialState: initialState,
    extraReducers: {
        [getCity.pending.toString()]: state => {
            state.loading = true;
        },
        [getCity.rejected.toString()]: state => {
            state.loading = false;
        },
        [getCity.fulfilled.toString()]: (state, action) => {
            state.loading = false;
            state.successCreate = null;
            state.successEdit = null;
            state.cityList = action.payload.data;
        },
        [getDistrict.pending.toString()]: state => {
            state.loading = true;
        },
        [getDistrict.rejected.toString()]: state => {
            state.loading = false;
        },
        [getDistrict.fulfilled.toString()]: (state, action) => {
            state.loading = false;
            state.successCreate = null;
            state.successEdit = null;
            state.districtList = action.payload.data;
        },
        [getWard.pending.toString()]: state => {
            state.loading = true;
        },
        [getWard.rejected.toString()]: state => {
            state.loading = false;
        },
        [getWard.fulfilled.toString()]: (state, action) => {
            state.loading = false;
            state.successCreate = null;
            state.wardList = action.payload.data;
        },
        [getDetailWard.pending.toString()]: state => {
            state.loading = true;
        },
        [getDetailWard.rejected.toString()]: state => {
            state.loading = false;
        },
        [getDetailWard.fulfilled.toString()]: (state, action) => {
            state.loading = false;
            state.successCreate = null;
            state.detailWard = action.payload.data;
        },
    }
});

const { reducer: locationReducer } = locationSlice;
export default locationReducer;
