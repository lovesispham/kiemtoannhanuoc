import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axiosInstance from '../service/config';
import { removeTokenCookie } from '../../erp/helps/storage';
import Cookies from 'js-cookie';
import jwt_decode from 'jwt-decode';


const initialState = {
  accounts: []
};

const ACTION = {
  GET_ACCOUNTS: 'account/getAccount'
};

const logout = () => { 
 
  // return removeAllCookieStorage(['access_token' ]) 
  // return Cookies.remove('access_token', access_token);
  return removeTokenCookie()
  // console.log(access_token)
}

// export const getAccount1 = (access_token, refresh_token) => {
//   console.log(access_token)

//        removeAllCookieStorage([
//       {  access_token },
//       // { key: 'refresh_token', value: refresh_token },
//       // { key: 'expire_refresh_token', value: expRefreshToken },
//       // { key: 'expire_token', value: expToken }
//     ]);
 
// };

export const getAccount = createAsyncThunk(ACTION.GET_ACCOUNTS, async body => {
  return logout()
});

const accountSlice = createSlice({
  name: 'account',
  initialState: initialState,
  extraReducers: {
    [getAccount.pending.toString()]: state => {
      state.loading = true;
    },
    [getAccount.rejected.toString()]: state => {
      state.loading = false;
    },
    [getAccount.fulfilled.toString()]: (state, action) => {
      state.loading = false;
      state.accounts = action.payload;
    }
  }
});

const { reducer: accountReducer } = accountSlice;
export default accountReducer;
