import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import axiosInstance from '../service/config';

const initialState = {
  ticketList: [],
  ticketUUID: [],
  technicanList: [],
  requestList: [],
  historyList: [],
  siteList: [],
  prirotyList: [],
  categoryList: [],
  groupList: [],
  modeList: [],
  listSubCategory: [],
  itemList: [],
  requesterList: [],
  statusList: [],
  groupList: [],
  successEdit: null,
  successCreate: null,
  successEditUUID: null,
  successRequest: null,
};

const ACTION = {
  GET_TICKET: 'ticket/getTicket',
  GET_UUID_TICKET: 'ticket/getUUIDTicket',
  CREATE_TICKET: 'ticket/createTicket',
  EDIT_TICKET: 'ticket/editTicket',
  EDIT_UUID_TICKET: 'ticket/editTicketUUID',
  REQUEST_TICKET: 'ticket/requestTicket',
  GET_TECHNICAN: 'ticket/getTechnican',
  GET_REQUEST: 'ticket/getRequest',
  GET_HISTORY: 'ticket/getHistory',
  GET_SITE: 'ticket/site',
  GET_PRIRORITY: 'ticket/PRIRORITY',
  GET_CATEGORY: 'ticket/catagory',
  GET_GROUP: 'ticket/group',
  GET_MODE: 'ticket/mode',
  GET_SUB_CATEGORY: 'ticket/sub_category',
  GET_ITEM: 'ticket/item',
  GET_REQUESTER: 'ticket/requestes',
  GET_STATUS: 'ticket/status',
};

export const getTicket = createAsyncThunk(ACTION.GET_TICKET, async (body) => {
  return axiosInstance.get('/api/tickets/', {
    params: body,
  });
});

// export const getStatus = createAsyncThunk('get/status', async (data) => {
//   return axiosInstance.get(`/tickets/statuses`, {
//     params: body,
//   });
// });

export const getUUIDTicket = createAsyncThunk(ACTION.GET_UUID_TICKET, async (body) => {
  return axiosInstance.get(`/api/tickets/${body}/`);
});

export const getTicketSite = createAsyncThunk(ACTION.GET_SITE, async (body) => {
  return axiosInstance.get('/api/tickets/sites', {
    params: body,
  });
});

export const getTicketPriroty = createAsyncThunk(ACTION.GET_PRIRORITY, async (body) => {
  return axiosInstance.get('/api/tickets/prirority', {
    params: body,
  });
});

export const getTicketCategory = createAsyncThunk(ACTION.GET_CATEGORY, async (body) => {
  return axiosInstance.get('/api/tickets/category', {
    params: body,
  });
});

export const getTicketGroup = createAsyncThunk(ACTION.GET_GROUP, async (body) => {
  return axiosInstance.get('/api/tickets/group', {
    params: body,
  });
});
export const getTicketMode = createAsyncThunk(ACTION.GET_MODE, async (body) => {
  return axiosInstance.get('/api/tickets/modes', {
    params: body,
  });
});
export const getTicketSubCategory = createAsyncThunk(ACTION.GET_SUB_CATEGORY, async (body) => {
  return axiosInstance.get(`/api/tickets/subcategory/${body}`, {
    params: body,
  });
});

export const getTicketItem = createAsyncThunk(ACTION.GET_ITEM, async (body) => {
  return axiosInstance.get(`/api/tickets/item/${body}`, {
    params: body,
  });
});

export const getTicketRequester = createAsyncThunk(ACTION.GET_REQUESTER, async (body) => {
  return axiosInstance.get(`/api/tickets/requesters?search=${body.search}`);
});
export const getTicketStatus = createAsyncThunk(ACTION.GET_STATUS, async (body) => {
  return axiosInstance.get(`/api/tickets/statuses`, {
    params: body,
  });
});

export const createTicket = createAsyncThunk(ACTION.CREATE_TICKET, async (body) => {
  let params = '';
  console.log('body', body);
  const {
    requesterid,
    category,
    assignid,
    name,
    phone,
    content,
    trangthai,
    priority,
    contact,
    is_activate,
    super_ticket,
    assigned_to,
    org,
    subCategory,
    technician,
  } = body;
  params += name !== '' && name !== undefined ? `name=${name}&` : '';
  params += phone !== '' && phone !== undefined ? `phone=${phone}&` : '';
  params += content !== '' && content !== undefined ? `content=${content}&` : '';
  params += trangthai !== '' && trangthai !== undefined ? `trangthai=${trangthai}&` : '';
  params += priority !== '' && priority !== undefined ? `priority=${priority}&` : '';
  params += contact !== '' && contact !== undefined ? `contact=${contact}&` : '';
  params += assigned_to !== '' && assigned_to !== undefined ? `assigned_to=${assigned_to}&` : '';
  params += org !== '' && org !== undefined ? `org=${org}&` : '';
  params += requesterid !== '' && requesterid !== undefined ? `requesterid=${requesterid}&` : '';
  params += assignid !== '' && assignid !== undefined ? `assignid=${assignid}&` : '';
  params += category !== '' && category !== undefined ? `category=${category}&` : '';
  params += technician !== '' && technician !== undefined ? `technician=${technician}&` : '';
  params += subCategory !== '' && subCategory !== undefined ? `subcategory=${subCategory}&` : '';

  params += is_activate !== '' && is_activate !== undefined ? `is_activate=${is_activate}&` : '';
  params += super_ticket !== '' && super_ticket !== super_ticket ? `super_ticket=${super_ticket}&` : '';
  return axiosInstance.post('/api/tickets/?' + params);
});

export const editTicket = createAsyncThunk(ACTION.EDIT_TICKET, async (body) => {
  let params = '';
  const { name, phone, content, trangthai, priority } = body;
  params += name !== '' && name !== undefined ? `name=${name}&` : '';
  params += phone !== '' && phone !== undefined ? `phone=${phone}&` : '';
  params += content !== '' && content !== undefined ? `content=${content}&` : '';
  params += trangthai !== '' && trangthai !== undefined ? `trangthai=${trangthai}&` : '';
  params += priority !== '' && priority !== undefined ? `priority=${priority}&` : '';

  return axiosInstance.put(`/api/tickets/${body.id}/?` + params);
});

export const editTicketUUID = createAsyncThunk(ACTION.EDIT_UUID_TICKET, async (body) => {
  let params = '';
  const {
    requesterid,
    assignid,
    name,
    phone,
    content,
    trangthai,
    priority,
    org,
    contact,
    uuid,
    is_activate,
    super_ticket,
    assigned_to,
    category,
    technician,
    subCategory,
    group,
  } = body;
  // params += uuid !== "" && uuid !== undefined ? `uuid=${uuid}&`: ""
  params += name !== '' && name !== undefined ? `name=${name}&` : '';
  params += phone !== '' && phone !== undefined ? `phone=${phone}&` : '';
  params += content !== '' && content !== undefined ? `content=${content}&` : '';
  params += trangthai !== '' && trangthai !== undefined ? `trangthai=${trangthai}&` : '';
  params += priority !== '' && priority !== undefined ? `priority=${priority}&` : '';
  params += contact !== '' && contact !== undefined ? `contact=${contact}&` : '';
  // params += org !== "" && org !== undefined ? `org=${org}&`: ""
  params += group !== '' && group !== undefined ? `group=${group}&` : '';
  params += subCategory !== '' && subCategory !== undefined ? `subcategory=${subCategory}&` : '';

  params += technician !== '' && technician !== undefined ? `technician=${technician}&` : '';
  params += requesterid !== '' && requesterid !== undefined ? `requesterid=${requesterid}&` : '';
  params += assignid !== '' && assignid !== undefined ? `assignid=${assignid}&` : '';

  params += assigned_to !== '' && assigned_to !== undefined ? `assigned_to=${assigned_to}&` : '';

  params += is_activate !== '' && is_activate !== undefined ? `is_activate=${is_activate}&` : '';
  params += super_ticket !== '' && super_ticket !== undefined ? `super_ticket=${super_ticket}&` : '';
  params += category !== '' && category !== undefined ? `category=${category}&` : '';
  return axiosInstance.put(`/api/tickets/${uuid}/?` + params);
});

export const requestTicket = createAsyncThunk(ACTION.REQUEST_TICKET, async (body) => {
  let params = '';
  const { super_ticket, uuid, requesterid, assignid, name } = body;

  params += name !== '' && name !== undefined ? `name=${name}&` : '';
  params += super_ticket !== '' && super_ticket !== undefined ? `super_ticket=${super_ticket}&` : '';
  // params += uuid !== "" && uuid !== undefined ? `uuid=${uuid}&`: ""
  params += requesterid !== '' && requesterid !== undefined ? `requesterid=${requesterid}&` : '';
  params += assignid !== '' && assignid !== undefined ? `assignid=${assignid}&` : '';

  return axiosInstance.put(`/api/tickets/${uuid}/?` + params);
});

export const getTechnican = createAsyncThunk(ACTION.GET_TECHNICAN, async (body) => {
  return axiosInstance.get('/api/tickets/technician', body);
});

export const getRequest = createAsyncThunk(ACTION.GET_REQUEST, async (body) => {
  return axiosInstance.get('/api/tickets/requesters', body);
});

export const getHistory = createAsyncThunk(ACTION.GET_HISTORY, async (body) => {
  return axiosInstance.get('/api/histories/', { params: body });
});

export const getSyncData = createAsyncThunk('SyncData', async (body) => {
  return axiosInstance.get('/api/tickets/sync-data', body);
});

const ticketSlice = createSlice({
  name: 'Ticket',
  initialState: initialState,
  extraReducers: {
    [getTicket.pending.toString()]: (state) => {
      state.loading = true;
    },
    [getTicket.rejected.toString()]: (state) => {
      state.loading = false;
    },
    [getTicket.fulfilled.toString()]: (state, action) => {
      state.loading = false;
      state.successCreate = null;
      state.successEdit = null;
      state.successEditUUID = null;
      state.successRequest = null;
      state.ticketList = action.payload;
    },

    [getUUIDTicket.pending.toString()]: (state) => {},
    [getUUIDTicket.rejected.toString()]: (state) => {},
    [getUUIDTicket.fulfilled.toString()]: (state, action) => {
      state.successEditUUID = null;
      state.ticketUUID = action.payload;
    },

    [getTechnican.pending.toString()]: (state) => {},
    [getTechnican.rejected.toString()]: (state) => {},
    [getTechnican.fulfilled.toString()]: (state, action) => {
      state.technicanList = action.payload;
    },

    [getRequest.pending.toString()]: (state) => {},
    [getRequest.rejected.toString()]: (state) => {},
    [getRequest.fulfilled.toString()]: (state, action) => {
      state.requestList = action.payload;
    },

    [createTicket.pending.toString()]: (state) => {},
    [createTicket.rejected.toString()]: (state, action) => {
      state.successCreate = false;
    },
    [createTicket.fulfilled.toString()]: (state, action) => {
      state.successCreate = true;
    },
    [editTicket.pending.toString()]: (state) => {},
    [editTicket.rejected.toString()]: (state) => {
      state.successEdit = false;
    },
    [editTicket.fulfilled.toString()]: (state) => {
      state.successEdit = true;
    },
    [editTicketUUID.pending.toString()]: (state) => {},
    [editTicketUUID.rejected.toString()]: (state) => {
      state.successEditUUID = false;
    },
    [editTicketUUID.fulfilled.toString()]: (state) => {
      state.successEditUUID = true;
    },
    [requestTicket.pending.toString()]: (state) => {},
    [requestTicket.rejected.toString()]: (state) => {
      state.successRequest = false;
    },
    [requestTicket.fulfilled.toString()]: (state) => {
      state.successRequest = true;
    },

    [getHistory.pending.toString()]: (state) => {
      state.loadingHistory = true;
    },
    [getHistory.rejected.toString()]: (state, action) => {
      state.historyList = action.payload;
      state.loadingHistory = false;
    },
    [getHistory.fulfilled.toString()]: (state, action) => {
      state.loadingHistory = false;

      state.historyList = action.payload;
    },

    [getTicketSite.pending.toString()]: (state) => {
      state.loadingHistory = true;
    },
    [getTicketSite.rejected.toString()]: (state) => {
      state.loadingHistory = false;
    },
    [getTicketSite.fulfilled.toString()]: (state, action) => {
      state.loadingHistory = false;
      state.siteList = action.payload;
    },

    [getTicketPriroty.pending.toString()]: (state) => {
      state.loadingHistory = true;
    },
    [getTicketPriroty.rejected.toString()]: (state) => {
      state.loadingHistory = false;
    },
    [getTicketPriroty.fulfilled.toString()]: (state, action) => {
      state.loadingHistory = false;
      state.prirotyList = action.payload;
    },

    [getTicketCategory.pending.toString()]: (state) => {
      state.loadingHistory = true;
    },
    [getTicketCategory.rejected.toString()]: (state) => {
      state.loadingHistory = false;
    },
    [getTicketCategory.fulfilled.toString()]: (state, action) => {
      state.loadingHistory = false;
      state.categoryList = action.payload;
    },
    [getTicketGroup.pending.toString()]: (state) => {
      state.loadingHistory = true;
    },
    [getTicketGroup.rejected.toString()]: (state) => {
      state.loadingHistory = false;
    },
    [getTicketGroup.fulfilled.toString()]: (state, action) => {
      state.loadingHistory = false;
      state.groupList = action.payload;
    },

    [getTicketMode.pending.toString()]: (state) => {
      state.loadingHistory = true;
    },
    [getTicketMode.rejected.toString()]: (state) => {
      state.loadingHistory = false;
    },
    [getTicketMode.fulfilled.toString()]: (state, action) => {
      state.loadingHistory = false;
      state.modeList = action.payload;
    },
    [getTicketSubCategory.pending.toString()]: (state) => {
      state.loadingHistory = true;
    },
    [getTicketSubCategory.rejected.toString()]: (state) => {
      state.loadingHistory = false;
    },
    [getTicketSubCategory.fulfilled.toString()]: (state, action) => {
      state.loadingHistory = false;
      state.listSubCategory = action.payload;
    },

    [getTicketItem.pending.toString()]: (state) => {
      state.loadingHistory = true;
    },
    [getTicketItem.rejected.toString()]: (state) => {
      state.loadingHistory = false;
    },
    [getTicketItem.fulfilled.toString()]: (state, action) => {
      state.loadingHistory = false;
      state.listSubCategory = action.payload;
    },

    [getTicketRequester.pending.toString()]: (state) => {
      state.loadingHistory = true;
    },
    [getTicketRequester.rejected.toString()]: (state) => {
      state.loadingHistory = false;
    },
    [getTicketRequester.fulfilled.toString()]: (state, action) => {
      state.loadingHistory = false;
      state.requesterList = action.payload;
    },

    [getTicketStatus.pending.toString()]: (state) => {
      state.loadingHistory = true;
    },
    [getTicketStatus.rejected.toString()]: (state) => {
      state.loadingHistory = false;
    },
    [getTicketStatus.fulfilled.toString()]: (state, action) => {
      state.loadingHistory = false;
      state.statusList = action.payload;
    },
  },
});

const { reducer: ticketReducer } = ticketSlice;
export default ticketReducer;
