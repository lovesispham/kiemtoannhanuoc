import React, { useState, useEffect } from 'react';
import { useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TicketContext } from './TicketContent';
import TicketDetail from '../ticket/TicketDetail';
import TicketQuest from '../ticket/TicketQuest';
import { getUUIDTicket } from '../../erp/redux/ticket';
import { toast_type } from '../../constants';
import ToastMsg from '../../components/common/Toast';

const TicketDetailModal = () => {
  const dispatch = useDispatch();
  const dataContext = useContext(TicketContext);
  const count = dataContext.count;

  const uuid = dataContext.uuid;

  const setUUID = dataContext.setUUID;
  const { successEditUUID, successRequest } = useSelector((state) => state.ticket);

  const infoList = useSelector((state) => state.info.infoList);

  //   ticket detail
  const [openModalTicket, setOpenModalTicket] = useState(false);
  const [pulse, setPulse] = useState(false);

  useEffect(() => {
    if (successEditUUID === true && uuid) {
      setUUID();
      dispatch(getUUIDTicket(uuid));
    }
  }, [successEditUUID]);

  useEffect(() => {
    if (successRequest === true && uuid) {
      dispatch(getUUIDTicket(uuid));
    }
  }, [successRequest]);

  useEffect(() => {
    if (uuid) {
      setTimeout(() => {
        dispatch(getUUIDTicket(uuid));
      }, 2000);

      //   setData(filterDatabyuuid);
    }
  }, [uuid]);
  useEffect(() => {
    console.log('test ', uuid);
    if (uuid) {
      setOpenModalTicketAsk();
      console.log('test1 ', uuid);
      setTimeout(async () => {
        setOpenModalTicket(true);

        setPulse(false);
      }, 3000);
      //   setData(filterDatabyuuid);
    }
  }, [uuid]);

  const handleCloseModalTicket = () => {
    setOpenModalInfos(false);
    setOpenModalTicket(false);
    setUUID('');
  };

  const [openModalInfos, setOpenModalInfos] = useState(false);
  const handleOpenModalInfos = () => {
    setOpenModalInfos(!openModalInfos);
  };

  const [openModalTicketAsk, setOpenModalTicketAsk] = useState(false);

  return (
    <>
      {successRequest && <ToastMsg type={toast_type.success} />}
      {successRequest === false && <ToastMsg type={toast_type.error} msg='Thất Bại ' />}
      {pulse && (
        <>
          <div className='modal-popup_style2 open'>
            <div className='modal-overlay'></div>

            <div className='popup-container'>
              <div className='dot-pulse'></div>
            </div>
          </div>
        </>
      )}
      {openModalTicket && (
        <>
          <div className='modal-overlay'></div>

          <div className={`pu-notify-ticket ${openModalTicketAsk && count > 0 ? 'active' : ''}`}>
            <h3 className='title'>Bạn có muốn mở Ticket mới</h3>
            <div className='d-flex'>
              <span
                className='btn'
                onClick={() => {
                  setOpenModalTicket(false);
                  setPulse(true);
                  setUUID(uuid);
                }}
              >
                Đồng ý
              </span>

              <span className='btn' onClick={() => setOpenModalTicketAsk(false)}>
                Huỷ bỏ
              </span>
            </div>
          </div>

          <div className='notify-ticket' onClick={() => setOpenModalTicketAsk(!openModalTicketAsk)}>
            <div className='icon'>
              <div className='text'>
                <span className='fa fa-bell'></span>
              </div>
              <div className='count'>{count}</div>
            </div>
          </div>
          <div className='pu-quest' onClick={() => handleOpenModalInfos()}>
            <div className='text'>
              Tài liệu
              <span className='icon'>
                <i className='fa fa-question'></i>
              </span>
            </div>
          </div>
          <div className={`modal-popup_style2 open ${openModalInfos ? 'screen-dt' : ''}`}>
            <div className='popup-container pu-general pu-ticket style2'>
              <h3 className='pu-heading'>Chi tiết Ticket</h3>
              <div className='popup-close' onClick={() => handleCloseModalTicket()}>
                <span className='fa fa-times'></span>
              </div>
              <TicketDetail uuid={uuid} setUDO={setOpenModalTicket} />

              {openModalInfos && (
                <TicketQuest setUDO={setOpenModalInfos} openModalInfos={openModalInfos} infoList={infoList} />
              )}
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default TicketDetailModal;
