import React, { useEffect, useState, useRef } from 'react';
import axiosInstance from '../../erp/service/config';
import { getCookieStorage } from '../../erp/helps/storage';
const TicketContext = React.createContext();

function TicketProvider({ children }) {
  //websocket
  const webSocket = useRef(null);
  const [messages, setMessages] = useState([]);
  const [uuid, setUUID] = useState('');
  const [count, setCount] = useState(0);
  const accessToken = getCookieStorage('access_token');

  const [profile, setProfile] = useState('');
  useEffect(() => {
    const getProfile = async () => {
      try {
        const response = await axiosInstance.get('/api/profile/');
        setProfile(response);
      } catch (error) {
        // console.log(error);
      }
    };
    if (accessToken) {
      getProfile();
    }
  }, [accessToken]);

  useEffect(() => {
    if (profile && profile.ext && profile.ext.exentions) {
      setTimeout(() => {
        webSocket.current = new WebSocket(`wss://${process.env.REACT_APP_WSS_URL}ws/chat/${profile.ext.exentions}/`);
        webSocket.current.onmessage = (message) => {
          setMessages((prev) => [...prev, JSON.parse(message.data)]);
        };
        return () => webSocket.current.close();
      }, 2000);
    }
  }, [profile]);

  function get_last(array) {
    //if the array is empty return null
    return array[array.length - 1] || null;
  }

  useEffect(() => {
    console.log('messages', messages);
    const filterCount = messages.filter((x) => x.message !== 'admin' && x.message !== profile.ext.exentions);
    const filter = messages.filter((x) => x.message !== 'admin');
    const getuuidWSS = filter.map((item) => item.uuid);
    const getlastuuid = get_last(getuuidWSS);
    setUUID(getlastuuid);
    setCount(filterCount.length);
  }, [messages]);

  const value = {
    uuid,
    setUUID,
    count,
    profile,
  };
  return <TicketContext.Provider value={value}>{children}</TicketContext.Provider>;
}

export { TicketContext, TicketProvider };
