import React, { useEffect, useState } from 'react';
import {
  FormInput,
  FormCheckbox,
  Row,
  Col,
} from 'shards-react';
import { useDispatch, useSelector } from 'react-redux';
import { createOrg, getOrgFull} from '../../erp/redux/org';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { getCity, getDistrict, getWard } from '../../erp/redux/location'
import Select from 'react-select'
import { HelpText } from '../components-overview/Help-text';
import RequiredText from '../common/RequiredText';
const DialogUCOrg = ({ setUDO, setData }) => {
  const dispatch = useDispatch();

  const cityList = useSelector(state => state.location.cityList);
  const districtList = useSelector(state => state.location.districtList);
  const wardList = useSelector(state => state.location.wardList);

  const orgList = useSelector(state => state.org.orgListFull)



  const [active, setActive] = useState(false)

  useEffect(() => {
   dispatch(getCity())
   dispatch(getOrgFull())
  }, [])


  const submitForm = values => {
    dispatch(createOrg(values));
    setUDO(false);
    setData('');
  };

  useEffect(() => {
  }, []);

  // option list org

  const orgOptions = orgList && orgList.result && orgList.result.length > 0 && orgList.result.map(option => ({
    value: option.id,
    label: option.name
  }))

  ////////////thay đổi thành phố
  const cityOptions = cityList.map(option => ({
    value: option.id,
    label: option.name
  }));

  /////////////quan huyen
  const districtOptions = districtList.map(option => ({
    value: option.id,
    label: option.name
  }));
  //////////////phuong xa
  const wardOptions = wardList.map(option => ({
    value: option.id,
    label: option.name
  }));

  const handleChangeCityAuto = (newValue) => {

    if (newValue != null) {
      setFieldValue("city", newValue.value);
      dispatch(getDistrict(newValue.value))

    }
  };

  const handleChangeDistrictAuto = (newValue) => {
    if (newValue != null) {
      setFieldValue("district", newValue.value);
      dispatch(getWard(newValue.value));

    }
  };


  const handleChangeWardAuto = async (newValue) => {

    if (newValue != null) {

      setFieldValue("ward", newValue.value);
    }
  };

  const handleChangeOrg = async (newValue) => {

    if (newValue != null) {

      setFieldValue("parent_org", newValue.value);
    }
  };

  const handleChangeCheckbox = (e) => {
    const {checked} = e.target;
    setActive(checked)
    setFieldValue("is_parent", checked);
  }

  const phoneRegExp = ('^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$')
  const emailRegExp = ("([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])")

  const validationSchema = yup.object({
    name: yup
      .string()
      .trim()
      .required('Nhập tên đăng nhập'),

  });

  const {
    values,
    handleSubmit,
    handleChange,
    setFieldValue,
    handleBlur,
    errors,
    touched
  } = useFormik({
    initialValues:{},
    validationSchema: validationSchema,
    onSubmit: submitForm
  });

  return (
    <div className="content">
      <div className="box-form">
        <Row>
          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Tên đơn vị <RequiredText/></div>
              <FormInput
                type="text"
                name="name"
                placeholder="Tên"
                value={values.name}
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.name && errors.name ? 'error' : undefined}
              />
              <HelpText status='error'>
                {touched.name && errors.name ? errors.name : ''}
              </HelpText>
            </div>
          </Col>
{/*
          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Email <RequiredText/></div>
              <FormInput
                type="text"
                name="email"
                placeholder="Email"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.email && errors.email ? 'error' : undefined}
              />
              <HelpText status='error'>
                {touched.email && errors.email ? errors.email : ''}
              </HelpText>
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">SĐT <RequiredText/></div>

              <FormInput
                type="text"
                name="phone"
                placeholder="SĐT"
                value={values.phone}
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.phone && errors.phone ? 'error' : undefined}
              />
              <HelpText status='error'>
                {touched.phone && errors.phone ? errors.phone : ''}
              </HelpText>
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Quản lý <RequiredText/></div>

              <FormInput
                type="text"
                name="manage_org"
                placeholder="Quản lý "
                value={values.manage_org}
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.manage_org && errors.manage_org ? 'error' : undefined}
              />
              <HelpText status='error'>
                {touched.manage_org && errors.manage_org ? errors.manage_org : ''}
              </HelpText>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className="form-input-box">

              <FormCheckbox
                name='is_parent'
                checked={active}
                onChange={handleChangeCheckbox}
                >
                  Thuộc đơn vị
                </FormCheckbox>


            </div>
          </Col>
          <Col xs={12} sm={8}>
          {
            active && (

            <div className="form-input-box">
            <div className="label-name">Chọn đơn vị kiểm toán </div>

            <Select
                type="select"
                name="parent_org"
                placeholder={<div>Chọn</div>}
                onChange={handleChangeOrg}
                options={orgOptions}
              >
              </Select>


            </div>

            )
          }
          </Col>

          <Col xs={12} sm={4}>
            <div className="form-input-box">
              <div className="label-name">Thành phố </div>

              <Select
                type="select"
                name="city"
                placeholder={<div>Chọn</div>}
                onChange={handleChangeCityAuto}
                options={cityOptions}
              >
              </Select>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className="form-input-box">
              <div className="label-name">Quận/Huyện</div>

              <Select
                type="select"
                name="district"
                placeholder={<div>Chọn</div>}
                onChange={handleChangeDistrictAuto}
                options={districtOptions}
              >
              </Select>
            </div>
          </Col>


          <Col xs={12} sm={4}>
            <div className="form-input-box">
              <div className="label-name">Phường/Xã </div>

              <Select
                type="select"
                name="ward"
                placeholder={<div>Chọn </div>}
                onChange={handleChangeWardAuto}
                options={wardOptions}
              >
              </Select>
            </div>
          </Col> */}

          <Col xs={12}>
            <div className="form-input-box text-right">
              <button
                className="btn"
                type="submit"
                onClick={handleSubmit}>
               Tạo
              </button>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default DialogUCOrg;
