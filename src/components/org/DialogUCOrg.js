import React,{ useEffect, useState} from 'react';
import {
  FormInput,

  Row,
  Col,
} from 'shards-react';
import { useDispatch} from 'react-redux';
import { editOrg, getOrgFull } from '../../erp/redux/org';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { getCity} from '../../erp/redux/location'




const DialogUCOrg = ({ dataEdit, setUDO, setData }) => {
  const dispatch = useDispatch();




  useEffect(() => {
    if(dataEdit.city !== null || dataEdit.district !== null || dataEdit.ward !== null){
      dispatch(getCity())

    }
    dispatch(getOrgFull({}))
  }, [dataEdit])



  const validationSchema = yup.object();

  const submitForm = values => {
    if(values.is_parent === false){
      const newValue = {
        ...values,
        parent_org:null
      }
       dispatch(editOrg({
              id:dataEdit.id,
              body:newValue
            }));
    } else {
      dispatch(editOrg({
        id:dataEdit.id,
        body:values
      }));
    }

     setUDO(false);
     setData('');
  };








  const {
    values,
    handleSubmit,
    handleChange,
    setFieldValue,
    handleBlur,
    errors,
    // touched
  } = useFormik({
    initialValues: !dataEdit
      ? {}
      : {

        name: dataEdit.name === null ? '' : dataEdit.name,

      },
    validationSchema: validationSchema,
    onSubmit: submitForm
  });




  return (
    <div className="content">

      <div className="box-form">
        <Row>
          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Tên Phòng Ban</div>
              <FormInput
                type="text"
                name="name"
                placeholder="Tên"
                value={values.name}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
          </Col>


          <Col xs={12}>
            <div className="form-input-box text-right">
              <button
                className="btn"
                type="edit"
                onClick={handleSubmit}>
                {dataEdit ? 'Sửa' : 'Sửa'}
              </button>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default DialogUCOrg;
