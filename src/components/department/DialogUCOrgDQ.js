import React,{ useEffect, useState} from 'react';
import {
  FormInput,

  Row,
  Col,
} from 'shards-react';
import { useDispatch} from 'react-redux';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { postDepartmetn, putDepartmetn } from '../../erp/redux/department';
import Toast from '../common/Toast';


const DialogUCOrgDQ = ({dataEdit , setUDO, setData ,action}) => {

  const dispatch = useDispatch();







  const validationSchema = yup.object();

  const submitForm = values => {
    const newValue = {
      ...values,
      parent_org:null
    }
    if(action){
   dispatch(postDepartmetn(newValue))
    }
    else{
      dispatch(putDepartmetn({id:dataEdit.id,body:newValue}))
    }

     setUDO(false);
     setData('1');

  };








  const {
    values,
    handleSubmit,
    handleChange,
    handleBlur,
    errors,
    // touched
  } = useFormik({
    initialValues: !dataEdit
      ? {}
      : {

        name: dataEdit.name === null ? '' : dataEdit.name,

      },
    validationSchema: validationSchema,
    onSubmit: submitForm
  });



  return (
    <div className="content">

      <div className="box-form">
        <Row>
          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Phòng Ban</div>
              <FormInput
                type="text"
                name="name"
                placeholder="Tên"
                value={values.name}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
          </Col>


          <Col xs={12}>
            <div className="form-input-box text-right">
              <button
                className="btn"
                type="edit"
                onClick={handleSubmit}>
                {dataEdit ? 'Sửa' : 'Sửa'}
              </button>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  )
}

export default DialogUCOrgDQ
