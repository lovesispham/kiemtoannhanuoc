import React, { useEffect } from 'react';
import { FormInput, Row, Col, FormSelect } from 'shards-react';
import { useDispatch, useSelector } from 'react-redux';
import { createContact, getContact } from '../../erp/redux/contact';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { getPosition } from '../../erp/redux/position';
import { getOrg } from '../../erp/redux/org';
import Select from 'react-select';
import { HelpText } from '../components-overview/Help-text';
import { phoneRegExp } from '../../constants';
const TicketCreateContact = ({ setUDO }) => {
  const dispatch = useDispatch();

  const positionList = useSelector(state => state.position.positionList);

  const { successCreateP }  = useSelector(state => state.position);
  const {successCreate} = useSelector(state => state.contact);

  useEffect(() => {
    if(successCreate === true){
      dispatch(getContact({}))
    }
  }, [successCreate])
  


  useEffect(() => {
    if (successCreateP === true) {
      setTimeout(() => {
        dispatch(getPosition());
      }, 1000);
    }
  }, [successCreateP]);

  useEffect(() => {
    dispatch(getPosition());
  }, []);



  const handleChangeOrg = newValue => {
    if (newValue != null) {
      setFieldValue('org', newValue.name);
      dispatch(getOrg(newValue.name));
    }
  };
  const orgList = useSelector(state => state.org.orgList);

  const { successCreateO } = useSelector(state => state.org);

  const orgOptions = orgList && orgList.result && orgList.result.map(option => ({
    value: option.id,
    label: option.name
  }));

  useEffect(() => {
    if (successCreateO === true) {
      setTimeout(() => {
        dispatch(getOrg({}));
      }, 1000);
    }
  }, [successCreateO]);

  useEffect(() => {
    dispatch(getOrg({}));
  }, []);



  const validationSchema = yup.object({
    email: yup
    .string()
    .trim()
    //   .email('Email không hợp lệ')
    .required('Nhập Email'),
    fullname: yup
    .string()
    .trim()
    //   .email('Email không hợp lệ')
    .required('Nhập Tên'),
    phone: yup.string()
    .required("required")
    .matches(phoneRegExp, 'SĐT không đúng định dạng')
    .min(10, "to short")
    .max(10, "to long"),
  });

  const submitForm = values => {

    dispatch(createContact(values));

    setUDO(false);
  };

  // console.log('aaaa',Position)

  const {
    values,
    handleSubmit,
    handleChange,
    setFieldValue,
    handleBlur,
    errors,
    touched
  } = useFormik({
    initialValues: {},
    validationSchema: validationSchema,
    onSubmit: submitForm
  });
  return (
    <div className="content" style={{ background: '#e7e7e7', borderRadius: '0.5rem' }}>
      <h3 className="title">Tạo người liên hệ</h3>
      <div className="box-form">
        <Row>
          <Col xs={12} sm={4}>
            <div className="form-input-box">
              <div className="label-name">Tên</div>
              <FormInput
                type="text"
                name="fullname"
                placeholder="Tên"
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.fullname && errors.fullname ? 'error' : undefined}

              />
                          <HelpText status="error">
                {touched.fullname && errors.fullname ? errors.fullname : ''}
              </HelpText>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className="form-input-box">
              <div className="label-name">SĐT</div>

              <FormInput
                type="text"
                name="phone"
                placeholder="SĐT"
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.phone && errors.phone ? 'error' : undefined}

              />
            </div>
            <HelpText status="error">
                {touched.phone && errors.phone ? errors.phone : ''}
              </HelpText>
          </Col>

          <Col xs={12} sm={4}>
            <div className="form-input-box">
              <div className="label-name">Email</div>

              <FormInput
                type="text"
                name="email"
                placeholder="Email"
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.email && errors.email ? 'error' : undefined}

              />
                            <HelpText status="error">
                {touched.email && errors.email ? errors.email : ''}
              </HelpText>
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Phòng ban </div>

              <FormSelect
                type="select"
                name="position"
                placeholder="position"
                onChange={e => setFieldValue('position', e.target.value)}
                onBlur={handleBlur}>
                <option>Chọn</option>
                
                {positionList && positionList.results &&
                  positionList.results.length > 0 &&
                  positionList.results.map((item, i) => <option value={item.id}>{item.name}</option>)}
              </FormSelect>
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Đơn vị </div>

              <Select
                type="select"
                name="org"
                placeholder={<div>Chọn</div>}
                onChange={handleChangeOrg}
                onBlur={handleBlur}
                options={orgOptions}></Select>
            </div>
          </Col>

          <Col xs={12}>
            <div className="form-input-box text-right">
              <button
                style={{ width: '36px', minWidth: 'inherit' }}
                className="btn"
                type="submit"
                onClick={handleSubmit}
                >
                <i className="fa fa-plus"></i>{' '}
              </button>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default TicketCreateContact;
