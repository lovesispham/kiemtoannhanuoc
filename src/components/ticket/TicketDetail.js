import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormTextarea, FormSelect, FormInput, Row, Col } from 'shards-react';
import Select from 'react-select';
import { getOrg, getOrgId } from '../../erp/redux/org';
import { getContact, getContactId } from '../../erp/redux/contact';
import TicketHistory from './TicketHistory';
import { trangthaiList, priorityList } from '../../constants';
import { useFormik } from 'formik';
import AsyncSelect from 'react-select/async';
import * as yup from 'yup';
import {
  editTicketUUID,
  getTechnican,
  getRequest,
  getTicketSite,
  getTicketPriroty,
  getTicketCategory,
  getTicketGroup,
  getTicketMode,
  getTicketStatus,
  getTicketSubCategory,
  getTicketItem,
  getTicketRequester,
} from '../../erp/redux/ticket';
import { useContext } from 'react';
import { TicketContext } from '../../components/notification/TicketContent';
import useDebounce from './useDebounce';
import { useRef } from 'react';

const TicketDetail = ({ uuid, setUDO }) => {
  const dispatch = useDispatch();
  const dataContext = useContext(TicketContext);

  const profile = dataContext.profile;
  const [contextUUID, setContextUUID] = useState();
  const contactList = useSelector((state) => state.contact.contactList);
  const listStatus = useSelector((state) => state.ticket.statusList);
  const listPriroty = useSelector((state) => state.ticket.prirotyList);
  const listCatagori = useSelector((state) => state.ticket.categoryList);
  const listSubCatagory = useSelector((state) => state.ticket.listSubCategory);
  const techList = useSelector((state) => state.ticket.technicanList);

  const [listRequesser, setListRequesser] = useState({});

  const tickeUUID = useSelector((state) => state.ticket.ticketUUID);
  const [contact, setContact] = useState({});
  const [activeEditOrg, setEditOrg] = useState(false);
  const [contactPhone, setContactPhone] = useState('');
  const [nameOrg, setNameOrg] = useState('');
  const [nameDepartment, setNameDepartment] = useState('');
  const [namePosition, setNamePosition] = useState('');
  const [nameContact, setNameContact] = useState('');
  const [contactID, setContactID] = useState('');
  const [requests, setRequests] = useState();
  const [valuePriority, setValuePriority] = useState();
  const [valueCatagory, setvalueCatagory] = useState();
  const [mes, setMes] = useState({});
  const [listTechnician, setListTechnician] = useState([]);
  const [nameTenicate, setNameTenicate] = useState();
  const [nameStatus, setnameStatus] = useState();
  const [nameSubCatagory, setNameCatagory] = useState();
  const [activeNumber, setActiveNumber] = useState(0);
  const typingTimeOutRef = useRef(null);

  const callApi = async () => {
    await dispatch(getContact({}));
    await dispatch(getTechnican());

    await dispatch(getTicketSite({}));
    await dispatch(getTicketPriroty({}));
    await dispatch(getTicketCategory({}));

    await dispatch(getTicketStatus({}));
  };

  useEffect(() => {
    callApi();
  }, []);

  useEffect(() => {
    setContextUUID(uuid);
  }, [dataContext]);

  useEffect(() => {
    if (!tickeUUID) {
      return;
    }
    if (!tickeUUID.data) {
      return;
    }

    if (!tickeUUID.data.contact) {
      return;
    }
    setContact(tickeUUID.data.contact);
  }, [tickeUUID]);

  useEffect(() => {
    if (contactId) {
      dispatch(getContactId(contactId));
    }
  }, [contactId]);

  useEffect(() => {
    if (orgDataId) {
      dispatch(getOrgId(orgDataId));
    }
  }, [orgDataId]);

  const handleOptionChangeOrg = (value) => {
    setOrgName(value.label);
    setFieldValue('org', value.value);
    setContactID(value.value);
    setNameContact(value.label);
    setNameOrg(value.organization);
    setNameDepartment(value.department);
    setContactPhone(value.phone);
    setNamePosition(value.position);
  };

  const changeTechName = (newValue) => {
    if (newValue != null) {
      setFieldValue('assignid', newValue.value);
    }
  };

  const statuses =
    listStatus &&
    listStatus.data &&
    listStatus.data.map((item) => ({
      value: item.id,
      label: item.name,
      key: 'trangthai',
    }));

  const priroty =
    listPriroty &&
    listPriroty.data &&
    listPriroty.data.priorities &&
    listPriroty.data.priorities.map((item) => ({
      value: item.id,
      label: item.name,
      key: 'priority',
    }));

  const contactSelect =
    contactList &&
    contactList.contacts &&
    contactList.contacts.contacts &&
    contactList.contacts.contacts.map((item) => ({
      value: item.id,
      label: item.fullname,
      phone: item.mobile,
      department: item.department,
      position: item.position,
      organization: item.organization,
    }));
  const filterColors = (inputValue, res) => {
    const contactSelect =
      res &&
      res.contacts &&
      res.contacts.contacts &&
      res.contacts.contacts.map((item) => ({
        value: item.id,
        label: item.fullname,
        phone: item.mobile,
        department: item.department,
        position: item.position,
        organization: item.organization,
      }));
    return contactSelect && contactSelect.filter((i) => i.label.toLowerCase().includes(inputValue.toLowerCase()));
  };
  const loadOptions = (inputValue, callback) => {
    if (typingTimeOutRef.current) {
      clearTimeout(typingTimeOutRef.current);
    }

    if (inputValue !== '') {
      typingTimeOutRef.current = setTimeout(() => {
        dispatch(getContact({ name: inputValue }))
          .unwrap()
          .then((res) => {
            callback(filterColors(inputValue, res));
          });
      }, 400);
    }
  };
  // const debounceSearch = useDebounce(loadOptions, 500);

  const catagory =
    listCatagori &&
    listCatagori.data &&
    listCatagori.data.categories &&
    listCatagori.data.categories.map((item) => ({
      value: item.id,
      label: item.name,
      key: 'category',
    }));
  const subCategory =
    listSubCatagory &&
    listSubCatagory.data &&
    listSubCatagory.data.subcategories &&
    listSubCatagory.data.subcategories.map((item) => ({
      value: item.id,
      label: item.name,
      key: 'subCategory',
    }));

  const [orgDataId, setOrgId] = useState(
    tickeUUID.data !== undefined && tickeUUID.data !== null
      ? tickeUUID.data.contact !== null
        ? tickeUUID.data.contact.organization.id
        : ''
      : ''
  );

  const [orgName, setOrgName] = useState(
    tickeUUID.data !== undefined
      ? tickeUUID.data.contact !== null
        ? tickeUUID.data.contact.organization.name
        : ''
      : ''
  );

  const [contactId, setContactId] = useState(
    tickeUUID.data !== undefined && tickeUUID.data !== null
      ? tickeUUID.data.contact !== null
        ? tickeUUID.data.contact.id
        : ''
      : ''
  );

  const status1234 = [
    { label: tickeUUID && tickeUUID.data && tickeUUID.data.trangthai && tickeUUID.data.trangthai.name },
  ];
  const priroty1234 = [
    { label: tickeUUID && tickeUUID.data && tickeUUID.data.priority && tickeUUID.data.priority.name },
  ];

  const catagory1 = [{ label: tickeUUID && tickeUUID.data && tickeUUID.data.category && tickeUUID.data.category.name }];

  useEffect(() => {
    setRequests(tickeUUID && tickeUUID.req && tickeUUID.req.request && tickeUUID.req.request.requester);
  }, [tickeUUID]);

  //   open modal infos

  // edit

  const priorityValue = priorityList.filter(
    (item) =>
      item.id ===
      (tickeUUID.req !== undefined && tickeUUID.req !== null
        ? tickeUUID.req.request !== null && tickeUUID.req.request !== undefined
          ? tickeUUID.req.request.priority !== null
            ? parseInt(tickeUUID.req.request.priority.id)
            : ''
          : ''
        : '')
  );
  const trangthaiValue = trangthaiList.filter(
    (item) =>
      item.id ===
      (tickeUUID.req !== undefined && tickeUUID.req !== null
        ? tickeUUID.req.request !== null && tickeUUID.req.request !== undefined
          ? tickeUUID.req.request.status !== null
            ? parseInt(tickeUUID.req.request.status.id)
            : ''
          : ''
        : '')
  );

  const validationSchema = yup.object();

  const submitForm = (values) => {
    if (tickeUUID.data && !tickeUUID.data.contact) {
      if (!contactID) {
        setMes({ name: 'Không được để trống người liên hệ' });
        return;
      }
    }
    if (tickeUUID.data && tickeUUID.data.request_id === '0') {
      if (listRequesser && !listRequesser.requesterid) {
        setMes({ requesterid: 'Không được để trống người yêu cầu' });
        return;
      }
    }

    if (tickeUUID.data && !tickeUUID.data.category) {
      if (listRequesser && !listRequesser.category) {
        setMes({ category: 'Không được để trống danh mục hỗ trợ' });
        return;
      }
    }

    if (tickeUUID.data && !tickeUUID.data.priority) {
      if (listRequesser && !listRequesser.priority) {
        setMes({ priority: 'Không được để trống mức độ ưu tiên' });
        return;
      }
    }
    if (tickeUUID.data && !tickeUUID.data.name) {
      if (values && !values.name) {
        setMes({ valueName: 'Không được để trống tên ticket' });
        return;
      }
    }
    if (tickeUUID && tickeUUID.req && !tickeUUID.req.request) {
      if (listRequesser && !listRequesser.subCategory) {
        setMes({ subCategory: 'Không được để trống danh mục nhỏ' });
        return;
      }
    }

    console.log(' !tickeUUID.data', tickeUUID.data);
    if (tickeUUID.data && !tickeUUID.data.content) {
      if (values && !values.content) {
        setMes({ content: 'Không được để trống nội dung ticket' });
        return;
      }
    }

    setContextUUID({});
    setMes({});
    console.log('listRequesser', listRequesser);

    if (tickeUUID.data.super_ticket === false) {
      const newValue = {
        ...values,
        ...listRequesser,
        technician: listRequesser.technician || (profile && profile.technician && profile.technician.id_tech),
        trangthai: statuses && statuses.find((v) => v.value === '1').value,
        super_ticket: true,
        contact: contactID,
      };

      dispatch(editTicketUUID({ ...newValue, uuid: uuid }));
      setUDO(false);
    } else {
      dispatch(
        editTicketUUID({
          ...values,
          contact: contactID,
          requesterid: requests && requests.id,
          uuid: uuid,
          technician: listRequesser.technician || (profile && profile.technician && profile.technician.id_tech),
          ...listRequesser,
        })
      );
      setUDO(false);
    }
  };

  const { values, handleSubmit, handleChange, setFieldValue, handleBlur } = useFormik({
    initialValues: {
      uuid: uuid,
      // is_activate: tickeUUID.data !== undefined  ? tickeUUID.data.is_activate : '',
      name: tickeUUID.data !== undefined ? tickeUUID.data.name : '',
      id: tickeUUID.data !== undefined ? tickeUUID.data.id : '',
      phone: tickeUUID.data !== undefined ? tickeUUID.data.phone : '',
      priority: tickeUUID && tickeUUID.data && tickeUUID.data.priority && tickeUUID.data.priority.code,
      content: tickeUUID && tickeUUID.data && tickeUUID.data.name && tickeUUID.data.name,
      trangthai: tickeUUID && tickeUUID.data && tickeUUID.data.trangthai && tickeUUID.data.trangthai.code,
      group:
        tickeUUID &&
        tickeUUID.req &&
        tickeUUID.req.request &&
        tickeUUID.req.request.group &&
        tickeUUID.req.request.group.id,
      subCategory:
        tickeUUID &&
        tickeUUID.req &&
        tickeUUID.req.request &&
        tickeUUID.req.request.subcategory &&
        tickeUUID.req.request.subcategory.id,
      // technician:
      //   tickeUUID &&
      //   tickeUUID.req &&
      //   tickeUUID.req.request &&
      //   tickeUUID.req.request.technician &&
      //   tickeUUID.req.request.technician.id,
      category: tickeUUID && tickeUUID.data && tickeUUID.data.category && tickeUUID.data.category.code,
    },
    validationSchema: validationSchema,
    onSubmit: submitForm,
  });

  console.log('requests', tickeUUID);
  const changeRequiesser = (value) => {
    if (value.key === 'category') {
      dispatch(getTicketSubCategory(value.value));
      setvalueCatagory([{ label: value.label }]);
    }
    if (value.key === 'subCategory') {
      dispatch(getTicketItem(value.value));
      setNameCatagory(value.label);
    }
    if (value.key === 'priority') {
      setValuePriority([{ label: value.label }]);
    }
    if (value.key === 'requesterid') {
      setRequests(value);
    }
    if (value.key === 'technician') {
      setNameTenicate(value.label);
    }
    if (value.key === 'trangthai') {
      setnameStatus(value.label);
    }

    setListRequesser({ ...listRequesser, [value.key]: value.value });
  };

  const filterRequesters = (inputValue, res) => {
    const selectRequester =
      res &&
      res.data &&
      res.data.users &&
      res.data.users.map((item) => ({
        value: item.id,
        label: `${item.name} - ${item.mobile}`,
        name: `${item.name} - ${item.mobile}`,
        key: 'requesterid',
        phone: item.mobile,
        login_name: item.login_name,
        project: item.project_roles,
        jobtitle: item.jobtitle,
        departmentRequeste: item.department && item.department.name,
      }));

    return selectRequester && selectRequester.filter((i) => i.label.toLowerCase().includes(inputValue.toLowerCase()));
  };

  useEffect(() => {
    dataContext.uuid && setActiveNumber(2);
  }, [dataContext.uuid]);

  const loadOptionRequester = (inputValue, callback) => {
    if (typingTimeOutRef.current) {
      clearTimeout(typingTimeOutRef.current);
    }

    if (inputValue !== '') {
      typingTimeOutRef.current = setTimeout(() => {
        dispatch(getTicketRequester({ search: inputValue }))
          .unwrap()
          .then((res) => {
            callback(filterRequesters(inputValue, res));
          });
      }, 400);
    }
  };

  useEffect(() => {
    techList &&
      techList.data &&
      techList.data.technicians &&
      setListTechnician(
        techList.data.technicians.map((v) => ({
          label: v.name,
          value: v.id,
          key: 'technician',
        }))
      );
  }, [techList]);

  return (
    <div className='content'>
      <div className='box-form'>
        <Row>
          <Col xs={12} sm={4}>
            <div className='form-input-box'>
              <div className='label-name'>
                Người Liên Hệ
                <span
                  className='fa fa-edit'
                  onClick={() => setEditOrg(!activeEditOrg)}
                  style={{
                    marginLeft: 15,
                    color: '#074d89',
                    cursor: 'pointer',
                  }}
                ></span>
              </div>
              <AsyncSelect
                placeholder={<div>Chọn</div>}
                className='basic-single'
                isDisabled={tickeUUID && tickeUUID.data && tickeUUID.data.contact && true}
                onChange={handleOptionChangeOrg}
                name='org'
                value={[{ name: nameContact || contact.fullname, label: nameContact || contact.fullname }]}
                loadOptions={loadOptions}
                debounceTimeout={5000}
              />

              <p style={{ color: 'red', fontSize: '12px' }}>{mes.name}</p>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className='form-input-box'>
              <div className='label-name'>Chức Vụ</div>
              <div className='d-flex'>
                <FormInput
                  disabled
                  type='text'
                  name='org_phone'
                  value={namePosition || contact.position}
                  placeholder='phongban'
                />
              </div>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className='form-input-box'>
              <div className='label-name'>Phòng Ban</div>
              <div className='d-flex'>
                <FormInput
                  disabled
                  type='text'
                  name='parent_org_phone'
                  value={nameDepartment || contact.department}
                  placeholder='phong ban'
                />
              </div>
            </div>
          </Col>
          <Col xs={12} sm={6}>
            <div className='form-input-box'>
              <div className='label-name'>Số Điện Thoại</div>
              <div className='d-flex'>
                <FormInput
                  disabled
                  type='text'
                  name='org_phone'
                  value={contactPhone || contact.mobile}
                  placeholder='don vi'
                />
              </div>
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className='form-input-box'>
              <div className='label-name'>Đơn Vị</div>
              <div className='d-flex'>
                <FormInput
                  disabled
                  type='text'
                  name='org_phone'
                  value={nameOrg || contact.organization}
                  placeholder='SĐT'
                />
              </div>
            </div>
          </Col>

          <Col xs={12}>
            <h3 className='title'>
              Thông tin Ticket{' '}
              {tickeUUID && tickeUUID.req && tickeUUID.req.request && tickeUUID.req.request.requester ? (
                ''
              ) : (
                <sub style={{ fontSize: '10px', color: 'red' }}>(Chưa tạo được yêu cầu, vui lòng tạo lại yêu cầu)</sub>
              )}
            </h3>
          </Col>
          <Col xs={12} sm={12}>
            <div className='form-input-box'>
              <div className='label-name'>Người yêu cầu </div>
              <AsyncSelect
                type='text'
                name='Requesser'
                value={{
                  label: requests && requests.name,

                  value: 2,
                }}
                placeholder={<div>Chọn</div>}
                onChange={changeRequiesser}
                loadOptions={loadOptionRequester}
                defaultOptions
                // isDisabled={tickeUUID && tickeUUID.req && tickeUUID.req.request && tickeUUID.req.request.requester}
              />
              <p style={{ color: 'red', fontSize: '12px' }}>{mes.requesterid}</p>
            </div>
            {requests && (
              <div style={{ display: 'flex', gap: '10px', marginTop: '10px', fontSize: '10px' }}>
                <p>
                  {' '}
                  <b>Số điện thoại :</b> {requests && requests.phone}
                </p>
                <p>
                  {' '}
                  <b>Nội dung :</b> {requests && requests.jobtitle}
                </p>
                <p>
                  <b>Tên đăng nhập :</b> {requests && requests.login_name}
                </p>
                <p>
                  <b> Phòng ban:</b> {requests && requests.departmentRequeste}
                </p>
              </div>
            )}
          </Col>

          <Col xs={12} sm={6}>
            <div className='form-input-box'>
              <div className='label-name'>Mức ưu tiên </div>
              <Select
                type='text'
                name='Priority'
                value={valuePriority || priroty1234}
                placeholder={<div>Chọn</div>}
                onChange={changeRequiesser}
                options={priroty}
                isDisabled={tickeUUID && tickeUUID.req && tickeUUID.req.request && tickeUUID.req.request.requester}
              ></Select>
              <p style={{ color: 'red', fontSize: '12px' }}>{mes.priority}</p>
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className='form-input-box'>
              <div className='label-name'>Trạng thái </div>
              <Select
                value={[
                  {
                    label:
                      nameStatus ||
                      (tickeUUID && tickeUUID.data && tickeUUID.data.trangthai && tickeUUID.data.trangthai.name),
                  },
                ]}
                type='text'
                name='Status'
                placeholder={<div>Chọn</div>}
                onChange={changeRequiesser}
                options={statuses}
              ></Select>
            </div>
          </Col>
          <Col xs={12} sm={6}>
            <div className='form-input-box'>
              <div className='label-name'>Danh mục hỗ trợ *</div>
              <Select
                value={valueCatagory || catagory1}
                type='text'
                name='Category'
                onChange={changeRequiesser}
                options={catagory}
                isDisabled={tickeUUID && tickeUUID.req && tickeUUID.req.request && tickeUUID.req.request.requester}
                placeholder={<div>Chọn</div>}
              ></Select>
              <p style={{ color: 'red', fontSize: '12px' }}>{mes.category}</p>
            </div>
          </Col>
          <Col xs={12} sm={6}>
            <div className='form-input-box'>
              <div className='label-name'>Danh mục nhỏ </div>
              <Select
                type='text'
                name='SubCategory'
                value={[
                  {
                    label:
                      nameSubCatagory ||
                      (tickeUUID &&
                        tickeUUID.req &&
                        tickeUUID.req.request &&
                        tickeUUID.req.request.subcategory &&
                        tickeUUID.req.request.subcategory.name),
                  },
                ]}
                placeholder={<div>Chọn</div>}
                onChange={changeRequiesser}
                options={subCategory}
                isDisabled={tickeUUID && tickeUUID.req && tickeUUID.req.request && tickeUUID.req.request.requester}
              ></Select>
              <p style={{ color: 'red', fontSize: '12px' }}>{mes.subCategory}</p>
            </div>
          </Col>

          {(activeNumber > 1 ||
            (tickeUUID &&
              tickeUUID.req &&
              tickeUUID.req.response_status &&
              tickeUUID.req.response_status.status_code &&
              tickeUUID.req.response_status.status_code === 2000)) && (
            <Col xs={12} sm={4}>
              <div className='form-input-box'>
                <div className='label-name'>Kỹ thuật viên</div>
                <Select
                  value={[
                    {
                      label:
                        nameTenicate ||
                        (tickeUUID &&
                          tickeUUID.req &&
                          tickeUUID.req.request &&
                          tickeUUID.req.request.technician &&
                          tickeUUID.req.request.technician.name) ||
                        (profile &&
                          profile.technician &&
                          listTechnician &&
                          listTechnician.find((v) => v.value === profile.technician.id_tech) &&
                          listTechnician.find((v) => v.value === profile.technician.id_tech).label),
                    },
                  ]}
                  type='text'
                  name='Status'
                  placeholder={<div>Chọn</div>}
                  onChange={changeRequiesser}
                  isDisabled
                  options={listTechnician}
                ></Select>
              </div>
            </Col>
          )}

          {tickeUUID &&
            tickeUUID.req &&
            tickeUUID.req.response_status &&
            tickeUUID.req.response_status.status_code &&
            tickeUUID.req.response_status.status_code === 2000 && (
              <>
                <Col xs={12} sm={4}>
                  <div className='form-input-box'>
                    <div className='label-name'>Ngày đến hạn</div>
                    <Select
                      value={[
                        {
                          label:
                            tickeUUID &&
                            tickeUUID.req &&
                            tickeUUID.req.request &&
                            tickeUUID.req.request.due_by_time &&
                            tickeUUID.req.request.due_by_time.display_value,
                        },
                      ]}
                      type='text'
                      name='Category'
                      onChange={changeRequiesser}
                      isDisabled
                      placeholder={<div>Chọn</div>}
                    ></Select>
                  </div>
                </Col>

                <Col xs={12} sm={4}>
                  <div className='form-input-box'>
                    <div className='label-name'>Nhóm</div>
                    <Select
                      value={[
                        {
                          label:
                            tickeUUID &&
                            tickeUUID.req &&
                            tickeUUID.req.request &&
                            tickeUUID.req.request.group &&
                            tickeUUID.req.request.group.name,
                        },
                      ]}
                      type='text'
                      name='Category'
                      onChange={changeRequiesser}
                      isDisabled
                      placeholder={<div>Chọn</div>}
                    ></Select>
                  </div>
                </Col>
              </>
            )}

          <Col xs={12}>
            <div className='form-input-box'>
              <div className='label-name'>Tên ticket</div>
              <FormInput
                type='text'
                disabled={!contextUUID && true}
                name='name'
                value={values.name}
                onChange={handleChange}
                placeholder='Tên ticket'
              />
              <p style={{ color: 'red', fontSize: '12px' }}>{mes.valueName}</p>
            </div>
          </Col>

          <Col xs={12}>
            <div className='form-input-box'>
              <div className='label-name'>Nội dung</div>
              <FormTextarea
                type='text'
                name='content'
                disabled={!contextUUID && true}
                placeholder='Nội dung'
                value={values.content}
                onChange={handleChange}
                onBlur={handleBlur}
                style={{ height: '70px' }}
              />
              <p style={{ color: 'red', fontSize: '12px' }}>{mes.content}</p>
            </div>
          </Col>

          <Col xs={12}>
            <div className='text-right'>
              <span className='btn' onClick={handleSubmit}>
                <i
                  className='fa fa-edit'
                  style={{
                    marginLeft: 15,
                  }}
                ></i>
                Cập nhập
              </span>
            </div>
          </Col>

          <Col xs={12}>
            <h3 className='title'>Lịch sử Ticket</h3>
            <TicketHistory ticketHistory={tickeUUID} />
          </Col>
          <Col xs={12}>
            <div className='form-input-box text-right'></div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default TicketDetail;
