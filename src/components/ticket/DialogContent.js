import React, { useState } from 'react';
import {
  Col,
} from 'shards-react';

const DialogContent = ({ content }) => {
  const [openModal, setOpenModal] = useState(false);

  // const handleCloseModal = () => {
  //   setOpenModal(false);
  // };


  return (
    <div >
      <Col xs={12} sm={12}>
        <div className="label-name">Nội dung : </div>
        <div className="label-name"> {content}</div>
        <div
          value={content}
          onClick={() => setOpenModal(!openModal)}
        />
      </Col>


      {/* <Col xs={12}>
        <div className="form-input-box text-right">
          <button
            style={{ marginRight: 10 }}
            className="btn"
            type="edit"
            onClick={handleCloseModal}>
            {dataEdit ? 'Đóng ' : 'Đóng '}
          </button>
        </div>
      </Col> */}
    </div>
  );
};

export default DialogContent;
