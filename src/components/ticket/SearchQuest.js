import React, { useState, useRef, useEffect } from 'react';
import { getInfo } from '../../erp/redux/info';
import { useDispatch } from 'react-redux';
import useOutsideClick from "../../hooks/useOutsideClick";

function SearchQuest() {
  const dispatch = useDispatch();
  const [searchToggle, setSearchToggle] = useState(false);
  const [keyword, setKeyWord] = useState(keyword ? keyword : '');
  const searchbarRef = useRef();
  const searchInputRef = useRef();

   useOutsideClick(searchbarRef,()=> {
        if(searchToggle){
          setSearchToggle(false)
          setActiveClear(false);
        }
   })

  const handleSearchToggle = () => {
    searchInputRef.current.focus();
    setSearchToggle(!searchToggle);
  };

  const clearSearchToggle = () => {
    setKeyWord('');
  };

  const handleSearchInput = event => {
    const { value } = event.target;
    setKeyWord(value);
  };

  useEffect(() => {
    dispatch(getInfo({ question:keyword }));
  }, [keyword]);

  const [activeClear, setActiveClear] = useState(false);

  const onFocus = e => {
    setActiveClear(true);
  };

  return (
    <div className="search-bar" ref={searchbarRef}>
      <input
        type="text"
        placeholder="Tìm kiếm nội dung"
        value={keyword}
        onChange={handleSearchInput}
        ref={searchInputRef}
        className={`form-control ${searchToggle && 'active'}`}
        onFocus={onFocus}
      />

      {/* {!searchToggle ? (
        <span
          className="btn"
          style={{ width: '36px', minWidth: 'inherit' }}
          onClick={handleSearchToggle}>
          <i className="fa fa-search"></i>
        </span>
      ) : null} */}
      {activeClear && (
        <span
          onClick={() => {
            setKeyWord('');
          }}
          className="clear-search">
          <i className="fa fa-times"></i>
        </span>
      )}
      {/* <div onClick={clearSearchToggle}></div> */}
    </div>
  );
}

export default SearchQuest;
