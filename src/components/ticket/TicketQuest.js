import React, { useState } from 'react';
import SearchQuest from './SearchQuest';
import Draggable from 'react-draggable'; // The default
import { Resizable } from 're-resizable';

const TicketQuest = ({ infoList, setUDO, openModalInfos }) => {
  const [select, setSelect] = useState('');
  const [dataQuest, setDataQuest] = useState('');
  const [openModalExpand, setOpenModalExpand] = useState(false);

  function toggleExpand() {
    let btnClick = document.querySelector('.btn-viewmore');
    btnClick.classList.toggle('hidden');
  }
  return (
    <div className="box-quest">
      <div className="title">
        Tài liệu  
        <div className="btn-quest-close" onClick={() => setUDO(!openModalInfos)}>
          <span className="fa fa-times"></span>
        </div>
      </div>
      <SearchQuest />

      {openModalExpand && (
        <Draggable>
          <div className="popup-quest-expand">
            <Resizable
              defaultSize={{
                width: 300,
                height: 400
              }}>
              <h3 className="pu-heading">Nội dung</h3>
              <div className="popup-close" onClick={() => setOpenModalExpand(!openModalExpand)}>
                <span className="fa fa-times"></span>
              </div>
              <div className="content">
                <div className="box-form">{dataQuest}</div>
              </div>
            </Resizable>
          </div>
        </Draggable>
      )}

      {infoList && infoList.data &&
        infoList.data.map((item, index) => {
          return (
            <div key={index} className={`faq-item ${select === index ? 'active' : ''}`}>
              <div
                className={`faq-heading ${select === index ? 'active' : ''}`}
                onClick={() => setSelect(index)}>
                <div className="number_faq">{index + 1}</div>
                <div className="txt">{item.question}</div>
                <span className="icon-sub fa icon-plus"></span>
              </div>
              <div className={`box-content collapse ${select === index ? 'in' : ''}`}>
                {item.answer}
                {select === index && (
                  <div
                    className="btn btn-viewmore"
                    onClick={() => {
                      toggleExpand();
                      setOpenModalExpand(true);
                      setDataQuest(item.answer);
                    }}>
                    Xem thêm 
                  </div>
                )}
              </div>
            </div>
          );
        })}
    </div>
  );
};

export default TicketQuest;
