import React, { useEffect } from 'react';
import { FormInput, Row, Col } from 'shards-react';
import { useDispatch, useSelector } from 'react-redux';
import { createUser } from '../../erp/redux/user';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { getPosition } from '../../erp/redux/position';
import Select from 'react-select';
import { HelpText } from '../components-overview/Help-text';
import { phoneRegExp } from '../../constants';
const DialogUser = ({ setUDO }) => {
  const dispatch = useDispatch();

  const positionList = useSelector(state => state.position.positionList);

  const positionOptions = positionList &&  positionList.results &&  positionList.results.map(option => ({
    value: option.id,
    label: option.name
  }));
  const validationSchema = yup.object({
    email: yup
    .string()
    .trim()
    //   .email('Email không hợp lệ')
    .required('Nhập Email'),
    fullname: yup
    .string()
    .trim()
    //   .email('Email không hợp lệ')
    .required('Nhập Tên'),
    phone: yup.string()
    .required("required")
    .matches(phoneRegExp, 'SĐT không đúng định dạng')
    .min(10, "to short")
    .max(10, "to long"),
  });

  const submitForm = values => {

    dispatch(createUser(values));
    setUDO(false);
  };

  useEffect(() => {
    dispatch(getPosition());
  }, []);

  const handleChangePosition = newValue => {
    if (newValue != null) {
      setFieldValue('position', newValue.value);
    }
  };

  const {
    values,
    handleSubmit,
    handleChange,
    setFieldValue,
    handleBlur,
    errors,
    touched,
  } = useFormik({
    initialValues: {},
    validationSchema: validationSchema,
    onSubmit: submitForm
  });
  return (
    <div className="content" style={{ background: '#e7e7e7', borderRadius: '0.5rem' }}>
      <h3 className="title">Tạo nhân viên</h3>
      <div className="box-form">
        <Row>
          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Tên đăng nhập </div>
              <FormInput
                type="text"
                name="username"
                placeholder="Tên"
                onChange={handleChange}
                onBlur={handleBlur}

              />
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Email</div>
              <FormInput
                type="text"
                name="email"
                placeholder="Email"
                onChange={handleChange}
                onBlur={handleBlur}

              />
                                          <HelpText status="error">
                {touched.email && errors.email ? errors.email : ''}
              </HelpText>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className="form-input-box">
              <div className="label-name">Họ</div>
              <FormInput
                type="text"
                name="first_name"
                placeholder="Họ"
                value={values.first_name}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className="form-input-box">
              <div className="label-name">Tên</div>
              <FormInput
                type="text"
                name="last_name"
                placeholder="Tên"
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className="form-input-box">
              <div className="label-name">SĐT</div>
              <FormInput
                type="text"
                name="phone"
                placeholder="SĐT"
                onChange={handleChange}
                onBlur={handleBlur}
              />
                          <HelpText status="error">
                {touched.phone && errors.phone ? errors.phone : ''}
              </HelpText>
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Mở rộng</div>
              <FormInput
                type="text"
                name="Máy lẻ"
                placeholder="Extensions"
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Vị trí </div>
              <Select
                type="select"
                name="position"
                placeholder={<div>Chọn</div>}
                onChange={handleChangePosition}
                onBlur={handleBlur}
                options={positionOptions}></Select>
            </div>
          </Col>

          <Col xs={12}>
            <div className="form-input-box text-right">
              <button
                style={{ width: '36px', minWidth: 'inherit' }}
                className="btn"
                type="submit"
                onClick={handleSubmit}>
                <i className="fa fa-plus"></i>
              </button>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default DialogUser;
