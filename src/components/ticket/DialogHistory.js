import React, { useRef, useEffect } from 'react';
import { Pagination } from '../common/Pagination';
import { usePaginationState } from '../../hooks/use-pagination';
import { useDispatch, useSelector } from 'react-redux';
import { getHistory } from '../../erp/redux/ticket';
const DialogHistory = ({ phone, loading }) => {
  const historyList = useSelector((state) => state.ticket.historyList);
  console.log(historyList);
  const audioRef = useRef(null);
  const pagination = usePaginationState();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      getHistory({
        phone: phone,
        limit: pagination.perPage,
        offset: pagination.perPage * pagination.page - pagination.perPage,
      })
    );
  }, [pagination]);
  return (
    <div className='content'>
      {loading ? (
        <div className='dot-pulse m-auto'></div>
      ) : (
        <div className='table-wrapper'>
          <table className='table mb-0'>
            <thead className='bg-light'>
              <tr>
                <th scope='col' className='border-0'>
                  STT
                </th>
                <th scope='col' className='border-0'>
                  Nguồn
                </th>

                <th scope='col' className='border-0'>
                  Đích
                </th>
                <th scope='col' className='border-0'>
                  Thời gian
                </th>
                <th scope='col' className='border-0'>
                  File ghi âm
                </th>
              </tr>
            </thead>
            <tbody>
              {historyList &&
                historyList.cdr &&
                historyList.cdr.length > 0 &&
                historyList.cdr.map((item, index) => {
                  return (
                    <tr key={index}>
                      <td>{index + 1}</td>
                      <td>{item.src}</td>
                      <td>{item.dst}</td>
                      <td>{item.duration}</td>
                      <td>
                        <audio src={`${item.recordingfile}`} ref={audioRef} controls />
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
          <Pagination
            currentPage={pagination.page}
            pageSize={pagination.perPage}
            lastPage={Math.min(
              Math.ceil((historyList ? parseInt(historyList.total) : 0) / pagination.perPage),
              Math.ceil((historyList ? parseInt(historyList.total) : 0) / pagination.perPage)
            )}
            onChangePage={pagination.setPage}
            onChangePageSize={pagination.setPerPage}
            onGoToLast={() =>
              pagination.setPage(Math.ceil((historyList ? parseInt(historyList.total) : 0) / pagination.perPage))
            }
          />
        </div>
      )}
    </div>
  );
};

export default DialogHistory;
