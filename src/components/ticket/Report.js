import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { exportReport, getReport } from '../../erp/redux/report';
import { saveAs } from 'file-saver';
import Chart from 'react-apexcharts';
import moment from 'moment';
import axiosInstance from '../../erp/service/config';
const Report = () => {
  const dispatch = useDispatch();
  const date = new Date();

  const [active, setActive] = useState(true);
  const [call, setCall] = useState();

  const [value, setValue] = useState({
    from: moment(date).format('DD/MM/YYYY'),
    to: moment(date).format('DD/MM/YYYY'),
  });
  const [valueInput, setValueInput] = useState({
    from: moment(date).format('YYYY-MM-DD'),
    to: moment(date).format('YYYY-MM-DD'),
  });

  const reportList = useSelector((state) => state.report.report);

  const listDashboard = reportList && reportList.dashboards && reportList.dashboards.dashboard;
  const newTicket = reportList && reportList.dashboards && reportList.dashboards.newTicket;
  const process = reportList && reportList.dashboards && reportList.dashboards.process;
  const sucsess = reportList && reportList.dashboards && reportList.dashboards.sucsess;
  const ass = reportList && reportList.dashboards && reportList.dashboards.ass_total;
  useEffect(() => {
    dispatch(getReport());
  }, []);

  const STATUS_ORDER = {
    Closed: 'Đã ',
    Open: 'Mở',
    Onhold: 'Tạm ',
    Resolved: 'Hoàn ',
    Assigned: 'Đã ',
    InProgress: 'Đang ',
    Cancelled: 'Bị',
  };

  const [data, setData] = useState({
    options: {
      chart: {
        type: 'bar',
        height: 100,
        backgroundColor: '#0099FF',
        // stacked: true,
        toolbar: {
          // show: true,
        },
        zoom: {
          enabled: true,
        },
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              position: 'bottom',
              offsetX: -10,
              offsetY: 0,
            },
          },
        },
      ],
      plotOptions: {
        bar: {
          horizontal: false,
          borderRadius: 5,
          backgroundColor: 'red',
        },
      },
      xaxis: {
        type: 'text',
        categories: Object.values(STATUS_ORDER),
      },
      legend: {
        position: 'right',
        offsetY: 40,
      },
      fill: {
        opacity: 1,
      },
    },
    series: [
      {
        name: 'Tổng số',
        data: listDashboard && Object.keys(listDashboard).map((v) => listDashboard[v]),
      },
    ],
  });

  useEffect(() => {
    const fakeData = listDashboard && Object.keys(listDashboard).map((v) => listDashboard[v]);
    const label = listDashboard && Object.keys(listDashboard);

    setData({
      ...data,
      options: {
        ...data.options,
        xaxis: {
          type: 'text',
          categories: label,
        },
      },
      series: [
        {
          name: 'Tổng số',
          data: fakeData,
        },
      ],
      labels: listDashboard && Object.keys(listDashboard),
    });
  }, [listDashboard]);

  const [config, setConfig] = useState({
    options: {
      chart: {
        toolbar: {
          show: true,
          tools: {
            download: true,
            selection: true,
          },
        },
      },
      plotOptions: {
        radialBar: {
          offsetY: -10,
          startAngle: 0,
          endAngle: 200,
          hollow: {
            margin: 15,
            size: '0%',
            background: 'transparent',
            image: undefined,
          },
          dataLabels: {
            name: {
              show: false,
            },
            value: {
              show: false,
            },
          },
        },
      },
      height: 100,
      colors: ['#66FF00', '#CCCC00', '#FF9900', '#FF6600', '#0000FF'],
      labels: ['Mở', 'Đang tiến hành', 'Chờ phản Hồi', 'Đã Đóng', 'Mở lại'],
      legend: {
        show: true,
        floating: true,
        fontSize: '10px',
        position: 'left',

        offsetX: 60,
        offsetY: 10,
        labels: {
          useSeriesColors: true,
        },
        markers: {
          size: 0,
        },
        formatter: function(seriesName, opts) {
          return seriesName + ':  ' + opts.w.globals.series[opts.seriesIndex];
        },
        itemMargin: {
          horizontal: 1,
        },
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              show: false,
            },
          },
        },
      ],
    },
    series: [
      {
        name: 'Tổng số',
        data: [42, 39, 36, 32, 59, 52, 39],
      },
    ],
  });

  const [assData, setAssData] = useState({
    options: {
      chart: {
        // height: '200px',
        toolbar: {
          show: true,
          tools: {
            download: true,
            selection: true,
          },
        },
      },
      plotOptions: {
        radialBar: {
          offsetY: -10,
          startAngle: 0,
          endAngle: 170,
          hollow: {
            margin: 5,
            size: '10%',
            background: 'transparent',
            image: undefined,
          },
          dataLabels: {
            name: {
              show: false,
            },
            value: {
              show: false,
            },
          },
        },
      },

      colors: ['#66FF00', '#CCCC00', '#FF9900', '#FF6600', '#0000FF'],
      labels: ['Mở', 'Đang tiến hành', 'Chờ phản Hồi', 'Đã Đóng', 'Mở lại'],
      legend: {
        show: true,
        // floating: true,
        fontSize: '14px',
        // position: 'left',
        // offsetX: 60,
        // offsetY: 10,
        labels: {
          useSeriesColors: true,
        },
        markers: {
          size: 0,
        },
        formatter: function(seriesName, opts) {
          return seriesName + ':  ' + opts.w.globals.series[opts.seriesIndex];
        },
        itemMargin: {
          horizontal: 1,
        },
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              // show: false,
            },
          },
        },
      ],
    },
    series: [
      {
        name: 'Tổng số',
        data: [42, 39, 36, 32, 59, 52, 39],
      },
    ],
  });

  useEffect(() => {
    const datafake = ass && ass.map((v) => v.count);
    const label = ass && ass.map((v) => `${v.create_by__username}`);

    setAssData({
      ...assData,
      options: { ...assData.options, labels: label },
      series: (datafake && datafake) || [],
    });
  }, [listDashboard]);

  useEffect(() => {
    const datafake = listDashboard && Object.keys(listDashboard).map((v) => listDashboard[v]);
    const sum = datafake && datafake.reduce((a, b) => a + b, 0);

    const data = datafake && sum && datafake.map((v) => Math.floor((v / sum) * 100));

    const label = listDashboard && Object.keys(listDashboard);
    setConfig({
      ...config,
      options: { ...config.options, labels: label },
      series: (data && data) || [],
    });
  }, [listDashboard]);

  const handleChange = (e) => {
    setValue({ ...value, [e.target.name]: moment(e.target.value).format('DD/MM/YYYY') });
    setValueInput({ ...valueInput, [e.target.name]: moment(e.target.value).format('YYYY-MM-DD') });
  };

  const submitExport = () => {
    console.log('value', value);

    // const download = async () => {
    //   let params = `?from=${value.from}&to=${value.to}`;
    //   await axiosInstance.post(`/api/report/${params}`).then((response) => console.log(response));
    // };
    // download();
    dispatch(exportReport(value))
      .unwrap()
      .then((res) => {
        let blob = new Blob([res], {
          type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8',
        });

        let datetime = `${value.from}_to_${value.to}`;
        saveAs(blob, `result_${datetime}.xlsx`);
      });
  };

  const submit = () => {
    dispatch(getReport(value));
  };

  console.log('reportList', call);

  useEffect(() => {
    setCall(
      reportList &&
        reportList.dashboards &&
        reportList.dashboards.callsummary &&
        reportList.dashboards.callsummary.summary[0]
    );
  }, [reportList]);
  return (
    <div style={{ margin: '20px' }}>
      <div
        style={{
          display: 'flex',
          flexDirection: 'row-reverse',
          alignItems: 'center',
          gap: '10px',
          marginBottom: '10px',
        }}
      >
        <div style={{ ...cssButton }} onClick={submitExport}>
          export
        </div>
        <div style={{ ...cssButton }} onClick={submit}>
          Hiển thị
        </div>
        <input name='to' type='date' value={valueInput.to} onChange={handleChange} />
        <div>Kết thúc </div>
        <input name='from' type='date' value={valueInput.from} onChange={handleChange} />
        <div>Bắt đầu </div>
      </div>
      {/* div 0 */}
      <div
        style={{
          display: 'grid',
          gridTemplateColumns: '31% 31% 31%',
          height: '100px',
          gap: '3%',
          marginBottom: '15px',
        }}
      >
        <div
          style={{
            ...cssBorder,
            display: 'flex',
            border: 'none',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
            backgroundImage: 'linear-gradient(to right,#2E3192, #1BFFFF)',
          }}
        >
          <div>
            <b>Tổng số cuộc gọi</b>
          </div>
          <div>{call && call.num_incoming_call}</div>
        </div>
        <div
          style={{
            ...cssBorder,
            border: 'none',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
            backgroundImage: 'linear-gradient(to right,   #009245, #FCEE21)',
          }}
        >
          <div>
            <b>Tổng số phút</b>
          </div>
          <div>{call && call.minute_incoming_call}</div>
        </div>
        <div
          style={{
            ...cssBorder,
            display: 'flex',
            border: 'none',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
            backgroundImage: 'linear-gradient(to right,#f12711, #f5af19)',
          }}
        >
          <div>
            <b>Tổng số cuộc gọi nhỡ</b>
          </div>
          <div>{call && call.num_missed_call}</div>
        </div>
      </div>
      {/* div1 */}
      <div
        style={{
          display: 'grid',
          gridTemplateColumns: '66% 32%',
          gap: '15px',
        }}
      >
        <div style={{ ...cssBorder }}>
          <div style={{ height: '250px', overflowY: 'scroll' }}>
            <div style={{ fontSize: '12px', marginTop: '10px', marginLeft: '10px' }}>
              <b>TICKET ĐANG MỞ</b>
            </div>
            <table style={{ fontSize: '12px', marginLeft: '10px' }}>
              <tr style={{ color: '#0099FF', fontWeight: '550' }}>
                <td style={{ width: '50%' }}>Tiêu đề</td>
                <td style={{ width: '12.5%' }}>Trạng thái</td>
                <td style={{ width: '12.5%' }}>Ưu tiên</td>
                <td style={{ width: '23%' }}>Danh mục</td>
              </tr>
              {newTicket &&
                newTicket.map((v, i) => (
                  <tr style={{ height: '35px' }} key={i}>
                    <td>
                      <p
                        style={{ width: '370px', whiteSpace: 'nowrap', overflow: ' hidden', textOverflow: 'ellipsis' }}
                      >
                        {' '}
                        {v.name}
                      </p>
                    </td>
                    <td>{v.trangthai && v.trangthai.name}</td>
                    <td
                      style={{
                        color:
                          v.priority && v.priority.name === 'Low'
                            ? '#0099FF'
                            : v.priority && v.priority.name === 'High'
                            ? 'red'
                            : '#FF9900',
                      }}
                    >
                      {v.priority && v.priority.name}
                    </td>
                    <td>{v.category && v.category.name}</td>
                  </tr>
                ))}
            </table>
          </div>
          <div
            style={{ background: '#EEEEEE', height: '30px', marginTop: '17px', borderRadius: '0px 0px 5px 5px' }}
          ></div>
        </div>
        <div style={{ ...cssBorder }}>
          <div style={{ fontSize: '12px', marginTop: '10px', marginLeft: '10px' }}>
            <b>THỐNG KÊ TICKET THEO TÌNH TRẠNG</b>
          </div>
          <div>
            <Chart options={data.options} series={data.series} type='bar' width='100%' />
          </div>
          <div
            style={{ background: '#EEEEEE', height: '30px', marginTop: '1px', borderRadius: '0px 0px 5px 5px' }}
          ></div>
        </div>
      </div>
      {/* div 2 */}
      <div style={{ display: 'grid', gridTemplateColumns: '36% 62%', gap: '1.5%', height: '505px', marginTop: '15px' }}>
        <div>
          <div style={{ ...cssBorder, height: '245px', overflowY: 'scroll' }}>
            <div style={{ fontSize: '12px', marginTop: '10px', marginLeft: '10px' }}>
              <b>TICKET ĐANG TIẾN HÀNH</b>
            </div>
            <table style={{ width: '100%', fontSize: '12px', marginLeft: '10px' }}>
              <tr style={{ color: '#0099FF', fontWeight: '550' }}>
                <td style={{ width: '50%' }}>Tiêu đề</td>

                <td style={{ width: '12.5%' }}>Ưu tiên</td>
                <td style={{ width: '23%', marginRight: '10px' }}>Danh mục</td>
              </tr>
              {process &&
                process.map((v) => (
                  <tr style={{ height: '35px' }}>
                    <td style={{ width: '50%' }}>
                      <p
                        style={{ width: '160px', whiteSpace: 'nowrap', overflow: ' hidden', textOverflow: 'ellipsis' }}
                      >
                        {' '}
                        {v.name}
                      </p>
                    </td>
                    <td
                      style={{
                        color:
                          v.priority && v.priority.name === 'Low'
                            ? '#0099FF'
                            : v.priority && v.priority.name === 'High'
                            ? 'red'
                            : '#FF9900',
                      }}
                    >
                      {v.priority && v.priority.name}
                    </td>
                    <td>{v.category && v.category.name}</td>
                  </tr>
                ))}
            </table>

            <div
              style={{ background: '#EEEEEE', height: '30px', marginTop: '7px', borderRadius: '0px 0px 5px 5px' }}
            ></div>
          </div>
          <div style={{ ...cssBorder, height: '245px', marginTop: '15px', overflowY: 'scroll' }}>
            <div style={{ fontSize: '12px', marginTop: '10px', marginLeft: '10px' }}>
              <b>TICKET ĐANG CHỜ PHẢN HỒI</b>
            </div>
            <table style={{ width: '100%', fontSize: '12px', marginLeft: '10px' }}>
              <tr style={{ color: '#0099FF', fontWeight: '550' }}>
                <td style={{ width: '50%' }}>Tiêu đề</td>

                <td style={{ width: '12.5%' }}>Ưu tiên</td>
                <td style={{ width: '23%', marginRight: '10px' }}>Danh mục</td>
              </tr>
              {sucsess &&
                sucsess.map((v) => (
                  <tr style={{ height: '35px' }}>
                    <td style={{ width: '50%' }}>
                      <p
                        style={{ width: '160px', whiteSpace: 'nowrap', overflow: ' hidden', textOverflow: 'ellipsis' }}
                      >
                        {' '}
                        {v.name}
                      </p>
                    </td>
                    <td
                      style={{
                        color:
                          v.priority && v.priority.name === 'Low'
                            ? '#0099FF'
                            : v.priority && v.priority.name === 'High'
                            ? 'red'
                            : '#FF9900',
                      }}
                    >
                      {v.priority && v.priority.name}
                    </td>
                    <td>{v.category && v.category.name}</td>
                  </tr>
                ))}
            </table>
            <div
              style={{ background: '#EEEEEE', height: '30px', marginTop: '7px', borderRadius: '0px 0px 5px 5px' }}
            ></div>
          </div>
        </div>

        <div style={{ ...cssBorder }}>
          <div style={{ fontSize: '12px', marginTop: '10px', marginLeft: '10px' }}>
            <div style={{ display: 'flex', gap: '10px' }}>
              <div
                style={{ borderBottom: active && '3px solid red', color: active && '#0099FF', cursor: 'pointer' }}
                onClick={() => {
                  console.log('thin');
                  setActive(true);
                }}
              >
                BÁO CÁO TÌNH TRẠNG THEO NGƯỜI
              </div>
              <div
                style={{ borderBottom: !active && '3px solid red', color: !active && '#0099FF', cursor: 'pointer' }}
                onClick={() => {
                  console.log('thin1');
                  setActive(false);
                }}
              >
                {' '}
                BÁO CÁO TÌNH TRẠNG THEO TRẠNG THÁI
              </div>
            </div>
          </div>
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              height: '300px',
              marginTop: '90px',
            }}
          >
            <div
              style={{
                width: '80%',
                display: 'grid',
                gridTemplateColumns: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                padding: '40px',
              }}
            >
              {' '}
              {active && <Chart options={assData.options} series={assData.series} type='pie' />}
              {!active && <Chart options={config.options} series={config.series} type='radialBar' />}
            </div>
          </div>
          <div
            style={{ background: '#EEEEEE', height: '30px', marginTop: '7px', borderRadius: '0px 0px 5px 5px' }}
          ></div>
        </div>
      </div>
    </div>
  );
};

export default Report;

export const cssBorder = {
  borderRadius: '5px',
  border: '1px solid gray',
  display: 'grid',
  alignContent: 'space-between',
};
export const cssButton = {
  height: '30px',
  backgroundColor: '#074d89',
  width: '80px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: '5px',
  color: 'white',
  fontWeight: '600',
  cursor: 'pointer',
};
