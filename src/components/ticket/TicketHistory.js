import React, { useState, useRef, useEffect } from 'react';
import moment from 'moment';
import { trangthaiList } from '../../constants';
import DialogContent from '../ticket/DialogContent';

const TicketHistory = ({ ticketHistory }) => {
  const [openModal, setOpenModal] = useState(false);
  const [content, setContent] = useState('');
  const [disable, setDisable] = useState('');

  const filterTrangthai = (trangthai) => {
    const found = trangthaiList.filter((item) => item.id === parseInt(trangthai));
    return found.length && found[0].name;
  };
  useEffect(() => {
    const arr =
      ticketHistory &&
      ticketHistory.history &&
      ticketHistory.history[0] &&
      ticketHistory.history[0].length &&
      ticketHistory.history[0].map((s) => false);
    setOpenModal(arr);
  }, []);
  const wrapperRef = useRef(null);
  const closeModal = (e) => {
    if (e.target === wrapperRef.current) setOpenModal(false);
  };

  return (
    <div>
      {ticketHistory && (
        <div className='form-input-box'>
          <div className='table-wrapper'>
            <table className='table mb-0'>
              <thead className='bg-light'>
                <tr>
                  <th scope='col' className='border-0'>
                    STT
                  </th>
                  <th scope='col' className='border-0'>
                    Tên Ticket
                  </th>

                  <th scope='col' className='border-0'>
                    SĐT
                  </th>
                  <th scope='col' className='border-0'>
                    Ngày tạo
                  </th>
                  <th scope='col' className='border-0'>
                    Tình trạng
                  </th>
                  <th scope='col' className='border-0'>
                    Chi tiết
                  </th>
                </tr>
              </thead>

              {ticketHistory &&
                ticketHistory.history &&
                ticketHistory.history[0] &&
                ticketHistory.history[0].length > 0 &&
                ticketHistory.history[0].map((item, idx) => {
                  return (
                    <tbody key={idx}>
                      <tr>
                        <td>{idx + 1}4</td>
                        <td>{item.name}</td>
                        <td>{item.phone}</td>
                        <td>{moment(item.created_on).format('DD-MM-YYYY')}</td>
                        <td>{item.trangthai !== null ? filterTrangthai(item.trangthai) : ''}</td>
                        <td>
                          <a
                            href='#'
                            onClick={() => {
                              const arr = openModal;
                              arr[idx] = true;
                              setOpenModal(arr);
                            }}
                          >
                            {' '}
                            Xem thêm
                          </a>
                        </td>
                      </tr>
                      <tr>
                        {openModal[idx] === true && (
                          <td colspan={6}>
                            {' '}
                            <DialogContent
                              setUDO={setOpenModal}
                              setContent={setContent}
                              dataEdit={content}
                              style={{ width: '600px' }}
                              content={item.content}
                            />{' '}
                          </td>
                        )}
                      </tr>
                    </tbody>
                  );
                })}
            </table>
          </div>
        </div>
      )}
    </div>
  );
};

export default TicketHistory;
