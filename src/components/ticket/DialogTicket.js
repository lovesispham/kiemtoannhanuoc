import React, { useEffect, useRef, useState } from 'react';
import { useContext } from 'react';
import { TicketContext } from '../../components/notification/TicketContent';
import { FormTextarea, FormSelect, FormInput, Row, Col } from 'shards-react';
import { useDispatch, useSelector } from 'react-redux';
import {
  createTicket,
  getRequest,
  getTechnican,
  getTicketCategory,
  getTicketGroup,
  getTicketItem,
  getTicketMode,
  getTicketPriroty,
  getTicketRequester,
  getTicketSite,
  getTicketStatus,
  getTicketSubCategory,
} from '../../erp/redux/ticket';
import { trangthaiList, priorityList } from '../../constants';
import { useFormik } from 'formik';
import * as yup from 'yup';
import Select from 'react-select';
import AsyncSelect from 'react-select/async';
import { getUser } from '../../erp/redux/user';
import { getOrg, getOrgId } from '../../erp/redux/org';
import TicketCreateContact from './TicketCreateContact';
import { getContact, getContactId } from '../../erp/redux/contact';
import TicketCreateOrg from './TicketCreateOrg';
import TicketCreateAssign from './TicketCreateAssign';
import RequiredText from '../common/RequiredText';

const DialogTicket = ({
  setUDO,
  openModal,
  activeEditOrg,
  activeEditAssign,
  activeEditContact,
  setEditContact,
  setEditOrg,
  setEditAssign,
}) => {
  const dataContext = useContext(TicketContext);

  const profile = dataContext.profile;
  const dispatch = useDispatch();
  const userList = useSelector((state) => state.user.userList);
  const orgList = useSelector((state) => state.org.orgList);
  const { successCreate } = useSelector((state) => state.contact);
  const listSite = useSelector((state) => state.ticket.siteList);
  const listPriroty = useSelector((state) => state.ticket.prirotyList);
  const listCatagori = useSelector((state) => state.ticket.categoryList);
  const listGroup = useSelector((state) => state.ticket.groupList);
  const listMode = useSelector((state) => state.ticket.modeList);
  const listItem = useSelector((state) => state.ticket.listSubCategory);
  const listStatus = useSelector((state) => state.ticket.statusList);
  const listSubCatagory = useSelector((state) => state.ticket.listSubCategory);
  const successCreateOrg = useSelector((state) => state.org.successCreate);
  const successCreateUser = useSelector((state) => state.user.successCreate);
  const contactList = useSelector((state) => state.contact.contactList);
  const techList = useSelector((state) => state.ticket.technicanList);
  const requestList = useSelector((state) => state.ticket.requestList);

  const [contactPhone, setContactPhone] = useState('');
  const [nameOrg, setNameOrg] = useState('');
  const [nameDepartment, setNameDepartment] = useState('');
  const [namePosition, setNamePosition] = useState('');
  const [contactID, setContactID] = useState('');
  const [listRequesser, setListRequesser] = useState({ priority: '2', trangthai: '3' });
  const [requests, setRequests] = useState();
  const [listTechnician, setListTechnician] = useState([]);
  const [mes, setMes] = useState({});
  const [defaultPriority, setDefaultPriority] = useState();
  const [defaultStatus, setDefaultStatus] = useState();
  const [technicianName, setTechnicianName] = useState();
  const [activeSubCatagory, setActiveSubCatagory] = useState(false);
  const typingTimeOutRef = useRef(null);
  const orgSelect =
    orgList &&
    orgList.result &&
    orgList.result.map((item) => ({
      value: item.id,
      label: item.name,
      phone: item.phone,
      manage_org: item.manage_org,
      parent_org: item.parent_org,
    }));

  const item =
    listItem &&
    listItem.data &&
    listItem.data.items &&
    listItem.data.items.map((item) => ({
      value: item.id,
      label: item.name,
      name: 'item',
    }));

  const subCategory =
    listSubCatagory &&
    listSubCatagory.data &&
    listSubCatagory.data.subcategories &&
    listSubCatagory.data.subcategories.map((item) => ({
      value: item.id,
      label: item.name,
      name: 'subCategory',
    }));

  const statuses =
    listStatus &&
    listStatus.data &&
    listStatus.data &&
    listStatus.data.map((item) => ({
      value: item.id,
      label: item.name,

      name: 'trangthai',
    }));

  const site =
    listSite &&
    listSite.data &&
    listSite.data.sites &&
    listSite.data.sites.map((item) => ({
      value: item.id,
      label: item.name,
    }));

  const priroty =
    listPriroty &&
    listPriroty.data &&
    listPriroty.data.priorities &&
    listPriroty.data.priorities.map((item) => ({
      value: item.id,
      label: item.name,
      name: 'priority',
    }));

  const catagory =
    listCatagori &&
    listCatagori.data &&
    listCatagori.data.categories &&
    listCatagori.data.categories.map((item) => ({
      value: item.id,
      label: item.name,
      name: 'category',
    }));

  const group =
    listGroup &&
    listGroup.data &&
    listGroup.data.groups &&
    listGroup.data.groups.map((item) => ({
      value: item.id,
      label: item.name,
    }));

  useEffect(() => {
    CallApi();
  }, []);

  const CallApi = async () => {
    await dispatch(getContact({}));
    await dispatch(getTechnican());
    await dispatch(getRequest());
    await dispatch(getTicketSite({}));
    await dispatch(getTicketPriroty({}));
    await dispatch(getTicketCategory({}));
    await dispatch(getTicketStatus({}));
  };

  useEffect(() => {
    if (successCreate === true) {
      dispatch(getContact());
    }
  }, [successCreate]);

  useEffect(() => {
    if (successCreateOrg === true) {
      dispatch(getOrg());
    }
  }, [successCreateOrg]);

  useEffect(() => {
    if (successCreateUser === true) {
      dispatch(getUser());
    }
  }, [successCreateUser]);

  const changeTechName = (newValue) => {
    if (newValue != null) {
      setFieldValue('assignid', newValue.value);
    }
  };

  const handleChangeRequest = (newValue) => {
    if (newValue != null) {
      setFieldValue('requesterid', newValue.value);
    }
  };

  const handleOptionChangeContact = (value) => {
    dispatch(getContactId(value.value));
    setContactPhone(value.phone);
    setFieldValue('contact', value.value);
  };

  const handleOptionChangeOrg = (value) => {
    // dispatch(getOrgId(value.value));
    setContactID(value.value);
    setNameOrg(value.organization);
    setNameDepartment(value.department);
    setContactPhone(value.phone);
    setNamePosition(value.position);
  };

  useEffect(() => {
    if (openModal === false) {
      setNameOrg('');
      setNameDepartment('');
      setContactPhone('');
      setNamePosition('');
      setContactID('');
    }
  }, [openModal]);

  const validationSchema = yup.object();

  const submitForm = (values) => {
    if (!contactID) {
      setMes({ name: 'Không được để trống người liên hệ' });
      return;
    }
    if (listRequesser && !listRequesser.requesterid) {
      setMes({ requesterid: 'Không được để trống người yêu cầu' });
      return;
    }
    if (listRequesser && !listRequesser.category) {
      setMes({ category: 'Không được để trống danh mục hỗ trợ' });
      return;
    }
    if (listRequesser && !listRequesser.priority) {
      setMes({ priority: 'Không được để trống mức độ ưu tiên' });
      return;
    }
    if (values && !values.name) {
      setMes({ valueName: 'Không được để trống tên ticket' });
      return;
    }
    if (values && !values.content) {
      setMes({ content: 'Không được để trống nội dung ticket' });
      return;
    }

    setMes({});

    const newValue = {
      ...values,
      ...listRequesser,
      phone: contactPhone,
      contact: contactID,
      technician: listRequesser.technician || (profile && profile.technician && profile.technician.id_tech),
      trangthai: statuses && statuses.find((v) => v.value === '1').value,
    };

    dispatch(createTicket(newValue));
    setUDO(false);
  };

  const { values, handleSubmit, handleChange, setFieldValue, handleBlur, errors, touched } = useFormik({
    initialValues: {
      trangthai: statuses && statuses.find((v) => v.value === '1').value,
    },
    validationSchema: validationSchema,
    onSubmit: submitForm,
  });

  useEffect(() => {
    techList &&
      techList.data &&
      techList.data.technicians &&
      setListTechnician(
        techList.data.technicians.map((v) => ({
          label: v.name,
          value: v.id,
          name: 'technician',
        }))
      );
  }, [techList]);

  const filterColors = (inputValue, res) => {
    const contactSelect =
      res &&
      res.contacts &&
      res.contacts.contacts &&
      res.contacts.contacts.map((item) => ({
        value: item.id,
        label: item.fullname,
        phone: item.mobile,
        department: item.department,
        position: item.position,
        organization: item.organization,
      }));
    return contactSelect && contactSelect.filter((i) => i.label.toLowerCase().includes(inputValue.toLowerCase()));
  };

  const loadOptions = (inputValue, callback) => {
    if (typingTimeOutRef.current) {
      clearTimeout(typingTimeOutRef.current);
    }

    if (inputValue !== '') {
      typingTimeOutRef.current = setTimeout(() => {
        dispatch(getContact({ name: inputValue }))
          .unwrap()
          .then((res) => {
            callback(filterColors(inputValue, res));
          });
      }, 400);
    }
  };

  const filterRequesters = (inputValue, res) => {
    const selectRequester =
      res &&
      res.data &&
      res.data.users &&
      res.data.users.map((item) => ({
        value: item.id,
        label: `${item.name} - ${item.mobile}`,
        name: 'requesterid',
        phone: item.mobile,
        login_name: item.login_name,
        project: item.project_roles,
        jobtitle: item.jobtitle,
        departmentRequeste: item.department && item.department.name,
      }));

    return selectRequester && selectRequester.filter((i) => i.label.toLowerCase().includes(inputValue.toLowerCase()));
  };

  const loadOptionRequester = (inputValue, callback) => {
    if (typingTimeOutRef.current) {
      clearTimeout(typingTimeOutRef.current);
    }

    if (inputValue !== '') {
      typingTimeOutRef.current = setTimeout(() => {
        dispatch(getTicketRequester({ search: inputValue }))
          .unwrap()
          .then((res) => {
            callback(filterRequesters(inputValue, res));
          });
      }, 400);
    }
  };

  const changeRequiesser = (value) => {
    if (value.name === 'category') {
      dispatch(getTicketSubCategory(value.value));
      setActiveSubCatagory(true);
    }
    if (value.name === 'subCategory') {
      dispatch(getTicketItem(value.value));
    }
    if (value.name === 'requesterid') {
      setRequests(value);
    }
    if (value.name === 'priority') {
      setDefaultPriority(value);
    }
    if (value.name === 'trangthai') {
      setDefaultStatus(value);
    }
    if (value.name === 'technician') {
      setTechnicianName(value.label);
    }

    setListRequesser({ ...listRequesser, [value.name]: value.value });
  };

  const changeRequesser = (value) => {};

  return (
    <div className='content' style={{ height: '650px', overflowY: 'scroll' }}>
      <div className='box-form'>
        <Row>
          <Col xs={12}>
            <h3 className='title'>Cán Bộ Liên Hệ</h3>
          </Col>
          <Col xs={12} sm={4}>
            <div className='form-input-box'>
              <div className='label-name'>Người Liên Hệ</div>
              <AsyncSelect
                placeholder={<div>Chọn</div>}
                className='basic-single'
                onChange={handleOptionChangeOrg}
                loadOptions={loadOptions}
                defaultOptions
                name='org'
                // options={contactSelect}
              />
              <p style={{ color: 'red', fontSize: '12px' }}>{mes.name}</p>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className='form-input-box'>
              <div className='label-name'>Chức Vụ</div>
              <div className='d-flex'>
                <FormInput disabled type='text' name='org_phone' value={namePosition} placeholder='Chức vụ' />
              </div>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className='form-input-box'>
              <div className='label-name'>Phòng Ban</div>
              <div className='d-flex'>
                <FormInput
                  disabled
                  type='text'
                  name='parent_org_phone'
                  value={nameDepartment}
                  placeholder='Phòng ban'
                />
              </div>
            </div>
          </Col>
          <Col xs={12} sm={6}>
            <div className='form-input-box'>
              <div className='label-name'>Số điện Thoại</div>
              <div className='d-flex'>
                <FormInput disabled type='text' name='org_phone' value={contactPhone} placeholder='Số diện thoại' />
              </div>
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className='form-input-box'>
              <div className='label-name'>Đơn Vị</div>
              <div className='d-flex'>
                <FormInput disabled type='text' name='org_phone' value={nameOrg} placeholder='Đơn vị' />
              </div>
            </div>
          </Col>

          {/* {
            activeEditOrg && (
              <TicketCreateOrg setUDO={setEditOrg} />
            )
          }




          {
            activeEditContact && (
              <TicketCreateContact setUDO={setEditContact} />
            )
          } */}

          <Col xs={12}>
            <h3 className='title'>Thông tin Ticket</h3>
          </Col>

          <Col xs={12} sm={12}>
            <div className='form-input-box'>
              <div className='label-name'>Người yêu cầu </div>
              <AsyncSelect
                type='text'
                name='Requesser'
                placeholder={<div>Chọn</div>}
                onChange={changeRequiesser}
                loadOptions={loadOptionRequester}
                defaultOptions
              />
              <p style={{ color: 'red', fontSize: '12px' }}>{mes.requesterid}</p>
            </div>
            {requests && (
              <div style={{ display: 'flex', gap: '10px', marginTop: '10px', fontSize: '10px' }}>
                <p>
                  {' '}
                  <b>Số điện thoại :</b> {requests.phone}
                </p>
                <p>
                  {' '}
                  <b>Nội dung :</b> {requests.jobtitle}
                </p>
                <p>
                  <b>Tên đăng nhập :</b> {requests.login_name}
                </p>
                <p>
                  <b> Phòng ban:</b> {requests.departmentRequeste}
                </p>
              </div>
            )}
          </Col>
          {/* <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Site<RequiredText/></div>
              <Select

                type="text"
                name="Site"
                placeholder={<div>Chọn</div>}
                onChange={changeRequiesser}
                options={site}></Select>
            </div>
          </Col> */}
          <Col xs={12} sm={6}>
            <div className='form-input-box'>
              <div className='label-name'>Mức ưu tiên </div>
              <Select
                type='text'
                name='Priority'
                value={defaultPriority || (priroty && priroty[0])}
                placeholder={<div>Chọn</div>}
                onChange={changeRequiesser}
                options={priroty}
              ></Select>
              <p style={{ color: 'red', fontSize: '12px' }}>{mes.priority}</p>
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className='form-input-box'>
              <div className='label-name'>Trạng thái </div>
              <Select
                type='text'
                name='Status'
                value={statuses && statuses.find((v) => v.value === '1')}
                placeholder={<div>Chọn</div>}
                onChange={changeRequiesser}
                isDisabled
                options={''}
              ></Select>
            </div>
          </Col>
          <Col xs={12} sm={6}>
            <div className='form-input-box'>
              <div className='label-name'>
                Danh mục hỗ trợ
                <RequiredText />
              </div>
              <Select
                type='text'
                name='Category'
                placeholder={<div>Chọn</div>}
                onChange={changeRequiesser}
                options={catagory}
              ></Select>
              <p style={{ color: 'red', fontSize: '12px' }}>{mes.category}</p>
            </div>
          </Col>
          {activeSubCatagory && (
            <Col xs={12} sm={6}>
              <div className='form-input-box'>
                <div className='label-name'>Danh mục nhỏ </div>
                <Select
                  type='text'
                  name='SubCategory'
                  placeholder={<div>Chọn</div>}
                  onChange={changeRequiesser}
                  options={subCategory}
                ></Select>
              </div>
            </Col>
          )}

          <Col xs={12} sm={6}>
            <div className='form-input-box'>
              <div className='label-name'>
                Kỹ thuật viên
                <RequiredText />
              </div>
              <Select
                type='text'
                name='Priority'
                value={[
                  {
                    label:
                      technicianName ||
                      (techList &&
                        techList.data &&
                        techList.data.technicians &&
                        profile.technician &&
                        techList.data.technicians.find((v) => v.id === profile.technician.id_tech) &&
                        techList.data.technicians.find((v) => v.id === profile.technician.id_tech).name),
                  },
                ]}
                placeholder={<div>Chọn</div>}
                onChange={changeRequiesser}
                options={listTechnician}
                isDisabled={profile.technician}
              ></Select>
              <p style={{ color: 'red', fontSize: '12px' }}>{mes.category}</p>
            </div>
          </Col>

          {/* <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Request Type </div>
              <Select

                type="text"
                name="RequestType"
                placeholder={<div>Chọn</div>}
                onChange={changeTechName}
                options={techOptions}></Select>
            </div>
          </Col> */}

          {/* <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Mode </div>
              <Select

                type="text"
                name="Mode"
                placeholder={<div>Chọn</div>}
                onChange={changeTechName}
                options={mode}></Select>
            </div>
          </Col> */}
          {/* <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Item </div>
              <Select

                type="text"
                name="Item"
                placeholder={<div>Chọn</div>}
                onChange={changeRequiesser}
                options={item}></Select>
            </div>
          </Col> */}

          {/*
          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Nhóm hỗ trợ<RequiredText/></div>
              <Select

                type="text"
                name="Group"
                placeholder={<div>Chọn</div>}
                onChange={changeTechName}
                options={group}></Select>
            </div>
          </Col> */}

          {/*
          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Trạng Thái</div>
              <FormSelect
                id="trangthai"
                name="trangthai"
                value={values.trangthai}
                onChange={e => {
                  setFieldValue('trangthai', e.target.value);
                }}>
                {trangthaiList.map((item, index) => {
                  return (
                    <option key={index} value={item.id}>
                      {item.name}
                    </option>
                  );
                })}
              </FormSelect>
            </div>
          </Col> */}

          <Col xs={12}>
            <div className='form-input-box'>
              <div className='label-name'>
                Tên ticket <RequiredText />
              </div>
              <FormInput type='text' name='name' value={values.name} onChange={handleChange} placeholder='Tên ticket' />
              <p style={{ color: 'red', fontSize: '12px' }}>{mes.valueName}</p>
            </div>
          </Col>

          <Col xs={12}>
            <div className='form-input-box'>
              <div className='label-name'>Nội dung ticket</div>
              <FormTextarea
                type='text'
                name='content'
                placeholder='Nội dung'
                value={values.content}
                onChange={handleChange}
                onBlur={handleBlur}
                style={{ height: '70px' }}
              />
              <p style={{ color: 'red', fontSize: '12px' }}>{mes.content}</p>
            </div>
          </Col>
          <Col xs={12}>
            <div className='form-input-box text-right'>
              <button className='btn' type='submit' onClick={handleSubmit}>
                Tạo
              </button>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default DialogTicket;
