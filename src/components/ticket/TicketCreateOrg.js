import React, { useEffect, useState } from 'react';
import { FormInput, Row, Col, FormCheckbox } from 'shards-react';
import { useDispatch, useSelector } from 'react-redux';
import { createOrg, getOrgFull } from '../../erp/redux/org';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { getCity, getDistrict, getWard } from '../../erp/redux/location';
import Select from 'react-select';
import { HelpText } from '../components-overview/Help-text';
import { phoneRegExp } from '../../constants';

const DialogUCOrg = ({ setUDO }) => {
  const dispatch = useDispatch();
  const [active, setActive] = useState(false)

  const cityList = useSelector(state => state.location.cityList);
  const districtList = useSelector(state => state.location.districtList);
  const wardList = useSelector(state => state.location.wardList);

  const orgList = useSelector(state => state.org.orgListFull)

  useEffect(() => {
    dispatch(getCity());
    dispatch(getOrgFull())
  }, []);

  const validationSchema = yup.object({
    email: yup
    .string()
    .trim()
    //   .email('Email không hợp lệ')
    .required('Nhập Email'),
    phone: yup.string()
    .required("required")
    .matches(phoneRegExp, 'SĐT không đúng định dạng')
    .min(10, "to short")
    .max(10, "to long"),
  });

  const submitForm = values => {

    dispatch(createOrg(values));
    setUDO(false);
  };

    // option list org

    const orgOptions = orgList && orgList.result && orgList.result.length > 0 && orgList.result.map(option => ({
      value: option.id,
      label: option.name
    }))

  const cityOptions = cityList.map(option => ({
    value: option.id,
    label: option.name
  }));


  const districtOptions = districtList.map(option => ({
    value: option.id,
    label: option.name
  }));


  const wardOptions = wardList.map(option => ({
    value: option.id,
    label: option.name
  }));

  const handleChangeCityAuto = newValue => {
    if (newValue != null) {
      setFieldValue('city', newValue.value);
      dispatch(getDistrict(newValue.value));
    }
  };

  const handleChangeDistrictAuto = newValue => {
    if (newValue != null) {
      setFieldValue('district', newValue.value);
      dispatch(getWard(newValue.value));
    }
  };

  const handleChangeWardAuto = async newValue => {
    if (newValue != null) {
      setFieldValue('ward', newValue.value);
    }
  };

  const handleChangeOrg = async (newValue) => {

    if (newValue != null) {

      setFieldValue("parent_org", newValue.value);
    }
  };

  const handleChangeCheckbox = (e) => {
    const {checked} = e.target;
    setActive(checked)
    setFieldValue("is_parent", checked);
  }

  const {
    values,
    handleSubmit,
    handleChange,
    setFieldValue,
    handleBlur,
    errors,
    touched
  } = useFormik({
    initialValues: {},
    validationSchema: validationSchema,
    onSubmit: submitForm
  });
  return (
    <div className="content" style={{ background: '#e7e7e7', borderRadius: '0.5rem' }}>
      <h3 className="title">Tạo đơn vị</h3>
      <div className="box-form">
        <Row>
          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Tên</div>
              <FormInput
                type="text"
                name="name"
                placeholder="Tên"
                value={values.name}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Email</div>
              <FormInput
                type="text"
                name="email"
                placeholder="Email"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <HelpText status="error">
                {touched.email && errors.email ? errors.email : ''}
              </HelpText>
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">SĐT</div>

              <FormInput
                type="text"
                name="phone"
                placeholder="SĐT"
                value={values.phone}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <HelpText status="error">
                {touched.phone && errors.phone ? errors.phone : ''}
              </HelpText>
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Quản lý </div>

              <FormInput
                type="text"
                name="manage_org"
                placeholder="Quản lý "
                value={values.manage_org}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
          </Col>
          <Col xs={12} sm={4}>
            <div className="form-input-box">
              
              <FormCheckbox 
                name='is_parent'
                checked={active}
                onChange={handleChangeCheckbox}
                >
                  Thuộc đơn vị
                </FormCheckbox>

              
            </div>
          </Col>
          <Col xs={12} sm={8}>
          {
            active && (
              
            <div className="form-input-box">
            <div className="label-name">Chọn đơn vị kiểm toán </div>

            <Select
                type="select"
                name="parent_org"
                placeholder={<div>Chọn</div>}
                onChange={handleChangeOrg}
                options={orgOptions}
              >
              </Select>

              
            </div>
         
            )
          }
          </Col>
          <Col xs={12} sm={4}>
            <div className="form-input-box">
              <div className="label-name">Thành phố </div>

              <Select
                type="select"
                name="city"
                placeholder={<div>Chọn</div>}
                onChange={handleChangeCityAuto}
                onBlur={handleBlur}
                options={cityOptions}></Select>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className="form-input-box">
              <div className="label-name">Quận/Huyện</div>

              <Select
                type="select"
                name="district"
                placeholder={<div>Chọn</div>}
                onChange={handleChangeDistrictAuto}
                onBlur={handleBlur}
                options={districtOptions}></Select>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className="form-input-box">
              <div className="label-name">Phường/Xã </div>

              <Select
                type="select"
                name="ward"
                placeholder={<div>Chọn </div>}
                onChange={handleChangeWardAuto}
                onBlur={handleBlur}
                options={wardOptions}></Select>
            </div>
          </Col>

          <Col xs={12}>
            <div className="form-input-box text-right">
              <button
                style={{ width: '36px', minWidth: 'inherit' }}
                className="btn"
                type="submit"
                onClick={handleSubmit}>
                <i className="fa fa-plus"></i>
              </button>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default DialogUCOrg;
