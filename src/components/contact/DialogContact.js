import React, { useEffect, useState } from 'react';
import {
  FormInput,
  Row,
  Col,
} from 'shards-react';
import { useDispatch, useSelector } from 'react-redux';
import { createContact, editContact } from '../../erp/redux/contact';

import { useFormik } from 'formik';
import * as yup from 'yup';
import { getPosition } from '../../erp/redux/position'
import { getOrg } from '../../erp/redux/org'
import Select from 'react-select'
import { HelpText } from '../components-overview/Help-text';
import RequiredText from '../common/RequiredText';
import { getdepartment, selectListDepartment } from '../../erp/redux/department';
const DialogContact = ({ dataEdit, setUDO, setData }) => {
  const dispatch = useDispatch();

  const positionList = useSelector(state => state.position.positionList);
  const departmentList = useSelector(selectListDepartment)
  const orgList = useSelector(state => state.org.orgList);

  useEffect(() => {
    console.log('thin');
    dispatch(getOrg({}))
    dispatch(getPosition({}))
    dispatch(getdepartment({}))
  }, []);

  const handleChangeOrg = (newValue) => {

    if (newValue != null) {
      setOrgName(newValue.label);
      setFieldValue("org", newValue.value);

    }
  };

  const handleChangePosition = (newValue) => {
    if (newValue != null) {
      setPosName(newValue.label);
      setFieldValue("position", newValue.value);

    }
  };

  const orgOptions = orgList && orgList.result && orgList.result.map(option => ({
    value: option.id,
    label: option.name
  }));

  const departmentOptions = departmentList &&  departmentList.result &&  departmentList.result.map(option => ({
    value: option.id,
    label: option.name
  }));


  const [orgName, setOrgName] = useState(
    // dataEdit !== undefined && dataEdit !== null && dataEdit !== '' ? ( dataEdit.org !== null && dataEdit.org.name !== null ? dataEdit.org.name : '') : ''
    dataEdit && dataEdit.organization ? dataEdit.organization.name : ''
  );

  const [posName, setPosName] = useState(
    // dataEdit !== undefined && dataEdit !== null && dataEdit !== '' ? (dataEdit.position !== null && dataEdit.position.name !== null ? dataEdit.position.name : '') : ''
  dataEdit && dataEdit.department ? dataEdit.department.name :''
    );


console.log('posName',dataEdit);
  const submitForm = values => {

    if (dataEdit) {
      dispatch(editContact(values));
    } else {
      dispatch(createContact(values));

    }
    setUDO(false);
    setData('');
  };

  /////////////validate các thông tin

  const phoneRegExp = ('^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$')
  const emailRegExp = ("([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])")

  const validationSchema = yup.object({
    fullname: yup
      .string()
      .trim()
      .required('Nhập tên đăng nhập'),
    phone: yup
      .string()
      .trim()
      .matches(phoneRegExp, 'Phone Number is invalid')
      .required('Bạn chưa nhập mật khẩu'),
      email: yup
      .string()
      .trim()
      .matches(emailRegExp, 'Email is invalid')
      .required('Bạn chưa nhập mật khẩu')
  });


  const {
    values,
    handleSubmit,
    handleChange,
    setFieldValue,
    handleBlur,
    errors,
    touched
  } = useFormik({
    initialValues: !dataEdit
      ? {}
      : {
        id: dataEdit.id,
        phone: dataEdit.phone === null ? '' : dataEdit.phone,
        name: dataEdit.name === null ? '' : dataEdit.name,
        email: dataEdit.email === null ? '' : dataEdit.email,
        fullname: dataEdit.fullname === null ? '' : dataEdit.fullname,
      },
    validationSchema: validationSchema,
    onSubmit: submitForm
  });
  return (
    <div className="content">
      <div className="box-form">
        <Row>
          <Col xs={12} sm={4}>
            <div className="form-input-box">
              <div className="label-name">Tên {!dataEdit && (<RequiredText/>)}</div>
              <FormInput
                type="text"
                name="fullname"
                placeholder="Tên"
                value={values.fullname}
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.fullname && errors.fullname ? 'error' : undefined}
              />
            </div>
            <HelpText status='error'>
              {touched.fullname && errors.fullname ? errors.fullname : ''}
            </HelpText>
          </Col>

          <Col xs={12} sm={4}>
            <div className="form-input-box">
              <div className="label-name">SĐT {!dataEdit && (<RequiredText/>)}</div>

              <FormInput
                type="text"
                name="phone"
                placeholder="SĐT"
                value={values.phone}
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.phone && errors.phone ? 'error' : undefined}
              />
              <HelpText status="error">
                {touched.phone && errors.phone ? errors.phone : ''}
              </HelpText>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className="form-input-box">
              <div className="label-name">Email {!dataEdit && (<RequiredText/>)}</div>

              <FormInput
                type="text"
                name="email"
                placeholder="Email"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.email && errors.email ? 'error' : undefined}
              />
            </div>
            <HelpText status='error'>
              {touched.email && errors.email ? errors.email : ''}
            </HelpText>
          </Col>

           <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Phòng ban </div>

              <Select
                type="select"
                name="position"
                placeholder={<div>Chọn</div>}
                value={departmentOptions && departmentOptions.filter(option => option.label === posName)}
                onChange={handleChangePosition}
                onBlur={handleBlur}
                options={departmentOptions}
              >
              </Select>
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className="form-input-box">
              <div className="label-name">Đơn vị   </div>
              <Select
                type="select"
                name="org"
                placeholder={<div>Chọn</div>}
                value={orgOptions && orgOptions.filter(option => option.label === orgName)}
                onChange={handleChangeOrg}
                options={orgOptions}
              >


              </Select>
            </div>
          </Col>

          <Col xs={12}>
            <div className="form-input-box text-right">
              <button
                className="btn"
                type="submit"
                onClick={handleSubmit}>
                {!dataEdit ? 'Tạo' : 'Sửa'}
              </button>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default DialogContact;
