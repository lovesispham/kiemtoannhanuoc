import React,{useEffect, useState} from "react";

const Spinner = ({ heading, loading, top }) => {
  return (
    <div id="upload-loading" className='loading' style={{display:`${loading === false ? 'none' :'flex'}`, top:top}}>
      <div className="box-text-loading">
        {heading && <h3 className="title text-center" style={{fontSize:28}}>{heading}</h3>}
        <i style={{ marginTop: 50 }} className="spinning-loader"></i>
      </div>
    </div>
  );
};

export default Spinner;
