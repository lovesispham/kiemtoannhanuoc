import React from 'react'

const RequiredText = () => {
    const asterisk =<span className="required">*</span>;
  return (
    <>
       {asterisk}
    </>
  )
}

export default RequiredText