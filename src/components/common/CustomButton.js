import React, { useEffect, useRef } from 'react';

const CustomButton = props => {
  const btnRef = useRef();

  useEffect(() => {
    const btnWidth = btnRef.current.clientWidth + 29;
    btnRef.current.style.width = btnWidth + 'px';
  }, []);

  return (
    <button ref={btnRef} className="btn" onClick={props.onClick}>
      {props.icon && (
        <span className={`spinner ${props.loading ? 'active' : ''}`}>
          <i className="fa fa-spinner fa-spin"></i>
        </span>
      )}

      {props.children}
    </button>
  );
};

export default CustomButton;
