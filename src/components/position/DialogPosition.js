import React from 'react';
import {
  FormInput,
  Row,
  Col
} from 'shards-react';
import { useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { createPosition, editPosition } from '../../erp/redux/position';
import { HelpText } from '../components-overview/Help-text';

const DiaglogPosition = ({ dataEdit, setUDO, setData }) => {
  const dispatch = useDispatch();

  const submitForm = values => {

    if (dataEdit) {
      dispatch(editPosition(values));
      // dispatch(getPosition(values));
    } else {
      dispatch(createPosition(values));
    }
    setUDO(false);
    setData('');
  };

  ////////////////////validate phòng ban

  const validationSchema = yup.object({
    name:yup
    .string()
    .trim()
    .required('Nhập phòng ban')
  });

  const {
    values,
    handleSubmit,
    handleChange,
    setFieldValue,
    handleBlur,
    errors,
    touched
  } = useFormik({
    initialValues: !dataEdit
      ? {}
      : {
          id: dataEdit.id,
          name: dataEdit.name === null ? '' : dataEdit.name,
        },

    validationSchema: validationSchema,
    onSubmit: submitForm
  });
  return (
    <div className="content">
      <div className="box-form">
        <Row>
          <Col xs={12} sm={12}>
            <div className="form-input-box">
              <div className="label-name">Tên Chức vụ </div>
              <FormInput
                type="text"
                name="name"
                placeholder="Tên chức vụ"
                value={values.name}
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.name && errors.name ? 'error' : undefined}
              />
              <HelpText status='error'>
                {touched.name && errors.name ? errors.name : ' '}
              </HelpText>
            </div>
          </Col>

          <Col xs={12}>
            <div className="form-input-box text-right">
              <button
                className="btn"
                type="submit"
                onClick={handleSubmit}>
                {!dataEdit ? 'Tạo' : 'Sửa'}
              </button>
            </div>
            </Col>
        </Row>
      </div>
    </div>
  );
};

export default DiaglogPosition;
