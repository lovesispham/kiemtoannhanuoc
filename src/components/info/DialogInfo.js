import React from 'react';
import {
  FormInput,
  Row,
  Col,
  FormTextarea
} from 'shards-react';
import { useDispatch } from 'react-redux';
import { createInfo, editInfo } from '../../erp/redux/info';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { HelpText } from '../components-overview/Help-text';

const DialogInfo = ({ dataEdit, setUDO, setData }) => {
  const dispatch = useDispatch();

  const submitForm = values => {

    if (dataEdit) {
      dispatch(editInfo(values));
    } else {
      dispatch(createInfo(values));
    }
    setUDO(false);
    setData('');
  };

  const validationSchema = yup.object({
    question: yup
      .string()
      .trim()
      .required('Nhập câu hỏi')
  })
  const {
    values,
    handleSubmit,
    handleChange,
    setFieldValue,
    handleBlur,
    errors,
    touched
  } = useFormik({
    initialValues: !dataEdit
      ? {}
      : {
        id: dataEdit.id,
        question: dataEdit.question === null ? '' : dataEdit.question,
        answer: dataEdit.answer === null ? '' : dataEdit.answer
      },
    validationSchema: validationSchema,
    onSubmit: submitForm
  });
  return (
    <div className="content">
      <div className="box-form">
        <Row>
          <Col xs={12} sm={12}>
            <div className="form-input-box">
              <div className="label-name">Câu hỏi </div>
              <FormInput
                type="text"
                name="question"
                placeholder="Tên văn bản "
                value={values.question}
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.question && errors.question ? 'error' : undefined}
              />
            </div>
            <HelpText status='error'>
              {touched.question && errors.question ? errors.question : ''}
            </HelpText>
          </Col>

          <Col xs={12} sm={12}>
            <div className="form-input-box">
              <div className="label-name">Câu trả lời </div>
              <FormTextarea
                type="textarea"
                name="answer"
                placeholder="Loại văn bản"
                value={values.answer}
                onChange={handleChange}
                onBlur={handleBlur} rows='6'
              />
            </div>
          </Col>

          <Col xs={12}>
            <div className="form-input-box text-right">
              <button
                className="btn"
                type="submit"
                onClick={handleSubmit}>
                {!dataEdit ? 'Tạo' : 'Sửa'}
              </button>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default DialogInfo;
