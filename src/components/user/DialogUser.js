import React, { useEffect, useState } from 'react';
import { FormInput, Row, Col } from 'shards-react';
import { useDispatch, useSelector } from 'react-redux';
import { createUser, editUser } from '../../erp/redux/user';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { getPosition } from '../../erp/redux/position';
import Select from 'react-select';
import { HelpText } from '../../components/components-overview/Help-text';
import RequiredText from '../common/RequiredText';
import { getTechnican } from '../../erp/redux/ticket';
const DialogUser = ({ dataEdit, setUDO, setData }) => {
  const dispatch = useDispatch();

  const positionList = useSelector((state) => state.position.positionList);
  const techList = useSelector((state) => state.ticket.technicanList);
  const [listTechnician, setListTechnician] = useState([]);
  const [technician, setTechnician] = useState();
  const [technicianName, setTechnicianName] = useState();

  const positionOptions =
    positionList &&
    positionList.results &&
    positionList.results.map((option) => ({
      value: option.id,
      label: option.name,
    }));

  const submitForm = (values) => {
    console.log('technician', technician);
    if (dataEdit) {
      dispatch(editUser({ ...values, technician: technician }));
      // dispatch(getUser(values))
    } else {
      dispatch(createUser({ ...values, technician: technician }));
    }
    setUDO(false);
    setData('');
  };

  useEffect(() => {
    dispatch(getPosition({}));
  }, []);

  const handleChangePosition = (newValue) => {
    if (newValue != null) {
      setPosName(newValue.label);
      setFieldValue('position', newValue.value);
    }
  };

  const [posName, setPosName] = useState(
    dataEdit !== undefined && dataEdit !== null && dataEdit !== ''
      ? dataEdit.positions !== null
        ? dataEdit.positions.position.name
        : ''
      : ''
  );

  /////////validate phone
  const phoneRegExp = '^[+]?[(]?[0-9]{3}[)]?[-s.]?[0-9]{3}[-s.]?[0-9]{4,6}$';
  //////////validate email
  const regEmail =
    "([!#-'*+/-9=?A-Z^-~-]+(.[!#-'*+/-9=?A-Z^-~-]+)*|\"([]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(.[!#-'*+/-9=?A-Z^-~-]+)*|[[\t -Z^-~]*])";
  const passwordRegExp = '^(?=.*[a-z])(?=.*[A-Z])(?=.*d)(?=.*[@$!%*?&])[A-Za-zd@$!%*?&]{8,10}$';

  const validationSchema = yup.object(
    !dataEdit && {
      last_name: yup
        .string()
        .trim()
        .required('Nhập tên'),
      first_name: yup
        .string()
        .trim()
        .required('Nhập họ'),
      username: yup
        .string()
        .trim()
        .required('Nhập tên đăng nhập'),
      password: yup
        .string()
        .trim()
        .required('Nhập tên mật khẩu'),
      email: yup
        .string()
        .trim()
        .matches(regEmail, 'Email không hợp lệ')
        .required('Nhập email'),
      phone: yup
        .string()
        .required('Nhập số điện thoại')
        .matches(phoneRegExp, 'Số điện thoại không hợp lệ  ')
        .min(10, 'to short')
        .max(10, 'to long'),
    }
  );

  const { values, handleSubmit, handleChange, setFieldValue, handleBlur, isSubmitting, errors, touched } = useFormik({
    initialValues: !dataEdit
      ? {}
      : {
          id: dataEdit.id,
          username: dataEdit.username === null ? '' : dataEdit.username,
          password: dataEdit.password === null ? '' : dataEdit.password,
          email: dataEdit.email === null ? '' : dataEdit.email,
          first_name: dataEdit.first_name === null ? '' : dataEdit.first_name,
          last_name: dataEdit.last_name === null ? '' : dataEdit.last_name,
          technician: dataEdit.technician,
          extention: dataEdit.ext === null ? '' : dataEdit.ext.exentions,
          phone: dataEdit.phone === null ? '' : dataEdit.phone.phone,
        },
    validationSchema: validationSchema,
    onSubmit: submitForm,
  });

  useEffect(() => {
    dispatch(getTechnican());
  }, []);
  useEffect(() => {
    console.log('techList', dataEdit);
    techList &&
      techList.data &&
      techList.data.technicians &&
      setListTechnician(
        techList.data.technicians.map((v) => ({
          label: v.name,
          value: v.id,
        }))
      );
  }, [techList]);

  const changeRequiesser = (value) => {
    setTechnician(value.value);
    setTechnicianName(value.label);
  };

  return (
    <div className='content'>
      <div className='box-form'>
        <Row>
          <Col xs={12} sm={4}>
            <div className='form-input-box'>
              <div className='label-name'>Tên đăng nhập {!dataEdit && <RequiredText />} </div>
              <FormInput
                type='text'
                name='username'
                placeholder='Tên đăng nhập'
                value={values.username}
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.username && errors.username ? 'error' : undefined}
                disabled={isSubmitting}
              />
              <HelpText status='error'>{touched.username && errors.username ? errors.username : ''}</HelpText>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className='form-input-box'>
              <div className='label-name'>Mật khẩu </div>
              <FormInput
                type='password'
                name='password'
                placeholder='Mật khẩu '
                value={values.password}
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.password && errors.password ? 'error' : undefined}
              />
              <HelpText status='error'>{touched.password && errors.password ? errors.password : ''}</HelpText>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className='form-input-box'>
              <div className='label-name'>Email {!dataEdit && <RequiredText />}</div>
              <FormInput
                type='text'
                name='email'
                placeholder='Email'
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.email && errors.email ? 'error' : undefined}
              />
              <HelpText status='error'>{touched.email && errors.email ? errors.email : ''}</HelpText>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className='form-input-box'>
              <div className='label-name'>Họ</div>
              <FormInput
                type='text'
                name='first_name'
                placeholder='Họ'
                value={values.first_name}
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.first_name && errors.first_name ? 'error' : undefined}
              />
              <HelpText status='error'>{touched.first_name && errors.first_name ? errors.first_name : ''}</HelpText>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className='form-input-box'>
              <div className='label-name'>Tên</div>
              <FormInput
                type='text'
                name='last_name'
                placeholder='Tên'
                value={values.last_name}
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.last_name && errors.last_name ? 'error' : undefined}
              />
              <HelpText status='error'>{touched.last_name && errors.last_name ? errors.last_name : ''}</HelpText>
            </div>
          </Col>

          <Col xs={12} sm={4}>
            <div className='form-input-box'>
              <div className='label-name'>SĐT</div>
              <FormInput
                type='text'
                name='phone'
                placeholder='SĐT'
                value={values.phone}
                onChange={handleChange}
                onBlur={handleBlur}
                status={touched.phone && errors.phone ? 'error' : undefined}
              />
              <HelpText status='error'>{touched.phone && errors.phone ? errors.phone : ''}</HelpText>
            </div>
          </Col>

          <Col xs={12} sm={6}>
            <div className='form-input-box'>
              <div className='label-name'>Máy lẻ</div>
              <FormInput
                type='text'
                name='extention'
                placeholder='Máy lẻ'
                value={values.extention}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
          </Col>
          <Col xs={12} sm={6}>
            <div className='form-input-box'>
              <div className='label-name'>Kỹ thuật viên</div>

              <Select
                type='text'
                name='Priority'
                value={[
                  {
                    label:
                      technicianName ||
                      (techList &&
                        techList.data &&
                        techList.data.technicians &&
                        dataEdit.technician &&
                        techList.data.technicians.find((v) => v.id === dataEdit.technician.id_tech).name),
                  },
                ]}
                placeholder={<div>Chọn</div>}
                onChange={changeRequiesser}
                options={listTechnician}
              ></Select>
            </div>
          </Col>

          <Col xs={12}>
            <div className='form-input-box text-right'>
              <button className='btn' type='submit' onClick={handleSubmit}>
                {!dataEdit ? 'Tạo' : 'Sửa'}
              </button>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default DialogUser;
