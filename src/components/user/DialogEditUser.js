import React, { useEffect, useState } from 'react';
import {
    FormInput,
    Row,
    Col,

} from 'shards-react';
import { useDispatch, useSelector } from 'react-redux';
import { editUser, getUser } from '../../erp/redux/user';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { getPosition } from '../../erp/redux/position';
import Select from 'react-select'
import { HelpText } from '../components-overview/Help-text';

const DialogEditUser = ({ dataEdit, setUDO, setData }) => {
    const dispatch = useDispatch();
    const { successCreate } = useSelector(state => state.user);
    const { successEdit } = useSelector(state => state.user);

    const positionList = useSelector(state => state.position.positionList)
    const { successCreateP } = useSelector(state => state.position);

    const [id, setId] = useState(getPosition)


    const positionOptions = positionList &&  positionList.results &&  positionList.results.map(option => ({
        value: option.id,
        label: option.name
    }))

    const submitForm = values => {

        if (dataEdit) {
            dispatch(editUser(values));
            dispatch(getUser(values))
        }
        setUDO(false);
        setData('');
    };

    useEffect(() => {
        if (successEdit === true) {
            setTimeout(() => {
                dispatch(getUser())
            }, 1000);
        }
    }, [successEdit]);

    useEffect(() => {
        dispatch(getUser())
    }, []);


    useEffect(() => {
        if (successCreateP === true) {
            setTimeout(() => {
                dispatch(getPosition())
            }, 1000);
        }
    }, [successCreateP]);

    useEffect(() => {
        dispatch(getPosition())
    }, []);

    const handleChangePosition = (newValue) => {

        if (newValue != null) {
            setEditPosition(newValue.value)
            setFieldValue("position", newValue.value);
            dispatch(getPosition(newValue.value))

        }
    };

    //////////////thay doi (edit)
    useEffect(() => {
        if (successCreate === true) {
            setTimeout(() => {
                dispatch(getUser())
            }, 1000);
        }
    }, [successCreate]);

    useEffect(() => {
        dispatch(getUser())
    }, []);


    ///////validate phone
    const phoneRegExp = ('^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$')
    //////////validate email
    const regEmail = ("([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])")
    // const passwordRegExp = ("^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$" )


    const validationSchema = yup.object({
        last_name: yup
        .string()
        .trim()
        .required('Nhập tên'),
        first_name: yup
        .string()
        .trim()
        .required('Nhập họ'),
        username: yup
        .string()
        .trim()
        .required('Nhập tên đăng nhập'),
        email: yup
            .string()
            .trim()
            .matches(regEmail, 'Email is not vaild')
            .required('Nhập email'),
        phone: yup.string()
        .required("Nhập số điện thoại")
        .matches(phoneRegExp, 'Số điện thoại không hợp lệ  ')
        .min(10, "to short")
        .max(10, "to long")
    });


    const {
        values,
        handleSubmit,
        handleChange,
        setFieldValue,
        handleBlur,
        errors,
        touched
    } = useFormik({
        initialValues: !dataEdit
            ? {}
            : {
                id: dataEdit.id,
                username: dataEdit.username === null ? '' : dataEdit.username,
                password: dataEdit.password === null ? '' : dataEdit.password,
                email: dataEdit.email === null ? '' : dataEdit.email,
                first_name: dataEdit.first_name === null ? '' : dataEdit.first_name,
                last_name: dataEdit.last_name === null ? '' : dataEdit.last_name,
                positions: dataEdit.positions === null ? '' : dataEdit.positions.position.id,
                extention: dataEdit.ext === null ? '' : dataEdit.ext.exentions,
                phone: dataEdit.phone === null ? '' : dataEdit.phone.phone,

            },
        validationSchema: validationSchema,
        onSubmit: submitForm
    });

    const [editPosition, setEditPosition] = useState(
        dataEdit !== null && dataEdit !==undefined && dataEdit!==''? (dataEdit.positions.position.name !== null ? dataEdit.positions.position.name : '') : ''
    )

    return (
        <div className="content">
            <div className="box-form">
                <Row>
                    <Col xs={12} sm={6}>
                        <div className="form-input-box">
                            <div className="label-name">Tên đăng nhập </div>
                            <FormInput
                                type="text"
                                name="username"
                                placeholder="Tên"
                                value={values.username}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                status={touched.username && errors.username ? 'error' : undefined}
                                />
                                <HelpText status="error">
                                    {touched.username && errors.username ? errors.username : ''}
                                </HelpText>
                        </div>
                    </Col>

                    <Col xs={12} sm={6}>
                        <div className="form-input-box">
                            <div className="label-name">Email</div>
                            <FormInput
                                type="text"
                                name="email"
                                placeholder="Email"
                                value={values.email}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                status={touched.email && errors.email ? 'error' : undefined}
                              />
                              <HelpText status="error">
                                {touched.email && errors.email ? errors.email : ''}
                              </HelpText>
                        </div>
                    </Col>

                    <Col xs={12} sm={6}>
                        <div className="form-input-box">
                            <div className="label-name">Tên</div>
                            <FormInput
                                type="text"
                                name="last_name"
                                placeholder="Tên"
                                value={values.last_name}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                status={touched.last_name && errors.last_name ? 'error' : undefined}
                                />
                                <HelpText status="error">
                                    {touched.last_name && errors.last_name ? errors.last_name : ''}
                                </HelpText>
                        </div>
                    </Col>

                    <Col xs={12} sm={6}>
                        <div className="form-input-box">
                            <div className="label-name">Họ</div>
                            <FormInput
                                type="text"
                                name="first_name"
                                placeholder="Họ"
                                value={values.first_name}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                status={touched.first_name && errors.first_name ? 'error' : undefined}
                                />
                                <HelpText status="error">
                                    {touched.first_name && errors.first_name ? errors.first_name : ''}
                                </HelpText>
                        </div>
                    </Col>

                    <Col xs={12} sm={6}>
                        <div className="form-input-box">
                            <div className="label-name">SĐT</div>
                            <FormInput
                                type="text"
                                name="phone"
                                placeholder="SĐT"
                                value={values.phone}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                status={touched.phone && errors.phone ? 'error' : undefined}
                            />
                            <HelpText status="error">
                                {touched.phone && errors.phone ? errors.phone : ''}
                            </HelpText>
                        </div>
                    </Col>

                    <Col xs={12} sm={6}>
                        <div className="form-input-box">
                            <div className="label-name">Máy lẻ</div>
                            <FormInput
                                type="text"
                                name="extention"
                                placeholder="Máy lẻ"
                                value={values.extention}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </div>
                    </Col>


                    <Col xs={12}>
                        <div className="form-input-box text-right">
                            <button
                                style={{ marginRight: 10 }}
                                className="btn"
                                type="submit"
                                onClick={handleSubmit}>
                                {!dataEdit ? 'Tạo' : 'Sửa'}
                            </button>
                        </div>
                    </Col>
                </Row>
            </div>
        </div>
    );
};

export default DialogEditUser;
