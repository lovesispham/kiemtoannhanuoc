const DASHBOARD = '/';
const SIGN_IN = '/login';
export const routeConstants = {
  DASHBOARD,
  SIGN_IN
};
export const TIMEOUT = 5000;

export const toast_type = {
  error:'error',
  success:'success'
}
export const phoneRegExp = ('^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$')

export const trangthaiList = [
  { id: 1, name: 'Đã đóng' },
  { id: 2, name: 'Mở' },
  { id: 3, name: 'Tạm chờ' },
  { id: 4, name: 'Hoàn thành' },
  { id: 5, name: 'Đã giao' },
  { id: 6, name: 'Đang xử lý' },
  { id: 7, name: 'Bị huỷ' },
];

export const priorityList = [
  { id: 4, name: 'Cao' },
  { id: 3, name: 'Bình thường' },
  { id: 2, name: 'Trung bình' },
  { id: 1, name: 'Thấp' }
];