import React from 'react';
import { Redirect, Route } from 'react-router-dom';

// Layout Types
import { DefaultLayout } from '../layouts';

// Route Views

import SignIn from '../erp/view/SignIn';

import PrivateRoute from '../routes/PrivateRoute';
import Org from '../erp/view/Org';
import Contact from '../erp/view/Contact';
import Info from '../erp/view/Info';
import Ticket from '../erp/view/Ticket';
import Position from '../erp/view/Position';
import User from '../erp/view/User';
import Department from '../erp/view/Department';
import Report from '../components/ticket/Report';


const routes = {
  dashboard: {

    exact: true,
    layout: DefaultLayout,
    path: '/',
    component: () => <Redirect to="/quan-ly-ticket" />,
    route: PrivateRoute
  },
  signIn: {
    path: '/login',
    exact: true,
    layout: SignIn,
    route: Route
  },
  orglist: {

    path: '/quan-ly-don-vi',
    layout: DefaultLayout,
    component: Org,
    route: PrivateRoute
  },
  contactList: {

    path: '/quan-ly-lien-he',
    layout: DefaultLayout,
    component: Contact,
    route: PrivateRoute
  },
  inforList: {

    path:'/quan-ly-tai-lieu',
    layout:DefaultLayout,
    component: Info,
    route: PrivateRoute
  },
  ticketList: {

    path:'/quan-ly-ticket',
    layout:DefaultLayout,
    component: Ticket,
    route: PrivateRoute,
  },
  ticketDetail: {
    path: '/quan-ly-ticket/:uuid',
    exact: true,
    layout: DefaultLayout,
    component: Ticket,
    route: PrivateRoute
  },
  positionList:{

    path:'/quan-ly-chuc-vu',
    layout: DefaultLayout,
    component: Position,
    route: PrivateRoute
  },
  userList:{

    path:'/quan-ly-nhan-vien',
    layout: DefaultLayout,
    component: User,
    route: PrivateRoute
  },
  listDepartment:{
    path:'/quan-ly-phong-ban',
    layout: DefaultLayout,
    component: Department,
    route: PrivateRoute
  },
  listDashboard:{
    path:'/thong-ke',
    layout: DefaultLayout,
    component: Report,
    route: PrivateRoute
  }



};

export default routes;
